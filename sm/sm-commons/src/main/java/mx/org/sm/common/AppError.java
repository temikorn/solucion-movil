package mx.org.sm.common;

import java.util.ResourceBundle;

public enum AppError {
  ERROR_01("ERR_01"),
  ERROR_02("ERR_02"),
  ERROR_03("ERR_03"),
  ERROR_04("ERR_04"),
  ERROR_05("ERR_05");
  
  private final String code;
  private String codeMessage;

  private AppError(String code) {
    this.code = code;
    this.codeMessage = null;
  }

  public String getCode() {
    return code;
  }      
  
  public String getMessageCode() {
    ResourceBundle messages = null;
    
    if(this.codeMessage == null) {
      messages = ResourceBundle.getBundle("message");
      this.codeMessage = messages.getString(this.code);
    }
    
    return this.codeMessage;
  }
}
