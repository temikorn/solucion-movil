package mx.org.sm.common;

@SuppressWarnings("serial")
public class ApplicationException extends Exception {
    private AppError error;
    private String codeMessage;
    
    public ApplicationException(AppError error) {
      super();
      this.error = error;
      this.codeMessage = null;
    }

    public String getCode() {
      return this.error.getCode();
    }
    
    public String getCodeMessage() {
      if(this.codeMessage == null) {
        this.codeMessage = this.error.getMessageCode();
      }
      
      return this.codeMessage;
    }

    public AppError getError() {
      return error;
    }
}
