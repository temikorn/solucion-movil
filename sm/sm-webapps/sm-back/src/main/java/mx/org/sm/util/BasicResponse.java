package mx.org.sm.util;

import java.io.Serializable;

@SuppressWarnings("serial")
public class BasicResponse implements Serializable {
  private int code;
  private String errorCode;
  private String message;
  
  public BasicResponse() {
    this.code = 0;
    this.errorCode = "";
    this.message = "";
  }

  public int getCode() {
    return code;
  }
  public void setCode(int code) {
    this.code = code;
  }
  public String getMessage() {
    return message;
  }
  public void setMessage(String message) {
    this.message = message;
  }
  public String getErrorCode() {
    return errorCode;
  }
  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }
}
