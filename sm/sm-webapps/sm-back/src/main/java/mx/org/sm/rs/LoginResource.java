package mx.org.sm.rs;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import mx.org.sm.common.AppError;
import mx.org.sm.common.ApplicationException;
import mx.org.sm.dao.model.Usuario;
import mx.org.sm.req.LoginReq;
import mx.org.sm.res.LoginRes;
import mx.org.sm.service.AdminService;
import mx.org.sm.util.GeneralResource;

@Path("/myt/login")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public class LoginResource extends GeneralResource {
  private static final Logger LOG = Logger.getLogger(LoginResource.class);
  private AdminService adminService = getBean("AdminService");

  @POST
  public Response login(LoginReq request, @Context HttpServletRequest req){
    LOG.info("# login #");
    HttpSession session = req.getSession();
    LoginRes response = null;
    Usuario usuario = null;
    
    response = new LoginRes();
    
    try {
      usuario = adminService.getUsuario(request.getNombre(), request.getPassword());
      usuario.setToken(session.getId());
      response.setUsuario(usuario);
      
      response.setCode(Status.OK.getStatusCode());
      response.setMessage(Status.OK.toString());
    
    } catch(ApplicationException e) {
      LOG.error(e.getCodeMessage(), e);
      response.setCode(Status.INTERNAL_SERVER_ERROR.getStatusCode());
      response.setErrorCode(e.getError().getCode());
      response.setMessage(e.getCodeMessage());
      
    } catch(Exception e) {
      LOG.error(e.getMessage(), e);
      response.setCode(Status.INTERNAL_SERVER_ERROR.getStatusCode());
      response.setErrorCode(AppError.ERROR_01.getCode());
      response.setMessage(Status.INTERNAL_SERVER_ERROR.getReasonPhrase());
    }
    
    return Response.status(Status.OK).entity(response).build();
  }
}
