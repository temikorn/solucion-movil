package mx.org.sm.res;

import java.util.List;

import mx.org.sm.dao.model.Localizacion;
import mx.org.sm.util.BasicResponse;

@SuppressWarnings("serial")
public class GetLocationsRes extends BasicResponse {
  private List<Localizacion> localizacion;

public List<Localizacion> getLocalizacion() {
	return localizacion;
}

public void setLocalizacion(List<Localizacion> localizacion) {
	this.localizacion = localizacion;
} 
}
