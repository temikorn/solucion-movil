package mx.org.sm.req;

import mx.org.sm.dao.model.Localizacion;
import mx.org.sm.util.BasicRequest;

@SuppressWarnings("serial")
public class AddLocationsReq extends BasicRequest {
  private Localizacion localizacion;

public Localizacion getLocalizacion() {
	return localizacion;
}
public void setLocalizacion(Localizacion localizacion) {
	this.localizacion = localizacion;
}
 
}
