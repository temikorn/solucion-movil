package mx.org.sm.util;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;

public class HttpSessionController implements HttpSessionListener {
  private final static Logger LOG = Logger.getLogger(HttpSessionController.class);
  private static final Map<String, HttpSession> sessions = new HashMap<String, HttpSession>();

  @Override
  public void sessionCreated(HttpSessionEvent event) {
    LOG.info("# sessionCreated #");
    HttpSession session = event.getSession();
    LOG.info("# SessionId-> " + session.getId());
    sessions.put(session.getId(), session);
  }

  @Override
  public void sessionDestroyed(HttpSessionEvent event) {
    LOG.info("# sessionDestroyed #");
    LOG.info("# SessionId-> " + event.getSession().getId());
    sessions.remove(event.getSession().getId());
  }

  public static HttpSession find(String sessionId) {
    LOG.info("# find #");
    LOG.info("# SessionId-> " + sessionId);
    return sessions.get(sessionId);
  }

}
