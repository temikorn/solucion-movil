package mx.org.sm.req;

import mx.org.sm.util.BasicRequest;

@SuppressWarnings("serial")
public class LoginReq extends BasicRequest {

  private String nombre;
  private String password;

  public String getNombre() {
    return nombre;
  }
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
  public String getPassword() {
    return password;
  }
  public void setPassword(String password) {
    this.password = password;
  }
}
