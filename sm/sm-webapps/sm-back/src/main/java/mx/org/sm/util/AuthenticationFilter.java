package mx.org.sm.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import mx.org.sm.common.AppError;

public class AuthenticationFilter implements Filter {
  private final static Logger LOG = Logger.getLogger(AuthenticationFilter.class);

  private ServletContext context;
  
  @Override
  public void destroy() {
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest) request;
    HttpServletResponse res = (HttpServletResponse) response;
    HttpSession session = null;
    String token = null;
    String uri = req.getRequestURI();
    Gson gson = new Gson();
    BasicResponse basicResponse = null;
    
    token = req.getHeader("token");
    session = HttpSessionController.find(token);
    LOG.info("uri-> " + uri);
    
    if(session == null || token == null || !session.getId().equalsIgnoreCase(token)){
      basicResponse = new BasicResponse();
      basicResponse.setCode(Status.INTERNAL_SERVER_ERROR.getStatusCode());
      basicResponse.setErrorCode(AppError.ERROR_03.getCode());
      basicResponse.setMessage(AppError.ERROR_03.getMessageCode());
      
      response.setContentType("application/json");
      PrintWriter out = response.getWriter();
      out.print(gson.toJson(basicResponse));
      out.flush();
        
    }else{
      LOG.info("idSession-> " + session.getId());
      LOG.info("token-> " + token);
      chain.doFilter(request, response);
    }
  }

  @Override
  public void init(FilterConfig fConfig) throws ServletException {
    this.context = fConfig.getServletContext();
    this.context.log("AuthenticationFilter initialized");
  }

}
