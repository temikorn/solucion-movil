package mx.org.sm.res;

import mx.org.sm.dao.model.Usuario;
import mx.org.sm.util.BasicResponse;

@SuppressWarnings("serial")
public class LoginRes extends BasicResponse {

  private Usuario usuario;

  public Usuario getUsuario() {
    return usuario;
  }

  public void setUsuario(Usuario usuario) {
    this.usuario = usuario;
  }
}
