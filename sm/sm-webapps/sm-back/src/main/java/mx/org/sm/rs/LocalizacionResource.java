package mx.org.sm.rs;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import mx.org.sm.common.AppError;
import mx.org.sm.common.ApplicationException;
import mx.org.sm.dao.model.Localizacion;
import mx.org.sm.req.AddLocationsReq;
import mx.org.sm.req.GetLocationsReq;
import mx.org.sm.res.AddLocationsRes;
import mx.org.sm.res.GetLocationsRes;
import mx.org.sm.service.LocalizacionService;
import mx.org.sm.util.GeneralResource;

@Path("/sm/localizacion")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public class LocalizacionResource extends GeneralResource {
  private static final Logger LOG = Logger.getLogger(LocalizacionResource.class);
  private LocalizacionService localizacionService = getBean("LocalizacionService");
  
  @POST
  @Path("getLocations")
  public Response getLocations(GetLocationsReq request){
    LOG.info("# getLocations #");
    GetLocationsRes response = null;
    List<Localizacion> localizacion = null;
    response = new GetLocationsRes();
    
    try {
      localizacion = localizacionService.getLocations();
      LOG.info("# localizaciones" + localizacion.size());
      response.setLocalizacion(localizacion);
      response.setCode(Status.OK.getStatusCode());
      response.setMessage(Status.OK.toString());
    
    } catch(ApplicationException e) {
      LOG.error(e.getCodeMessage(), e);
      response.setCode(Status.INTERNAL_SERVER_ERROR.getStatusCode());
      response.setErrorCode(e.getError().getCode());
      response.setMessage(e.getCodeMessage());
      
    } catch(Exception e) {
      LOG.error(e.getMessage(), e);
      response.setCode(Status.INTERNAL_SERVER_ERROR.getStatusCode());
      response.setErrorCode(AppError.ERROR_01.getCode());
      response.setMessage(Status.INTERNAL_SERVER_ERROR.getReasonPhrase());
    }
    
    return Response.status(Status.OK).entity(response).build();
  }
  
  @POST
  @Path("addLocations")
  public Response addLocations(AddLocationsReq request){
    LOG.info("# addLocations #");
    AddLocationsRes response = null;
    response = new AddLocationsRes();
    
    try {
      localizacionService.addLocations(request.getLocalizacion());
      response.setCode(Status.OK.getStatusCode());
      response.setMessage(Status.OK.toString());
    
    } catch(ApplicationException e) {
      LOG.error(e.getCodeMessage(), e);
      response.setCode(Status.INTERNAL_SERVER_ERROR.getStatusCode());
      response.setErrorCode(e.getError().getCode());
      response.setMessage(e.getCodeMessage());
      
    } catch(Exception e) {
      LOG.error(e.getMessage(), e);
      response.setCode(Status.INTERNAL_SERVER_ERROR.getStatusCode());
      response.setErrorCode(AppError.ERROR_01.getCode());
      response.setMessage(Status.INTERNAL_SERVER_ERROR.getReasonPhrase());
    }
    
    return Response.status(Status.OK).entity(response).build();
  }
 
}
