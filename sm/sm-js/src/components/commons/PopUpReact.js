'use strict';

var Constants = require('../../utils/Constants.js');

var PopUpReact = React.createClass({displayName: "PopUpReact",
  getInitialState: function() {
    return {
      isShow: false,
      message: '',
      buttons: this.props.buttons,
      htmlHeaderContent: this.props.htmlHeaderContent,
      htmlBodyContent: this.props.htmlBodyContent,
      popUpStyle: this.props.popUpStyle
    };
  },
  getDefaultProps: function() {
    return {
      defaultPopUpStyle: 'PopUpReact',
      onClickAceptar: undefined
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
  },
  componentWillReceiveProps: function(nextProps) {
    this.setState({
      htmlBodyContent: nextProps.htmlBodyContent,
      htmlHeaderContent: nextProps.htmlHeaderContent
    });
  },
  shouldComponentUpdate: function() {
    return true;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  show: function(message) {
    this.setState({
      isShow: true,
      message: message
    });
  },
  hide: function() {
    this.setState({
      isShow: false,
      message: ''
    });
  },
  onClickAceptar: function(evt) {
    this.hide();

    if(this.props.onClickAceptar != undefined) {
      this.props.onClickAceptar(evt);
    }
  },
  render: function() {  
    var self = this;
    var showStyle = 'componentShow';
    var hideStyle = 'componentHide';
    var componentShow = hideStyle;
    var buttons = [];
    var htmlBodyContent = this.state.htmlBodyContent;
    var htmlHeaderContent = this.state.htmlHeaderContent;
    var popUpStyle = undefined;

    if(this.state.isShow) {
      componentShow = showStyle;

    } else {
      componentShow = hideStyle;
    }

    if(this.state.popUpStyle != undefined) {
      popUpStyle = this.state.popUpStyle + ' ' + componentShow;

    } else {
      popUpStyle = this.props.defaultPopUpStyle + ' ' + componentShow;
    }

    if(this.state.htmlHeaderContent == undefined) {
      htmlHeaderContent = '';
    }

    if(this.state.buttons != undefined) {
        this.state.buttons.forEach(function(button, index) {
        var onClickButton = function(button, index, evt) {
          button.onClick(evt);
        }.bind(self, button, index);

        buttons.push(
          React.createElement("button", {type: "button", className: "btn btn-success btn-md col-sm-3", onClick: onClickButton}, 
            button.label
          )
        );
      });

    } else {
      //crear un boton de aceptar por default
      buttons.push(
        React.createElement("button", {type: "button", className: "btn btn-success btn-md col-sm-3", onClick: this.onClickAceptar}, 
          "Aceptar"
        )
      );
    }
    //crear contenido html o en forma de mensaje
    if(htmlBodyContent == undefined) {
      htmlBodyContent = (React.createElement("p", {className: "mensaje"}, this.state.message));
    }

    return (
      React.createElement("div", {className: popUpStyle}, 
        React.createElement("section", null, 
          React.createElement("div", {className: "container PopUp"}, 
              React.createElement("div", {className: "row"}, 
                  React.createElement("div", {className: "col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4"}, 
                      React.createElement("div", {className: "panel panel-default", id: "main"}, 
                        React.createElement("div", {className: "panel-header"}, 
                          htmlHeaderContent
                        ), 
                        React.createElement("div", {className: "panel-body"}, 
                          React.createElement("div", {style: {width: '100%', marginBottom: '5px'}}, 
                            htmlBodyContent
                          ), 
                          React.createElement("div", {className: "form-group last"}, 
                            React.createElement("div", {className: "col-sm-12"}, 
                              buttons
                            )
                          )
                        )
                    )
                )
            )
          )
        )
      )
    );
  }
});

module.exports = PopUpReact;