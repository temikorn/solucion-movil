'use strict';

var CheckTableReact = React.createClass({displayName: "CheckTableReact",
  getInitialState: function() {
    return {
      isShow: this.props.isShow,
      idElementsChecked: [],
      isAllChecked: this.props.isAllChecked,
      arrElements: this.props.arrElements,
      elementId: this.props.elementId,
      elementName: this.props.elementName,
      showTools: this.props.showTools
    };
  },
  getDefaultProps: function() {
    return {
      isShow: true,
      showTools: false,
      isAllChecked: false,
      arrElements: undefined
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
  },
  componentWillReceiveProps: function(nextProps) {
    if(nextProps.arrElements != undefined) {
      this.setState({
        arrElements: nextProps.arrElements
      });
    }
  },
  shouldComponentUpdate: function() {
    return true;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  onSelectedItem: function(elem, index, evt) {
    evt.preventDefault();
    if(this.props.onSelectedItem != undefined) {
      this.props.onSelectedItem(elem, index);
    }
  },
  reset: function() {
    this.setState({
      isAllChecked: false,
      arrElements: undefined,
      idElementsChecked: []
    });
  },
  show: function() {
    this.setState({
      isShow: true
    });
  },
  hide: function() {
    this.setState({
      isShow: false
    });
  },
  setArrElements: function(arrElements) {
    this.setState({
      isAllChecked: false,
      arrElements: arrElements
    });
  },
  onChangeElem: function(elem, index, value) {
    var arrElements = this.state.arrElements;
    
    if(arrElements[index].checked) {
      arrElements[index].checked = false;

    } else {
      arrElements[index].checked = true;
    }

    this.setState({
      arrElements: arrElements      
    });
  },
  onChangeCheckAll: function(evt) {
    var isAllChecked = this.state.isAllChecked;
    var arrElements = this.state.arrElements;

    if(isAllChecked) {
      isAllChecked = false;

    } else {
      isAllChecked = true;
    }

    if(arrElements != undefined) {
      arrElements.forEach(function(elem) {
        elem.checked = isAllChecked;
      });
    }

    this.setState({
      isAllChecked: isAllChecked,
      arrElements: arrElements
    });
  },
  getElementsChecked: function() {
    var self = this;
    var elementsChecked = [];

    this.state.arrElements.forEach(function(elem) {
      if(elem.checked) {
        elementsChecked.push(elem[self.state.elementId]);
      }
    });

    return elementsChecked;
  },
  render: function() {  
    var self = this;
    var showStyle = 'componentShow';
    var hideStyle = 'componentHide';
    var styleComponent = hideStyle;
    var elemHTML = [];
    var toolsHTML = [];

    if(this.state.isShow) {
      styleComponent = showStyle;

    } else {
      styleComponent = hideStyle;
    }
    //construir elementos de la tabla
    if(this.state.arrElements != undefined) {
      this.state.arrElements.forEach(function(elem, index) {
        var tools = [];
        var onChangeElem = function(elem, index, evt) {
          self.onChangeElem(elem, index, evt.target.value);
        }.bind(self, elem, index);

        var onSelectedItem = function(elem, index, evt) {
          self.onSelectedItem(elem, index, evt);
        }.bind(self, elem, index);

        if(elem.checked == undefined) {
          elem.checked = false;
        }

        if(self.state.showTools) {
          tools.push(React.createElement("td", {className: "check_td_tools"}, 
            React.createElement("a", {className: "IconSelectedItemStyle", href: "#", onClick: onSelectedItem})
          ));
        }

        elemHTML.push(React.createElement("tr", null, 
          React.createElement("td", {className: "check_td_checked"}, React.createElement("input", {type: "checkbox", value: elem[self.state.elementId], checked: elem.checked, onChange: onChangeElem})), 
          React.createElement("td", {className: "check_td_name"}, elem[self.state.elementName]), 
          tools
        ));
      });
    }

    if(self.state.showTools) {
      toolsHTML.push(React.createElement("th", {className: "check_th_tools"}));
    }

    return (
      React.createElement("div", {className: styleComponent, style: {width: '100%'}}, 
        React.createElement("table", {className: "checkTable"}, 
          React.createElement("thead", {className: "check_thead"}, 
            React.createElement("tr", null, 
              React.createElement("th", {className: "check_th_checked"}, 
                React.createElement("input", {type: "checkbox", value: "0", checked: this.state.isAllChecked, onChange: this.onChangeCheckAll})
              ), 
              React.createElement("th", {className: "check_th_name"}, "nombre"), 
              toolsHTML
            )
          ), 

          React.createElement("tbody", null, 
            elemHTML
          )
        )
      )
    );
  }
});

module.exports = CheckTableReact;