"use strict";

var Router = require('../libs/react-router/index.js');
var Label = require('../utils/Label.js');
var adminService = require('../modulos/MyTAdminService.js');
var ErrorCodes = require('../utils/ErrorCodes.js');
var ComponentState = require('../utils/ComponentState.js');
var Constants = require('../utils/Constants.js');
var PopUpReact = require('../components/commons/PopUpReact.js');

var ControlCierreAcciones = React.createClass({displayName: "ControlCierreAcciones",
  mixins: [ Router.Navigation ],
 	getInitialState: function() {
    return {
      usuario: ctx.get('usuario'),
      shouldMount: true,
      componentState: ComponentState.NORMAL_STATE,
      pagina: 0,
      resultPerPagina: 10,
      estatusCompromisos: undefined,
      idEstatusCompromiso: 0,
      emailFace: '',
      nombreVisitante: '',
      accionesCierreOrg: undefined,
      accionesCierre: undefined
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
    var self = this;

    var onSuccess = function(res) {
      self.setState({
        estatusCompromisos: res.estatusCompromisos
      });
    };

    if(validaSession()) {
      var params = {};
      adminService.getEstatusCompromisos(params, onSuccess, this.onError, this.onFail);

    } else {
      this.setState({
        shouldMount: false
      });
      this.transitionTo('Login');
    }
  },
  componentWillReceiveProps: function(nextProps) {
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  onFail: function(err) {
    console.log(err);
    this.setState({
      componentState: ComponentState.ERROR_STATE
    });
    this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
  },
  onError: function(err) {
    console.log(err);

    switch(err.errorCode) {
      case ErrorCodes.ERR_01:
        this.setState({
          componentState: ComponentState.ERROR_STATE
        });
        this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
      break;

      case ErrorCodes.ERR_03:
        this.setState({
          componentState: ComponentState.ERROR_STATE
        });
        this.refs.popUpAlert.show(ErrorCodes.ERR_03_MSG);
      break;

      default:
        console.log('Error no categorizado...');
      break;
    }

    scrollMainTo();
  },
  onClickLimpiar: function() {
    this.setState({
      componentState: ComponentState.NORMAL_STATE,
      idEstatusCompromiso: 0,
      emailFace: '',
      nombreVisitante: '',
      resultPerPagina: 10,
      accionesCierreOrg: undefined,
      accionesCierre: undefined
    });
  },
  getPaginado: function(accionesCierre, resultPerPagina) {
    var paginado = undefined;
    var pagina = undefined;
    var i = 0;
    var self = this;

    if(accionesCierre != undefined && accionesCierre.length > 0) {
      paginado = [];
      pagina = [];

      accionesCierre.forEach(function(accionCierre, index) {
        pagina.push(accionCierre);
        
        if(i < resultPerPagina) {
          i++
        } 

        if(i == resultPerPagina || index == (accionesCierre.length - 1)) {
          paginado.push(pagina);
          pagina = [];
          i = 0;
        }
      });
    }

    return paginado;
  },
  onClickBuscar: function() {
    var self = this;

    var onSuccess = function(res) {
      var componentState = ComponentState.NORMAL_STATE;

      if(res.accionesCierre == undefined || res.accionesCierre.length == 0) {
        componentState = ComponentState.INFO_STATE;
        self.refs.popUpAlert.show(Label.NO_RESULTADOS);
      }

      self.setState({
        componentState: componentState,
        pagina: 0,
        accionesCierreOrg: res.accionesCierre,
        accionesCierre: self.getPaginado(res.accionesCierre, self.state.resultPerPagina)
      });
    };

    var params = {
      emailFace: this.state.emailFace,
      nombreVisitante: this.state.nombreVisitante,
      estatusCompromiso: this.state.idEstatusCompromiso
    };
    adminService.getAccionesCierre(params, onSuccess, this.onError, this.onFail);
  },
  onChangeEmailFace: function(evt) {
    this.setState({
      emailFace: evt.target.value
    });
  },
  onChangeNombreVisitante: function(evt) {
    this.setState({
      nombreVisitante: evt.target.value
    });
  },
  onChangeEstatusCompromiso: function(evt) {
    this.setState({
      idEstatusCompromiso: evt.target.value
    });
  },
  onChangeCierreCompromiso: function(accionCierre, index, evt) {
    var idEstatusCompromiso = evt.target.value;
    var self = this;
    var accionesCierre = this.state.accionesCierre;

    var onSuccess = function(res) {
      var componentState = undefined;
      accionCierre.idEstatusCompromiso = parseInt(idEstatusCompromiso);
      accionesCierre[self.state.pagina][index] = accionCierre;
      componentState = ComponentState.EXITO_STATE;

      self.setState({
        componentState: componentState,
        accionesCierre: accionesCierre
      });
      self.refs.popUpAlert.show(Label.REGISTRO_MODIF_SUCCESS);
    };

    var params = {
      idUsuario: this.state.usuario.getIdUsuario(),
      idCompromiso: accionCierre.idCompromiso,
      idEstatusCompromiso: evt.target.value
    };
    adminService.updateEstatusCompromiso(params, onSuccess, this.onError, this.onFail);
  },
  onClickNextPage: function(evt) {
    evt.preventDefault();
    var nextPage = 0;

    if(this.state.accionesCierre != undefined) {
      nextPage = (this.state.pagina + 1);

      if(nextPage <= (this.state.accionesCierre.length - 1) ) {
        this.setState({
          pagina: nextPage
        });  
      }
    }
  },
  onClickBackPage: function(evt) {
    evt.preventDefault();
    var backPage = 0;
    var firstPage = 0;

    if(this.state.accionesCierre != undefined) {
      backPage = (this.state.pagina - 1);

      if(backPage >= firstPage) {
        this.setState({
          pagina: backPage
        });  
      }
    }
  },
  onClickFirstPage: function(evt) {
    evt.preventDefault();
    var firstPage = 0;

    if(this.state.accionesCierre != undefined) {
      if(firstPage != this.state.pagina) {
        this.setState({
          pagina: firstPage
        });  
      }
    }
  },
  onClickLastPage: function(evt) {
    evt.preventDefault();
    var lastPage = 0;

    if(this.state.accionesCierre != undefined) {
      lastPage = (this.state.accionesCierre.length - 1);

      if(lastPage != this.state.pagina) {
        this.setState({
          pagina: lastPage
        });  
      }
    }
  },
  onChangeResultPerPagina: function(evt) {
    if(this.state.accionesCierreOrg != undefined) {
      this.setState({
        pagina: 0,
        resultPerPagina: evt.target.value,
        accionesCierre: this.getPaginado(this.state.accionesCierreOrg, evt.target.value)
      });
    }
  },
  render: function() {
    var self = this;
    var estatusCompromisos = [];
    var acciones = [];
    var totalPaginado = '0 de 0 Páginas';

    if(!this.state.shouldMount) {
      return (React.createElement("div", null));
    }

    switch(this.state.componentState) {
      case ComponentState.NORMAL_STATE:
      break;

      case ComponentState.EXITO_STATE:
      break;

      case ComponentState.ERROR_STATE:
      break;

      case ComponentState.INFO_STATE:
      break;

      case ComponentState.ALERT_STATE:
      break;
    }
    //construr estatus de los compromisos
    if(this.state.estatusCompromisos != undefined) {
      this.state.estatusCompromisos.forEach(function(estatusCompromiso) {
        estatusCompromisos.push(React.createElement("option", {value: estatusCompromiso.idEstatusCompromiso}, estatusCompromiso.descripcion));
      });
    }
    //construir las acciones
    if(this.state.accionesCierre != undefined) {
      totalPaginado = ((this.state.pagina + 1) + ' de ' +  this.state.accionesCierre.length + ' Páginas')
      this.state.accionesCierre[this.state.pagina].forEach(function(accionCierre, index) {
        var EN_TIEMPO = 'SI'
        var FUERA_TIEMPO = 'NO';
        var enTiempo = '';

        var onChangeCierreCompromiso = function(accionCierre, index, evt) {
          self.onChangeCierreCompromiso(accionCierre, index, evt);
        }.bind(self, accionCierre, index);

        if(accionCierre.enTiempo) {
          enTiempo = EN_TIEMPO;

        } else {
          enTiempo = FUERA_TIEMPO;
        }

        acciones.push(
          React.createElement("tr", null, 
            React.createElement("td", null, accionCierre.nombreVisitante), 
            React.createElement("td", null, accionCierre.email), 
            React.createElement("td", null, accionCierre.accion), 
            React.createElement("td", null, 
              React.createElement("select", {value: accionCierre.idEstatusCompromiso, defaultValue: accionCierre.idEstatusCompromiso, onChange: onChangeCierreCompromiso}, 
                estatusCompromisos
              )
            ), 
            React.createElement("td", null, enTiempo)
          ) 
        );
      });
    }

    return (
      React.createElement("div", {id: "container-gral"}, 
        React.createElement(PopUpReact, {ref: "popUpAlert", type: "alert", popUpStyle: "PopUpReactAlert"}), 

        React.createElement("article", {className: "module width_full"}, 
          React.createElement("header", null, React.createElement("h3", null, "Filtros de Búsqueda")), 
            React.createElement("div", {className: "module_content"}, 
              React.createElement("fieldset", {style: {width:'48%', float:'left', marginRight: '3%'}}, 
                React.createElement("label", null, "Facebook o E-mail"), 
                React.createElement("input", {type: "text", style: {width:'92%'}, placeholder: "Ingresa correo de Faceboook o E-mail", 
                  maxLength: "100", 
                  value: this.state.emailFace, onChange: this.onChangeEmailFace})
              ), 
              React.createElement("fieldset", {style: {width:'48%', float:'left'}}, 
                React.createElement("label", null, "Estatus de la Acción"), 
                React.createElement("select", {style: {width:'92%'}, value: this.state.idEstatusCompromiso, onChange: this.onChangeEstatusCompromiso}, 
                  React.createElement("option", {value: "0"}, "Todos"), 
                  estatusCompromisos
                )
              ), 
              React.createElement("fieldset", {style: {width:'99%', float:'left', marginRight: '3%'}}, 
                React.createElement("label", null, "Nombre"), 
                React.createElement("input", {type: "text", style: {width:'96%'}, placeholder: "Ingresa el nombre del visitante", 
                  maxLength: "100", 
                  value: this.state.nombreVisitante, onChange: this.onChangeNombreVisitante})
              ), 
              React.createElement("div", {className: "clear"})
            ), 
          React.createElement("footer", null, 
            React.createElement("div", {className: "submit_link"}, 
              React.createElement("input", {type: "button", value: "BUSCAR", className: "alt_btn", onClick: this.onClickBuscar}), 
              React.createElement("input", {type: "button", value: "LIMPIAR", onClick: this.onClickLimpiar})
            )
          )
        ), 
        
        React.createElement("article", {className: "module width_full"}, 
          React.createElement("header", null, 
            React.createElement("h3", {className: "tabs_involved"}, "Acciones encontradas")
          ), 
          React.createElement("div", {className: "tab_container"}, 
            React.createElement("div", {id: "tab1", className: "tab_content"}, 
              React.createElement("table", {className: "tablesorter", cellSpacing: "0"}, 
                React.createElement("thead", null, 
                  React.createElement("tr", null, 
                    React.createElement("th", null, "Nombre visitante"), 
                    React.createElement("th", null, "Email"), 
                    React.createElement("th", null, "Acción"), 
                    React.createElement("th", null, "Estatus"), 
                    React.createElement("th", null, "En tiempo")
                  )
                ), 
                React.createElement("tbody", null, 
                  acciones
                ), 
                React.createElement("tfoot", null, 
                  React.createElement("tr", null, 
                    React.createElement("td", {colSpan: "100%"}, 
                      React.createElement("div", {id: "pag"}, 
                        React.createElement("select", {id: "soflow-color", value: this.state.resultPerPagina, onChange: this.onChangeResultPerPagina}, 
                          React.createElement("option", {value: "5"}, "5"), 
                          React.createElement("option", {value: "10"}, "10"), 
                          React.createElement("option", {value: "15"}, "15"), 
                          React.createElement("option", {value: "20"}, "20"), 
                          React.createElement("option", {value: "25"}, "25")
                        ), 
                        React.createElement("ul", {id: "pagination"}, 
                          React.createElement("li", {className: "previous"}, React.createElement("a", {href: "#", onClick: this.onClickFirstPage}, "<< Primero")), 
                          React.createElement("li", {className: "previous"}, React.createElement("a", {href: "#", onClick: this.onClickBackPage}, "< Anterior")), 
                          React.createElement("li", {className: "active"}, totalPaginado), 
                          React.createElement("li", {className: "next"}, React.createElement("a", {href: "#", onClick: this.onClickNextPage}, "Siguiente >")), 
                          React.createElement("li", {className: "next"}, React.createElement("a", {href: "#", onClick: this.onClickLastPage}, "Último >>"))
                        )
                      )
                    )
                  )
                )
              )
            )
          )
        ), 
        React.createElement("div", {className: "spacer"})
      )
    );
  }
});

module.exports = ControlCierreAcciones;