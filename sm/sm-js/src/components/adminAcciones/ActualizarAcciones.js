'use strict';

/* # seccion para declarar utilerias # */
var Clone = require('../../utils/Clone.js');
var Context = require('../../utils/Context.js');
var ErrorCodes = require('../../utils/ErrorCodes.js');
var ErrorMsg = require('../../utils/ErrorMsg.js');
var Constants = require('../../utils/Constants.js');
/* # seccion para declarar compronentes comunes # */
var PopUpReact = require('../../components/commons/PopUpReact.js');
var CheckTableReact = require('../../components/commons/CheckTableReact.js');
var InputFileReact = require('../../components/commons/InputFileReact.js');
/* # seccion para declarar servicios # */
var accionesService = require('../../modulos/AccionesService.js');
/* # Seccion de componentes a los que depende # */

/*################################################*/
/*###             ActualizarAcciones de accion               ###*/
/*################################################*/
var ActualizarAcciones = React.createClass({displayName: "ActualizarAcciones",
  getInitialState: function() {
    return {
      shouldMount: true,
      showComponent: false,
      tabSelected: 1,
      newAccion: {
        idAccion: 0,
        descripcion: '',
        idGeneralAccion: 0,
        idLlamadoAccion: 0
      },
      catGeneralAcciones: undefined,
      catLamadoAcciones: undefined,
      temas: undefined,
      temasClone: undefined,
      subTemasClone: undefined,
      sectores: undefined,
      sectoresClone: undefined,
      subSectoresClone: undefined,
      tiempos: undefined,
      tiemposClone: undefined,
      medios: undefined,
      mediosClone: undefined,
      niveles: undefined,
      nivelesClone: undefined,
      inversiones: undefined,
      inversionesClone: undefined,
      archivosAccionCount: 0,
      archivosAccion: [],
      archivosAccionToDelete: []
    };
  },
  getDefaultProps: function() {
    return {
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
    var self = this;

    if(validaSession()) {
      this.storage = new Context();

      var onSuccess = function(res) {
        self.setState({
          catGeneralAcciones: res.generalAcciones,
          catLlamadoAcciones: res.llamadoAcciones,
          temas: res.temas,
          temasClone: Clone.clone(res.temas),
          sectores: res.sectores,
          sectoresClone: Clone.clone(res.sectores),
          tiempos: res.tiempos,
          tiemposClone: Clone.clone(res.tiempos),
          medios: res.medios,
          mediosClone: Clone.clone(res.medios),
          niveles: res.niveles,
          nivelesClone: Clone.clone(res.niveles),
          inversiones: res.inversiones,
          inversionesClone: Clone.clone(res.inversiones)
        });
      };

      var params = {};
      accionesService.getCatalogos(params, onSuccess, this.onError, this.onFail);

    } else {
      this.setState({
        shouldMount: false
      });
    }
  },
  componentWillReceiveProps: function(nextProps) {
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  onFail: function(err) {
    console.log(err);
    this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
  },
  onError: function(err) {
    console.log(err);
    
    switch(err.errorCode) {
      case ErrorCodes.ERR_01:
        this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
      break;

      case ErrorCodes.ERR_03:
        this.refs.popUpAlert.show(ErrorCodes.ERR_03_MSG);
      break;

      default:
        console.log('Error no categorizado...');
      break;
    } 
  },
  reset: function() {
    this.refs.sectoresChecked.reset();
    this.refs.subSectoresChecked.reset();
    this.refs.temasChecked.reset();
    this.refs.subTemasChecked.reset();
    this.refs.tiemposChecked.reset();
    this.refs.mediosChecked.reset();
    this.refs.nivelesChecked.reset();
    this.refs.inversionesChecked.reset();
    this.setState({
      tabSelected: 1,
      newAccion: {
        idAccion: 0,
        descripcion: '',
        idGeneralAccion: 0,
        idLlamadoAccion: 0
      },
      temasClone: Clone.clone(this.state.temas),
      subTemasClone: undefined,
      sectoresClone: Clone.clone(this.state.sectores),
      subSectoresClone: undefined,
      tiemposClone: Clone.clone(this.state.tiempos),
      mediosClone: Clone.clone(this.state.medios),
      nivelesClone: Clone.clone(this.state.niveles),
      inversionesClone: Clone.clone(this.state.inversiones),
      archivosAccionCount: 0,
      archivosAccion: [],
      archivosAccionToDelete: []
    });
  },
  show: function() {
    this.setState({
      showComponent: true,
      tabSelected: 1
    });
  },
  hide: function() {
    this.setState({
      showComponent: false,
      tabSelected: 1
    });
  },
  setAccion: function(accion) {
    var self = this;
    //construir los clones de los filtros poniendo en checked los que la accion tiene actualmente configurada
    var onSuccess = function(res) {
      var temasClone = self.state.temasClone;
      var sectoresClone = self.state.sectoresClone;
      var tiemposClone = self.state.tiemposClone;
      var mediosClone = self.state.mediosClone;
      var nivelesClone = self.state.nivelesClone;
      var inversionesClone = self.state.inversionesClone;
      var archivosAccion = self.state.archivosAccion;
      var getAccionRes = res.getAccionRes;
      //configurar temas y subtemas
      getAccionRes.temas.forEach(function(tema) {
        temasClone.forEach(function(temaClone) {
          if(temaClone.idTema == tema.idTema && tema.checked) {
            temaClone.checked = tema.checked;

            tema.subTemas.forEach(function(subTema) {
              temaClone.subTemas.forEach(function(subTemaClone) {
                if(subTemaClone.idSubTema == subTema.idSubTema && subTema.checked) {
                  subTemaClone.checked = subTema.checked
                }
              });              
            });
          }
        });
      });
      //configurar sectores y subsectores
      getAccionRes.sectores.forEach(function(sector) {
        sectoresClone.forEach(function(sectorClone) {
          if(sectorClone.idSector == sector.idSector && sector.checked) {
            sectorClone.checked = sector.checked;
            sectorClone.hasSubSectores = sector.hasSubSectores;

            sector.subSectores.forEach(function(subSector) {
              sectorClone.subSectores.forEach(function(subSectorClone) {
                if(subSectorClone.idSubSector == subSector.idSubSector && subSector.checked) {
                  subSectorClone.checked = subSector.checked
                }
              });              
            });
          }
        });
      });
      //configurar tiempo  a invertir
      getAccionRes.tiempos.forEach(function(tiempo) {
        tiemposClone.forEach(function(tiempoClone) {
          if(tiempoClone.idTiempo == tiempo.idTiempo && tiempo.checked) {
            tiempoClone.checked = tiempo.checked;
          }
        });
      });
      //configurar medios
      getAccionRes.medios.forEach(function(medio) {
        mediosClone.forEach(function(medioClone) {
          if(medioClone.idMedio == medio.idMedio && medio.checked) {
            medioClone.checked = medio.checked;
          }
        });
      });
      //configurar niveles
      getAccionRes.niveles.forEach(function(nivel) {
        nivelesClone.forEach(function(nivelClone) {
          if(nivelClone.idNivel == nivel.idNivel && nivel.checked) {
            nivelClone.checked = nivel.checked;
          }
        });
      });
      //configurar inversiones
      getAccionRes.inversiones.forEach(function(inversion) {
        inversionesClone.forEach(function(inversionClone) {
          if(inversionClone.idInversion == inversion.idInversion && inversion.checked) {
            inversionClone.checked = inversion.checked;
          }
        });
      });
      //cargar los archivos configurados para la accion
      getAccionRes.archivosAccion.forEach(function(archivoAccion) {
        archivosAccion.push({
          fileId: archivoAccion.fileId,
          fileName: archivoAccion.fileName,
          inputFile: undefined,
          fileSelected: archivoAccion.fileSelected
        });
      });

      self.setState({
        newAccion: {
          idAccion: getAccionRes.accion.idAccion,
          descripcion: getAccionRes.accion.descripcion,
          idGeneralAccion: getAccionRes.accion.idGeneralAccion,
          idLlamadoAccion: getAccionRes.accion.idLlamadoAccion
        },
        temasClone: temasClone,
        sectoresClone: sectoresClone,
        tiemposClone: tiemposClone,
        mediosClone: mediosClone,
        nivelesClone: nivelesClone,
        inversionesClone: inversionesClone,
        archivoAccion: archivosAccion
      });
    };

    var params = {
      idAccion: accion.idAccion
    };
    accionesService.getAccionToUpdate(params, onSuccess, this.onError, this.onFail);
  },
  onClickTab: function(tabSelected, evt) {
    switch(tabSelected) {
      case Constants.ADMIN_ALTA_ACCION.TAB_CONFIG_ACCION:
      break;

      case Constants.ADMIN_ALTA_ACCION.TAB_CONFIG_ARCHIVOS:
      break;
    }

    this.setState({
      tabSelected: tabSelected
    });
  },
  onChangeNombreAccion: function(evt) {
    var newAccion = this.state.newAccion;
    newAccion.descripcion = evt.target.value;

    this.setState({
      newAccion: newAccion
    });
  },
  onChangeGeneralAccion: function(evt) {
    var newAccion = this.state.newAccion;
    newAccion.idGeneralAccion = evt.target.value;

    this.setState({
      newAccion: newAccion
    });
  },
  onChangeLlamadoAccion: function(evt) {
    var newAccion = this.state.newAccion;
    newAccion.idLlamadoAccion = evt.target.value;

    this.setState({
      newAccion: newAccion
    });
  },
  onClickCancelarActualizarAccion: function(evt) {
    this.reset();
    this.hide();
    this.props.onClickCancelarActualizarAccion();
  },
  onClickGuardarAccion: function(evt) {
    var self = this;
    var result = {
      isError: false,
      erromsg: ''
    };

    var onSuccess = function(res) {
      self.refs.popUpGuardarSucces.show("La acción fue actualizada exitosamente.");
    };
    //construir objetos necesarios para el servicio eliminando las propiedades no necesarios
    var accion = {
      idAccion: this.state.newAccion.idAccion,
      descripcion: this.state.newAccion.descripcion,
      idGeneralAccion: this.state.newAccion.idGeneralAccion,
      idLlamadoAccion: this.state.newAccion.idLlamadoAccion
    };

    var temas = [];
    this.state.temasClone.forEach(function(tema) {
      if(tema.checked) {
        var subTemas = [];

        tema.subTemas.forEach(function(subTema) {
          if(subTema.checked) {
            subTemas.push({
              idSubTema: subTema.idSubTema,
              checked: subTema.checked
            });
          }
        });

        temas.push({
          idTema: tema.idTema,
          checked: tema.checked,
          subTemas: subTemas
        });
      }
    });

    var sectores = [];
    this.state.sectoresClone.forEach(function(sector) {
      var subSectores = [];
      var hasSubSectores = false;

      if(sector.checked) {
        if(sector.subSectores != 0) {
          hasSubSectores = true;
          sector.subSectores.forEach(function(subSector) {
            if(subSector.checked) {
              subSectores.push({
                idSubSector: subSector.idSubSector,
                checked: subSector.checked
              });
            }
          });
        }

        sectores.push({
          idSector: sector.idSector,
          checked: sector.checked,
          subSectores: subSectores,
          hasSubSectores: hasSubSectores
        });
      }
    });

    var tiempos = [];
    this.state.tiemposClone.forEach(function(tiempo) {
      if(tiempo.checked) {
        tiempos.push({
          idTiempo: tiempo.idTiempo,
          checked: tiempo.checked
        });
      }
    });

    var medios = [];
    this.state.mediosClone.forEach(function(medio) {
      if(medio.checked) {
        medios.push({
          idMedio: medio.idMedio,
          checked: medio.checked
        });
      }
    });

    var niveles = [];
    this.state.nivelesClone.forEach(function(nivel) {
      if(nivel.checked) {
        niveles.push({
          idNivel: nivel.idNivel,
          checked: nivel.checked
        });
      }
    });

    var inversiones = [];
    this.state.inversionesClone.forEach(function(inversion) {
      if(inversion.checked) {
        inversiones.push({
          idInversion: inversion.idInversion,
          checked: inversion.checked
        });
      }
    });
    //aqui poner codigo para validar los datos
    //minimo se debe seleciconar una de cada cosa
    var archivosAccion = Clone.clone(this.state.archivosAccion);
    result = this.validaGuardarAccion(temas, sectores, tiempos, medios, niveles, inversiones, accion, archivosAccion);
    //agregar en el arreglo que se enviara a servicio los archivos a eliminar
    this.state.archivosAccionToDelete.forEach(function(archivoToDelete) {
      archivosAccion.push(archivoToDelete);
    });

    if(!result.isError) {
      var params = {
        accion: accion,
        temas: temas,
        sectores: sectores,
        tiempos: tiempos,
        medios: medios,
        niveles: niveles,
        inversiones: inversiones,
        archivosAccion: archivosAccion
      };
      accionesService.actualizarAccion(params, onSuccess, this.onError, this.onFail);

    } else {
      this.refs.popUpAlert.show(result.errorMsg);
    }
  },
  validaGuardarAccion: function(temas, sectores, tiempos, medios, niveles, inversiones, accion, archivosAccion) {
    var result = {
      isError: false,
      errorMsg: ''
    };

    if(accion.descripcion.trim() == '') {
      result = {
        isError: true,
        errorMsg: 'Es necesario escribir un nombre para la acción.'
      };

      return result;
    }

    if(accion.idGeneralAccion == 0) {
      result = {
        isError: true,
        errorMsg: 'Es necesario seleccionar un general acción.'
      };

      return result;
    }

    if(accion.idLlamadoAccion == 0) {
      result = {
        isError: true,
        errorMsg: 'Es necesario seleccionar un llamdo a la acción.'
      };

      return result;
    }

    if(archivosAccion.length != 0) {
      archivosAccion.every(function (archivoAccion) {
        if(!archivoAccion.fileSelected) {
          result = {
            isError: true,
            errorMsg: 'Existen archivos sin cargar para la acción.'
          };
          return false;
        }

        return true;
      });

      if(result.isError) {
        return result;  
      }
      
    } else {
      result = {
        isError: true,
        errorMsg: 'Es necesario cargar minimo un archivo para la acción.'
      };

      return result;
    }

    if(temas.length != 0) {
      temas.every(function(tema) {
        if(tema.subTemas.length == 0) {
          result = {
            isError: true,
            errorMsg: 'Es necesario seleccionar mínimo un subtema por cada tema seleccionado.'
          };

          return false;
        }

        return true;
      });

      if(result.isError) {
        return result;  
      }

    } else {
      result = {
        isError: true,
        errorMsg: 'Es necesario seleccionar mínimo un tema.'
      };

      return result;
    }

    if(sectores.length != 0) {
      //validar que si tiene subsectores entonces debe tener seleccionado minimo un subsector
      sectores.every(function(sector) {
        if(sector.hasSubSectores && sector.subSectores.length == 0) {
          result = {
            isError: true,
            errorMsg: 'Es necesario seleccionar mínimo un subsector por cada sector que contiene subsectores.'
          };

          return true;
        }

        return true;
      });

      if(result.isError) {
        return result;  
      }

    } else {
      result = {
        isError: true,
        errorMsg: 'Es necesario seleccionar mínimo un sector.'
      };

      return result;
    }

    if(tiempos.length == 0) {
      result = {
        isError: true,
        errorMsg: 'Es necesario seleccionar mínimo un tiempo a invertir.'
      };

      return result;
    }

    if(medios.length == 0) {
      result = {
        isError: true,
        errorMsg: 'Es necesario seleccionar mínimo un medio.'
      };

      return result;
    }

    if(niveles.length == 0) {
      result = {
        isError: true,
        errorMsg: 'Es necesario seleccionar mínimo un nivel.'
      };

      return result;
    }

    if(inversiones.length == 0) {
      result = {
        isError: true,
        errorMsg: 'Es necesario seleccionar mínimo un tipo de inversión.'
      };

      return result;
    }

    return result;
  },
  onSelectedSector: function(itemSelected, index) {
    this.refs.subSectoresChecked.setArrElements(itemSelected.subSectores);
  },
  onSelectedTema: function(itemSelected, index) {
    this.refs.subTemasChecked.setArrElements(itemSelected.subTemas);
  },
  onClickEliminaArchivo: function(archivoAccion, index, evt) {
    evt.preventDefault();
    var archivosAccion = this.state.archivosAccion;
    var archivosAccionToDelete = this.state.archivosAccionToDelete;
    archivosAccion.splice(archivosAccion.indexOf(archivoAccion), 1);

    if(archivoAccion.fileId > 0) {
      archivoAccion.statusAccion = 3; //estatus para eliminar archivo
      archivosAccionToDelete.push(archivoAccion);
    }

    this.setState({
      archivosAccion: archivosAccion,
      archivosAccionToDelete: archivosAccionToDelete
    });
  },
  onErrorFileSelected: function(fileId, message) {
    console.log('fileId-> ' + fileId + ', message-> ' + message);
    this.refs.popUpAlert.show(message);
  },
  onFileSelected: function(fileId, fileName, fileSelected, inputFile) {
    var archivosAccion = this.state.archivosAccion;
    var archivoAccion = undefined;
    var archivoIndex = 0;
    //buscar si el archivo ya existe en el arreglo (que debe :D)
    this.state.archivosAccion.every(function(archivo, index) {
      if(archivo.fileId == fileId) {
        archivoAccion = archivo;
        archivoIndex = index;

        return false;
      }

      return true;
    });

    archivoAccion.fileName = fileName;
    archivoAccion.inputFile = inputFile;
    archivoAccion.fileSelected = fileSelected;
    archivosAccion[archivoIndex] = archivoAccion;

    if(archivoAccion.fileId > 0) {
      archivoAccion.statusAccion = 2; //estatus del archivo para actualizar
    }

    this.setState({
      archivosAccion: archivosAccion
    });
  },
  onClickNewArchivoAccion: function(evt) {
    evt.preventDefault();
    var archivosAccion = this.state.archivosAccion;
    var archivosAccionCount = (this.state.archivosAccionCount - 1);
    var archivoAccion = {
      fileId: archivosAccionCount,
      fileName: '',
      inputFile: undefined,
      fileSelected: false,
      statusAccion: 1 //estatus para dar de alta un nuevo archivo
    };

    archivosAccion.push(archivoAccion);

    this.setState({
      archivosAccionCount: archivosAccionCount,
      archivosAccion: archivosAccion
    });
  },
  onSuccessGuardarAccion: function(evt) {
    this.props.onClickActualizarAccion();
  },
  render: function() {
    var self = this;
    var componentShow = 'componentShow';
    var componentHide = 'componentHide';
    var styleTabContent = 'tab_content';
    var activeTabConfigAccion = '';
    var styleTabConfigAccion = componentHide;
    var activeTabConfigArchivos = '';
    var styleTabConfigArchivos = componentHide;
    var styleAltaComp = componentHide;
    var onTabConfigAccion = undefined;
    var onTabConfigArchivos = undefined;
    var generalAcciones = [];
    var llamadoAcciones = [];
    var archivosAccion = [];

    if(!this.state.shouldMount) {
      return (React.createElement("div", null));
    }
    //decidir si mostrar el componente
    if(this.state.showComponent) {
      styleAltaComp = componentShow;

    } else {
      styleAltaComp = componentHide;
    }
    //que hacer dependiendo del tab seleccionado
    switch(this.state.tabSelected) {
      case Constants.ADMIN_ALTA_ACCION.TAB_CONFIG_ACCION:
        styleTabConfigAccion = (styleTabContent + ' ' + componentShow);
        activeTabConfigAccion = 'active';
      break;

      case Constants.ADMIN_ALTA_ACCION.TAB_CONFIG_ARCHIVOS:
        styleTabConfigArchivos = (styleTabContent + ' ' + componentShow);
        activeTabConfigArchivos = 'active';
      break;
    }
    //crear acciones para los tab existentes
    onTabConfigAccion = function(tabSelected, evt){
      self.onClickTab(tabSelected, evt);
    }.bind(self, Constants.ADMIN_ALTA_ACCION.TAB_CONFIG_ACCION);

    onTabConfigArchivos = function(tabSelected, evt){
      self.onClickTab(tabSelected, evt);
    }.bind(self, Constants.ADMIN_ALTA_ACCION.TAB_CONFIG_ARCHIVOS);
    //#####################
    //Construir catalogos
    if(this.state.catGeneralAcciones != undefined) {
      this.state.catGeneralAcciones.forEach(function(generalAccion) {
        generalAcciones.push(React.createElement("option", {value: generalAccion.idGeneralAccion}, generalAccion.nombre));
      });
    }

    if(this.state.catLlamadoAcciones != undefined) {
      this.state.catLlamadoAcciones.forEach(function(llamadoAccion) {
        llamadoAcciones.push(React.createElement("option", {value: llamadoAccion.idLlamadoAccion}, llamadoAccion.descripcion));
      });
    }
    //####################
    //construccion de compornentes para carga de archivos
    this.state.archivosAccion.forEach(function(archivoAccion, index) {
      var onClickEliminaArchivo = function(archivoAccion, index, evt){
        self.onClickEliminaArchivo(archivoAccion, index, evt);
      }.bind(self, archivoAccion, index);

      archivosAccion.push(
        React.createElement("article", {className: "width_2_full", style: {height: '30px'}}, 
          React.createElement("a", {className: "IconCancelStyle", style: {float: 'left'}, href: "#", onClick: onClickEliminaArchivo}), 
          React.createElement(InputFileReact, {fileId: archivoAccion.fileId, fileName: archivoAccion.fileName, fileInput: archivoAccion.fileInput, name: 'Archivo_' + index, 
            fileSelected: archivoAccion.fileSelected, placeholder: "Nombre del archivo", maxSize: "3000", extensions: ['.pdf', 'docx'], 
            onFileSelected: self.onFileSelected, onErrorFileSelected: self.onErrorFileSelected})
        )
      );
    });
    
    return (
      React.createElement("div", {id: "container-gral", className: styleAltaComp}, 
        React.createElement(PopUpReact, {ref: "popUpAlert", type: "alert", popUpStyle: "PopUpReactAlert"}), 
        React.createElement(PopUpReact, {ref: "popUpGuardarSucces", type: "alert", popUpStyle: "PopUpReactAlert", onClickAceptar: this.onSuccessGuardarAccion}), 

        React.createElement("nav", null, 
          React.createElement("ul", {id: "menuNav"}, 
            React.createElement("li", {className: activeTabConfigAccion, onClick: onTabConfigAccion}, "Configuración de Accion"), 
            React.createElement("li", {className: activeTabConfigArchivos, onClick: onTabConfigArchivos}, "Configuración de Archivos")
          )
        ), 
        
        React.createElement("article", {className: "module width_full"}, 
          React.createElement("div", {className: styleTabConfigAccion}, 
            React.createElement("article", {className: "module width_full"}, 
              React.createElement("header", null, React.createElement("h3", null, "Configuración acción")), 
              React.createElement("div", {className: "module_content", style: {width: '98%'}}, 

                React.createElement("article", {className: "width_2_full", style: {height: '30px'}}, 
                  React.createElement("label", {style: {width:'13%'}}, "Acción:"), 
                  React.createElement("input", {type: "text", style: {width:'86%'}, name: "nombre", maxLength: "340", size: "300", value: this.state.newAccion.descripcion, 
                    onChange: this.onChangeNombreAccion})
                ), 

                React.createElement("article", {className: "width_2_full", style: {height: '30px'}}, 
                  React.createElement("label", {style: {width:'13%'}}, "General Acción:"), 
                  React.createElement("select", {style: {width:'86%'}, value: this.state.newAccion.idGeneralAccion, onChange: this.onChangeGeneralAccion}, 
                    React.createElement("option", {value: "0"}, "Selecciona una opción"), 
                    generalAcciones
                  )
                ), 

                React.createElement("article", {className: "width_2_full", style: {height: '30px'}}, 
                  React.createElement("label", {style: {width:'13%'}}, "Llamado a la Acción:"), 
                  React.createElement("select", {style: {width:'86%'}, value: this.state.newAccion.idLlamadoAccion, onChange: this.onChangeLlamadoAccion}, 
                    React.createElement("option", {value: "0"}, "Selecciona una opción"), 
                    llamadoAcciones
                  )
                ), 

                React.createElement("article", {className: "width_half", style: {marginTop: '15px'}}, 
                  React.createElement("label", {style: {width:'100%'}}, "Temas:"), 
                  React.createElement(CheckTableReact, {ref: "temasChecked", arrElements: this.state.temasClone, elementId: "idTema", elementName: "descripcion", 
                    showTools: true, onSelectedItem: this.onSelectedTema})
                ), 

                React.createElement("article", {className: "width_half", style: {marginLeft: '5px', marginTop: '15px', marginBottom: '15px'}}, 
                  React.createElement("label", {style: {width:'100%'}}, "SubTemas:"), 
                  React.createElement(CheckTableReact, {ref: "subTemasChecked", arrElements: this.state.subTemasClone, elementId: "idSubTema", elementName: "descripcion"})
                ), 

                React.createElement("article", {className: "width_half", style: {marginTop: '15px'}}, 
                  React.createElement("label", {style: {width:'100%'}}, "Sectores:"), 
                  React.createElement(CheckTableReact, {ref: "sectoresChecked", arrElements: this.state.sectoresClone, elementId: "idSector", elementName: "descripcion", 
                    showTools: true, onSelectedItem: this.onSelectedSector})
                ), 

                React.createElement("article", {className: "width_half", style: {marginLeft: '5px', marginTop: '15px', marginBottom: '15px'}}, 
                  React.createElement("label", {style: {width:'100%'}}, "SubSectores:"), 
                  React.createElement(CheckTableReact, {ref: "subSectoresChecked", elementId: "idSubSector", elementName: "descripcion"})
                ), 

                React.createElement("article", {className: "width_1_4_quarter", style: {marginTop: '15px'}}, 
                  React.createElement("label", {style: {width:'100%'}}, "Tiempo a invertir:"), 
                  React.createElement(CheckTableReact, {ref: "tiemposChecked", arrElements: this.state.tiemposClone, elementId: "idTiempo", elementName: "descripcion"})
                ), 

                React.createElement("article", {className: "width_1_4_quarter", style: {marginLeft: '5px', marginTop: '15px'}}, 
                  React.createElement("label", {style: {width:'100%'}}, "Medios:"), 
                  React.createElement(CheckTableReact, {ref: "mediosChecked", arrElements: this.state.mediosClone, elementId: "idMedio", elementName: "descripcion"})
                ), 

                React.createElement("article", {className: "width_1_4_quarter", style: {marginLeft: '5px', marginTop: '15px'}}, 
                  React.createElement("label", {style: {width:'100%'}}, "Niveles:"), 
                  React.createElement(CheckTableReact, {ref: "nivelesChecked", arrElements: this.state.nivelesClone, elementId: "idNivel", elementName: "descripcion"})
                ), 

                React.createElement("article", {className: "width_1_4_quarter", style: {marginLeft: '5px', marginTop: '15px'}}, 
                  React.createElement("label", {style: {width:'100%'}}, "Inversión:"), 
                  React.createElement(CheckTableReact, {ref: "inversionesChecked", fileId: "0", arrElements: this.state.inversionesClone, elementId: "idInversion", elementName: "descripcion", 
                    onFileSelected: this.onFileSelected})
                )

              )
            )
          ), 

          React.createElement("div", {className: styleTabConfigArchivos}, 
            React.createElement("article", {className: "module width_full"}, 
              React.createElement("header", null, 
                React.createElement("div", {className: "AlignContainer"}, 
                  React.createElement("div", {className: "AlignLeft", style: {width: '95%'}}, 
                    React.createElement("h3", {className: "tabs_involved"}, "Configuración de acción")
                  ), 
                  React.createElement("div", {className: "AlignRight", style: {width: '5%', marginTop: '5px', textAlign: 'center'}}, 
                    React.createElement("a", {className: "IconNewStyle", href: "#", onClick: this.onClickNewArchivoAccion})
                  )
                )
              ), 
              React.createElement("div", {className: "module_content", style: {width: '98%'}}, 
                archivosAccion
              )
            )
          ), 

          React.createElement("footer", null, 
            React.createElement("div", {className: "submit_link"}, 
              React.createElement("input", {type: "button", value: "CANCELAR", className: "alt_btn", onClick: this.onClickCancelarActualizarAccion}), 
              React.createElement("input", {type: "button", value: "GUARDAR", onClick: this.onClickGuardarAccion})
            )
          )
        ), 
        React.createElement("div", {className: "spacer"})
      )
    );
  }
});

module.exports = ActualizarAcciones;
