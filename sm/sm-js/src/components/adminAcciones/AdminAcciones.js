'use strict';

var Router = require('../../libs/react-router/index.js');
/* # seccion para declarar utilerias # */
/* # seccion para declarar compronentes comunes # */
/* # seccion para declarar servicios # */
/* # Seccion de componentes a los que depende # */
var ConsultaAcciones = require('../../components/adminAcciones/ConsultaAcciones.js');
var AltaAcciones = require('../../components/adminAcciones/AltaAcciones.js');
var ActualizarAcciones = require('../../components/adminAcciones/ActualizarAcciones.js');
/*################################################*/
/*###       Administracion de acciones         ###*/
/*################################################*/
var AdminAcciones = React.createClass({displayName: "AdminAcciones",
  mixins: [ Router.Navigation ],
  getInitialState: function() {
    return {
      usuario: ctx.get('usuario'),
      shouldMount: true
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
    var self = this;

    if(validaSession()) {
      this.refs.consultaAcciones.show();
      this.refs.altaAcciones.hide();

    } else {
      this.setState({
        shouldMount: false
      });
      this.transitionTo('Login');
    }
  },
  componentWillReceiveProps: function(nextProps) {
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  onClickNewAccion: function() {
    this.refs.consultaAcciones.hide();
    this.refs.altaAcciones.reset();
    this.refs.altaAcciones.show();
  },
  onClickModifAccion: function(accion, index) {
    this.refs.consultaAcciones.hide();
    this.refs.actualizarAcciones.reset();
    this.refs.actualizarAcciones.setAccion(accion);
    this.refs.actualizarAcciones.show();
  },
  onClickCancelarAltaAccion: function() {
    this.refs.consultaAcciones.show();
  },
  onClickCancelarActualizarAccion: function() {
    this.refs.consultaAcciones.show();
  },
  onClickGuardarAccion: function() {
    this.refs.altaAcciones.reset();
    this.refs.altaAcciones.hide();
    this.refs.consultaAcciones.reset();
    this.refs.consultaAcciones.show();
  },
  onClickActualizarAccion: function() {
    this.refs.actualizarAcciones.reset();
    this.refs.actualizarAcciones.hide();
    this.refs.consultaAcciones.reset();
    this.refs.consultaAcciones.show();
  },
  render: function() {
    var self = this;

    if(!this.state.shouldMount) {
      return (React.createElement("div", null));
    }

    return (
      React.createElement("div", {style: {width: '100%', height: '100%'}}, 
        React.createElement(ConsultaAcciones, {ref: "consultaAcciones", onClickNewAccion: this.onClickNewAccion, onClickModifAccion: this.onClickModifAccion}), 
        React.createElement(AltaAcciones, {ref: "altaAcciones", onClickCancelarAltaAccion: this.onClickCancelarAltaAccion, onClickGuardarAccion: this.onClickGuardarAccion}), 
        React.createElement(ActualizarAcciones, {ref: "actualizarAcciones", onClickCancelarActualizarAccion: this.onClickCancelarActualizarAccion, onClickActualizarAccion: this.onClickActualizarAccion})
      )
    );
  }
});

module.exports = AdminAcciones;