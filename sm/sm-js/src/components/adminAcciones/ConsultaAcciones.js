'use strict';

/* # seccion para declarar utilerias # */
var Clone = require('../../utils/Clone.js');
var Context = require('../../utils/Context.js');
var Label = require('../../utils/Label.js');
var ErrorCodes = require('../../utils/ErrorCodes.js');
var ErrorMsg = require('../../utils/ErrorMsg.js');
var Constants = require('../../utils/Constants.js');
/* # seccion para declarar compronentes comunes # */
var PopUpReact = require('../../components/commons/PopUpReact.js');
/* # seccion para declarar servicios # */
var myTadminService = require('../../modulos/MyTAdminService.js');
var accionesService = require('../../modulos/AccionesService.js');
var validaService = require('../../utils/ValidaService.js');

/*#####################################*/
/*###       Consulta de accion      ###*/
/*#####################################*/
var ConsultaAcciones = React.createClass({displayName: "ConsultaAcciones",
  getInitialState: function() {
    return {
      usuario: ctx.get('usuario'),
      shouldMount: true,
      pagina: 0,
      resultPerPagina: 10,
      accionesOrg: undefined,
      acciones: undefined,
      catTemas: undefined,
      catSubTemas: undefined,
      catGeneralAcciones: undefined,
      idTema: 0,
      idSubTema: 0,
      idGeneralAccion: 0,
      showComponent: false,
      nombreAccion: ''
    };
  },
  getDefaultProps: function() {
    return {
      
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
    var self = this;
    //crear un unico objeto para almacenar de forma temporal informacion
    this.storage = new Context();

    var onSuccess = function(res) {
      self.setState({
        catTemas: res.catTemas,
        catGeneralAcciones: res.catGeneralAcciones
      });
    };

    if(validaSession()) {
      var params = {};
      accionesService.getCatAdminAcciones(params, onSuccess, this.onError, this.onFail);

    } else {
      this.setState({
        shouldMount: false
      });
    }
  },
  componentWillReceiveProps: function(nextProps) {
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  onFail: function(err) {
    console.log(err);
    this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
  },
  onError: function(err) {
    console.log(err);

    switch(err.errorCode) {
      case ErrorCodes.ERR_01:
        this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
      break;

      case ErrorCodes.ERR_03:
        this.refs.popUpAlert.show(ErrorCodes.ERR_03_MSG);
      break;

      default:
        console.log('Error no categorizado...');
      break;
    }
  },
  show: function() {
    this.setState({
      showComponent: true
    });
  },
  hide: function() {
    this.setState({
      showComponent: false
    });
  },
  reset: function() {
    this.setState({
      shouldMount: true,
      pagina: 0,
      resultPerPagina: 10,
      accionesOrg: undefined,
      acciones: undefined,
      idTema: 0,
      idSubTema: 0,
      idGeneralAccion: 0,
      nombreAccion: ''
    });
  },
  onClickLimpiar: function() {
    this.setState({
      pagina: 0,
      resultPerPagina: 10,
      accionesOrg: undefined,
      acciones: undefined,
      catSubTemas: undefined,
      idTema: 0,
      idSubTema: 0,
      idGeneralAccion: 0,
      nombreAccion: ''
    });
  },
  getPaginado: function(lista, resultPerPagina) {
    var paginado = undefined;
    var pagina = undefined;
    var i = 0;
    var self = this;

    if(lista != undefined && lista.length > 0) {
      paginado = [];
      pagina = [];

      lista.forEach(function(item, index) {
        pagina.push(item);
        
        if(i < resultPerPagina) {
          i++
        } 

        if(i == resultPerPagina || index == (lista.length - 1)) {
          paginado.push(pagina);
          pagina = [];
          i = 0;
        }
      });
    }

    return paginado;
  },
  onClickBuscar: function() {
    var self = this;

    var onSuccess = function(res) {
      if(res.acciones == undefined || res.acciones.length == 0) {
        self.refs.popUpAlert.show(Label.NO_RESULTADOS);
      }

      self.setState({
        pagina: 0,
        accionesOrg: res.acciones,
        acciones: self.getPaginado(res.acciones, self.state.resultPerPagina)
      });
    };

    var params = {
      filtroAccion: {
        idTema: this.state.idTema,
        idSubTema: this.state.idSubTema,
        idGeneralAccion: this.state.idGeneralAccion,
        nombreAccion: this.state.nombreAccion
      }
    };
    accionesService.getAccionesByFiltro(params, onSuccess, this.onError, this.onFail);
  },
  onChangeTema: function(evt) {
    var self = this;

    this.setState({
      idTema: evt.target.value,
      catSubTemas: undefined
    });

    var onSuccess = function(res) {
      self.setState({
        catSubTemas: res.catSubTemas
      });
    };

    if(evt.target.value > 0) {
      var params = {
        idTema: evt.target.value
      };
      myTadminService.getCatSubTemasByIdTema(params, onSuccess, this.onError, this.onFail);
    }
  },
  onChangeSubTema: function(evt) {
    this.setState({
      idSubtema: evt.target.value
    });
  },
  onChangeGeneralAccion: function(evt) {
    this.setState({
      idGeneralAccion: evt.target.value
    });
  },
  onClickNextPage: function(evt) {
    evt.preventDefault();
    var nextPage = 0;

    if(this.state.acciones != undefined) {
      nextPage = (this.state.pagina + 1);

      if(nextPage <= (this.state.acciones.length - 1) ) {
        this.setState({
          pagina: nextPage
        });  
      }
    }
  },
  onClickBackPage: function(evt) {
    evt.preventDefault();
    var backPage = 0;
    var firstPage = 0;

    if(this.state.acciones != undefined) {
      backPage = (this.state.pagina - 1);

      if(backPage >= firstPage) {
        this.setState({
          pagina: backPage
        });  
      }
    }
  },
  onClickFirstPage: function(evt) {
    evt.preventDefault();
    var firstPage = 0;

    if(this.state.acciones != undefined) {
      if(firstPage != this.state.pagina) {
        this.setState({
          pagina: firstPage
        });  
      }
    }
  },
  onClickLastPage: function(evt) {
    evt.preventDefault();
    var lastPage = 0;

    if(this.state.acciones != undefined) {
      lastPage = (this.state.acciones.length - 1);

      if(lastPage != this.state.pagina) {
        this.setState({
          pagina: lastPage
        });  
      }
    }
  },
  onChangeResultPerPagina: function(evt) {
    if(this.state.accionesOrg != undefined) {
      this.setState({
        pagina: 0,
        resultPerPagina: evt.target.value,
        acciones: this.getPaginado(this.state.accionesOrg, evt.target.value)
      });
    }
  },
  onClickIrEditar: function(accion, index, evt) {
    evt.preventDefault();
    this.props.onClickModifAccion(accion, index);
  },
  onChangeActivo: function(accion, index, evt) {
    evt.preventDefault();
    this.storage.clean();

    accion.index = index;
    this.storage.put('ACCION_TO_UPDATE', Clone.clone(accion));
    if(accion.activo) {
      //preguntar por desactivar
      this.refs.popUpAskActivo.show(Label.ASK_DESACTIVAR_ACCION + accion.descripcion + ' ?');

    } else {
      //preguntar por activas
      this.refs.popUpAskActivo.show(Label.ASK_ACTIVAR_ACCION + accion.descripcion + ' ?');
    }
  },
  onClickAceptarActivo: function(evt) {
    var self = this;
    var accion = this.storage.get('ACCION_TO_UPDATE');
    var activoDesactivo = true;

    var onSuccess = function(res) {
      //estableser todos los parametros a un estado normal
      self.recargaAcciones(accion);
      self.storage.clean();
      self.refs.popUpAskActivo.hide();
      self.refs.popUpAlert.show(Label.REGISTRO_MODIF_SUCCESS);
    };

    if(accion.activo) {
      accion.activo = false;

    } else {
      accion.activo = true;
    }

    var params = {
      idAccion: accion.idAccion,
      activoDesactivo: accion.activo
    };
    accionesService.changeEstatusAccion(params, onSuccess, this.onError, this.onFail);
  },
  onClickCancelarActivo: function(evt) {
    this.refs.popUpAskActivo.hide();
    this.storage.clean();
  },
  recargaAcciones: function(accion) {
    this.state.acciones[this.state.pagina][accion.index].activo = accion.activo;

    this.setState({
      acciones: this.state.acciones
    });
  },
  onClickNewAccion: function(evt) {
    evt.preventDefault();
    this.props.onClickNewAccion();
  },
  onChangeNombreAccion: function(evt) {
    var nombreAccion = this.state.nombreAccion;
    nombreAccion = evt.target.value;

    this.setState({
      nombreAccion: nombreAccion
    });
  },
  render: function() {
    var self = this;
    var catTemasHTML = [];
    var catSubTemasHTML =[];
    var catGeneralAccionesHTML = [];
    var accionesHTML = [];
    var totalPaginado = '0 de 0 Páginas';
    var showStyle = 'componentShow';
    var hideStyle = 'componentHide';
    var componentStyle = hideStyle;

    if(!this.state.shouldMount || !this.state.showComponent) {
      return (React.createElement("div", null));
    }
    //construr temas
    if(this.state.catTemas != undefined) {
      this.state.catTemas.forEach(function(tema) {
        catTemasHTML.push(React.createElement("option", {value: tema.idTema}, tema.descripcion));
      });
    }
    //construr subtemas
    if(this.state.catSubTemas != undefined) {
      this.state.catSubTemas.forEach(function(subTema) {
        catSubTemasHTML.push(React.createElement("option", {value: subTema.idSubTema}, subTema.descripcion));
      });
    }
    //construr general de accion
    if(this.state.catGeneralAcciones != undefined) {
      this.state.catGeneralAcciones.forEach(function(generalAccion) {
        catGeneralAccionesHTML.push(React.createElement("option", {value: generalAccion.idGeneralAccion}, generalAccion.nombre));
      });
    }
    //construir las acciones
    if(this.state.acciones != undefined) {
      totalPaginado = ((this.state.pagina + 1) + ' de ' +  this.state.acciones.length + ' Páginas')
      this.state.acciones[this.state.pagina].forEach(function(accion, index) {
        var onClickIrEditar = function(accion, index, evt) {
          self.onClickIrEditar(accion, index, evt);
        }.bind(self, accion, index);

        var onChangeActivo = function(accion, index, evt) {
          self.onChangeActivo(accion, index, evt);
        }.bind(self, accion, index);

        accionesHTML.push(
          React.createElement("tr", null, 
            React.createElement("td", null, accion.descripcion), 
            React.createElement("td", null, 
              React.createElement("a", {className: "IconEditStyle", href: "#", onClick: onClickIrEditar}), 
              React.createElement("label", {className: "switch switch-green"}, 
                React.createElement("input", {type: "checkbox", className: "switch-input", checked: accion.activo, onChange: onChangeActivo}), 
                React.createElement("span", {className: "switch-label", "data-on": "On", "data-off": "Off"}), 
                React.createElement("span", {className: "switch-handle"})
              )
            )
          ) 
        );
      });
    }

    if(this.state.showComponent) {
      componentStyle = showStyle;

    } else {
      componentStyle = hideStyle;
    }

    return (
      React.createElement("div", {id: "container-gral", className: componentStyle}, 
        React.createElement(PopUpReact, {ref: "popUpAlert", type: "alert", popUpStyle: "PopUpReactAlert"}), 
        React.createElement(PopUpReact, {ref: "popUpAskActivo", type: "ask", 
          buttons: [
            {name: 'Aceptar', label: 'Aceptar', onClick: this.onClickAceptarActivo},
            {name: 'Cancelar', label: 'Cancelar', onClick: this.onClickCancelarActivo}
        ]}), 

        React.createElement("article", {className: "module width_full"}, 
          React.createElement("header", null, React.createElement("h3", null, "Filtros de Búsqueda")), 
          React.createElement("div", {className: "module_content"}, 
            React.createElement("fieldset", {style: {width:'48%', float:'left', marginRight: '3%'}}, 
              React.createElement("label", null, "Tema:"), 
              React.createElement("select", {style: {width:'92%'}, value: this.state.idTema, onChange: this.onChangeTema}, 
                React.createElement("option", {value: "0"}, "Todos"), 
                catTemasHTML
              )
            ), 
            React.createElement("fieldset", {style: {width:'48%', float:'left'}}, 
              React.createElement("label", null, "Subtema:"), 
              React.createElement("select", {style: {width:'92%'}, value: this.state.idSubTema, onChange: this.onChangeSubTema}, 
                React.createElement("option", {value: "0"}, "Todos"), 
                catSubTemasHTML
              )
            ), 
            React.createElement("fieldset", {style: {width:'48%', float:'left', marginRight: '3%'}}, 
              React.createElement("label", null, "General Acción:"), 
              React.createElement("select", {style: {width:'92%'}, value: this.state.idGeneralAccion, onChange: this.onChangeGeneralAccion}, 
                React.createElement("option", {value: "0"}, "Todos"), 
                catGeneralAccionesHTML
              )
            ), 
            React.createElement("fieldset", {style: {width:'48%', float:'left'}}, 
              React.createElement("label", null, "Nombre:"), 
              React.createElement("input", {type: "text", style: {width:'92%'}, maxLength: "340", placeholder: "Nombre de la acción", value: this.state.nombreAccion, onChange: this.onChangeNombreAccion})
            ), 
            React.createElement("div", {className: "clear"})
          ), 
          React.createElement("footer", null, 
            React.createElement("div", {className: "submit_link"}, 
              React.createElement("input", {type: "button", value: "BUSCAR", className: "alt_btn", onClick: this.onClickBuscar}), 
              React.createElement("input", {type: "button", value: "LIMPIAR", onClick: this.onClickLimpiar})
            )
          )
        ), 
        
        React.createElement("article", {className: "module width_full"}, 
          React.createElement("header", null, 
            React.createElement("div", {className: "AlignContainer"}, 
              React.createElement("div", {className: "AlignLeft", style: {width: '95%'}}, 
                React.createElement("h3", {className: "tabs_involved"}, "Lista de acciones")
              ), 
              React.createElement("div", {className: "AlignRight", style: {width: '5%', marginTop: '5px', textAlign: 'center'}}, 
                React.createElement("a", {className: "IconNewStyle", href: "#", onClick: this.onClickNewAccion})
              )
            )
          ), 
          React.createElement("div", {className: "tab_container"}, 
            React.createElement("div", {id: "tab1", className: "tab_content"}, 
              React.createElement("table", {className: "tablesorter", cellSpacing: "0"}, 
                React.createElement("thead", null, 
                  React.createElement("tr", null, 
                    React.createElement("th", {style: {width: '90%'}}, "Nombre"), 
                    React.createElement("th", {style: {width: '10%'}})
                  )
                ), 
                React.createElement("tbody", null, 
                  accionesHTML
                ), 
                React.createElement("tfoot", null, 
                  React.createElement("tr", null, 
                    React.createElement("td", {colSpan: "100%"}, 
                      React.createElement("div", {id: "pag"}, 
                        React.createElement("select", {id: "soflow-color", value: this.state.resultPerPagina, onChange: this.onChangeResultPerPagina}, 
                          React.createElement("option", {value: "5"}, "5"), 
                          React.createElement("option", {value: "10"}, "10"), 
                          React.createElement("option", {value: "15"}, "15"), 
                          React.createElement("option", {value: "20"}, "20"), 
                          React.createElement("option", {value: "25"}, "25")
                        ), 
                        React.createElement("ul", {id: "pagination"}, 
                          React.createElement("li", {className: "previous"}, React.createElement("a", {href: "#", onClick: this.onClickFirstPage}, "<< Primero")), 
                          React.createElement("li", {className: "previous"}, React.createElement("a", {href: "#", onClick: this.onClickBackPage}, "< Anterior")), 
                          React.createElement("li", {className: "active"}, totalPaginado), 
                          React.createElement("li", {className: "next"}, React.createElement("a", {href: "#", onClick: this.onClickNextPage}, "Siguiente >")), 
                          React.createElement("li", {className: "next"}, React.createElement("a", {href: "#", onClick: this.onClickLastPage}, "Último >>"))
                        )
                      )
                    )
                  )
                )
              )
            )
          )
        ), 
        React.createElement("div", {className: "spacer"})
      )
    );
  }
});

module.exports = ConsultaAcciones;