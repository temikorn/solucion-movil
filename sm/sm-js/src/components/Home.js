'use strict';

var Router = require('../libs/react-router/index.js');
var RouteHandler = require('../libs/react-router/components/RouteHandler.js');
var Label = require('../utils/Label.js');
var Constants = require('../utils/Constants.js');

var Home = React.createClass({displayName: "Home",
  mixins: [ Router.Navigation ],
 	getInitialState: function() {
    return {
      usuario: ctx.get('usuario'),
      shouldMount: true, 
      menuActivo: false,
      sectionTitle: Label.TITLE_ESTADISTICAS_VISITANTE,
      menuItemSelected: Constants.MenuItems.TITLE_ESTADISTICAS_VISITANTE
    };
  },
  componentWillMount: function() {
    if(!validaSession()) {
      this.setState({
        shouldMount: false
      });
      this.transitionTo('Login');
    }
  },
  componentDidMount: function() {
    if(!validaSession()) {
      this.setState({
        shouldMount: false
      });
      this.transitionTo('Login');
    }
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillReceiveProps: function(nextProps) {
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
    resetApp();
  },
  onClickEstadisticasSatisfaccion: function(evt) {
    evt.preventDefault();
    this.setState({
      sectionTitle: Label.TITLE_ESTADISTICAS_SATISFACCION,
      menuItemSelected: Constants.MenuItems.ESTADISTICAS_SATISFACCION
    });
    this.transitionTo('EstadisticasSatisfaccion');
  },
  onClickControlCierreAccion: function(evt) {
    evt.preventDefault();
    this.setState({
      sectionTitle: Label.TITLE_CONTROL_CIERRE,
      menuItemSelected: Constants.MenuItems.CONTROL_CIERRE
    });
    this.transitionTo('ControlCierreAcciones');
  },
  onClickConfigParametros: function(evt) {
    evt.preventDefault();
    this.setState({
      sectionTitle: Label.TITLE_CONFIG_PARAMETROS,
      menuItemSelected: Constants.MenuItems.CONFIG_PARAMETROS
    });
    this.transitionTo('ConfigParametros');
  },
  onClickAdminGeneralAccion: function(evt) {
    evt.preventDefault();
    this.setState({
      sectionTitle: Label.TITLE_ADMIN_GENERAL_ACCION,
      menuItemSelected: Constants.MenuItems.ADMIN_GENERAL_ACCION
    });
    this.transitionTo('AdminGeneralAccion');
  },
  onClickAdminLlamadoAccion: function(evt) {
    evt.preventDefault();
    this.setState({
      sectionTitle: Label.TITLE_ADMIN_LLAMADO_ACCION,
      menuItemSelected: Constants.MenuItems.ADMIN_LLAMADO_ACCION
    });
    this.transitionTo('AdminLlamadoAccion');
  },
  onClickAdminAcciones: function(evt) {
    evt.preventDefault();
    this.setState({
      sectionTitle: Label.TITLE_ADMIN_ACCIONES,
      menuItemSelected: Constants.MenuItems.ADMIN_ACCIONES
    });
    this.transitionTo('AdminAcciones');
  },
  onClickAdminVisitantes: function(evt) {
    evt.preventDefault();
    this.setState({
      sectionTitle: Label.TITLE_ADMIN_VISITANTES,
      menuItemSelected: Constants.MenuItems.ADMIN_VISITANTES
    });
    this.transitionTo('AdminVisitantes');
  },
  onClickHome: function(evt) {
    this.setState({
      sectionTitle: Label.TITLE_ESTADISTICAS_VISITANTE,
      menuItemSelected: Constants.MenuItems.ESTADISTICAS_VISITANTE
    });
  },
  onClickMenuItem: function(evt) {
    evt.preventDefault();
  },
  render: function() {
    var nombreUsuario = this.state.usuario.getNombre();
    var popUpLoadingHide = 'componentHide';
    var MENU_ITEM_ACTIVO = 'activoMenu';
    var MENU_ITEM_NO_ACTIVO = '';
    var styleActiveEstadisticaVisitante = MENU_ITEM_NO_ACTIVO;
    var styleActiveEstadisticaSatisfaccion = MENU_ITEM_NO_ACTIVO;
    var styleActiveConfigParametros = MENU_ITEM_NO_ACTIVO;
    var styleActiveControlCierre = MENU_ITEM_NO_ACTIVO;
    var styleActiveAdminGeneralAccion = MENU_ITEM_NO_ACTIVO;
    var styleActiveAdminAcciones = MENU_ITEM_NO_ACTIVO;
    var styleActiveAdminVisitantes = MENU_ITEM_NO_ACTIVO;
    var styleActiveAdminLlamadoAccion = MENU_ITEM_NO_ACTIVO;

    if(!this.state.shouldMount) {
      return (React.createElement("div", null));
    }

    switch(this.state.menuItemSelected) {
      case Constants.MenuItems.ESTADISTICAS_VISITANTE:
        styleActiveEstadisticaVisitante = MENU_ITEM_ACTIVO;
      break;

      case Constants.MenuItems.ESTADISTICAS_SATISFACCION:
        styleActiveEstadisticaSatisfaccion = MENU_ITEM_ACTIVO;
      break;

      case Constants.MenuItems.CONTROL_CIERRE:
        styleActiveControlCierre = MENU_ITEM_ACTIVO;
      break;

      case Constants.MenuItems.CONFIG_PARAMETROS:
        styleActiveConfigParametros = MENU_ITEM_ACTIVO;
      break;

      case Constants.MenuItems.ADMIN_GENERAL_ACCION:
        styleActiveAdminGeneralAccion = MENU_ITEM_ACTIVO;
      break;

      case Constants.MenuItems.ADMIN_ACCIONES:
        styleActiveAdminAcciones = MENU_ITEM_ACTIVO;
      break;

      case Constants.MenuItems.ADMIN_VISITANTES:
        styleActiveAdminVisitantes = MENU_ITEM_ACTIVO;
      break;

      case Constants.MenuItems.ADMIN_LLAMADO_ACCION:
        styleActiveAdminLlamadoAccion = MENU_ITEM_ACTIVO;
      break;
    }

    return (
      React.createElement("div", {id: "home", style: {width:'100%', height:'100%'}}, 
        React.createElement("div", {id: "backgroundEspecial", className: popUpLoadingHide}, 
          React.createElement("span", {id: "loading"})
        ), 

        React.createElement("header", {id: "header"}, 
          React.createElement("hgroup", null, 
            React.createElement("h1", {className: "site_title"}, React.createElement("a", {href: "#/home", onClick: this.onClickHome}, Label.SITE_TITLE)), 
            React.createElement("h2", {className: "section_title"}, this.state.sectionTitle), React.createElement("div", {className: "btn_view_site"}, React.createElement("a", {href: "#/"}, "Salir"))
          )
        ), 
    
        React.createElement("section", {id: "secondary_bar"}, 
          React.createElement("div", {className: "user"}, 
            React.createElement("p", null, nombreUsuario)
          ), 
          React.createElement("div", {className: "breadcrumbs_container"}, 
            React.createElement("article", {className: "breadcrumbs"}, 
              React.createElement("a", {href: "#/home", onClick: this.onClickHome}, "Inicio"), 
              React.createElement("div", {className: "breadcrumb_divider"}), 
              React.createElement("a", {className: "current"}, this.state.sectionTitle)
            )
          )
        ), 

        React.createElement("aside", {id: "sidebar", className: "column"}, 
          React.createElement("h3", null, "Menú"), 
          React.createElement("ul", {className: "toggle"}, 
            React.createElement("li", {className: "icn_graphic_visitor"}, 
              React.createElement("a", {href: "#/home", onClick: this.onClickHome, className: styleActiveEstadisticaVisitante}, 
                Label.TITLE_ESTADISTICAS_VISITANTE
              )
            ), 
            React.createElement("li", {className: "icn_graphic_satisfaction"}, 
              React.createElement("a", {href: "#", onClick: this.onClickEstadisticasSatisfaccion, className: styleActiveEstadisticaSatisfaccion}, 
                Label.TITLE_ESTADISTICAS_SATISFACCION
              )
            ), 
            React.createElement("li", {className: "icn_change_action"}, 
              React.createElement("a", {href: "#", onClick: this.onClickControlCierreAccion, className: styleActiveControlCierre}, 
                Label.TITLE_CONTROL_CIERRE
              )
            ), 
            React.createElement("li", {className: "icn_tool_action"}, 
              React.createElement("a", {href: "#", onClick: this.onClickConfigParametros, className: styleActiveConfigParametros}, 
                Label.TITLE_CONFIG_PARAMETROS
              )
            ), 
            React.createElement("li", {className: "icn_tool_action"}, 
              React.createElement("a", {href: "#", onClick: this.onClickAdminGeneralAccion, className: styleActiveAdminGeneralAccion}, 
                Label.TITLE_ADMIN_GENERAL_ACCION
              )
            ), 
            React.createElement("li", {className: "icn_tool_action"}, 
              React.createElement("a", {href: "#", onClick: this.onClickAdminAcciones, className: styleActiveAdminAcciones}, 
                Label.TITLE_ADMIN_ACCIONES
              )
            ), 
            React.createElement("li", {className: "icn_tool_action"}, 
              React.createElement("a", {href: "#", onClick: this.onClickAdminVisitantes, className: styleActiveAdminVisitantes}, 
                Label.TITLE_ADMIN_VISITANTES
              )
            ), 
            React.createElement("li", {className: "icn_tool_action"}, 
              React.createElement("a", {href: "#", onClick: this.onClickAdminLlamadoAccion, className: styleActiveAdminLlamadoAccion}, 
                Label.TITLE_ADMIN_LLAMADO_ACCION
              )
            )
          ), 
          
          React.createElement("footer", null, 
            React.createElement("hr", null), 
            React.createElement("p", null, React.createElement("strong", null, "Copyright © 2015 Administración MyT "))
          )
        ), 
    
        React.createElement("section", {id: "main", className: "column"}, 
          React.createElement(RouteHandler, null)
        )
      )
    );
  }
});

module.exports = Home;