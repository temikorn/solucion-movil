var Config = React.createClass({displayName: "Config",
    getInitialState: function() {
        return ctx.get("masterDatos").getData();
    },
    changeTiempo: function(e) {
        var data = ctx.get("masterDatos");
        data.setData(e.target.value, data.getData().precio);
        localStorage.setItem("tiempo", data.getData().tiempo);
        localStorage.setItem("precio", data.getData().precio); 
    },
    changePrecio: function(e) {
        var data = ctx.get("masterDatos");
        data.setData(data.getData().tiempo, e.target.value);
        localStorage.setItem("tiempo", data.getData().tiempo);
        localStorage.setItem("precio", data.getData().precio); 
    },
    render: function() {
        return (
            React.createElement("section", {id: "config"}, 
                React.createElement("div", {className: "row"}, 
                    React.createElement("div", {className: "clearfix visible-xs-block separador"}), 
                    React.createElement("div", {className: "col-xs-12"}, 
                        React.createElement("span", {className: "tituloInput"}, "Tiempo:")
                    ), 
                    React.createElement("div", {className: "col-xs-12"}, 
                        React.createElement("input", {type: "number", defaultValue: this.state.tiempo, onChange: this.changeTiempo})
                    ), 
                    React.createElement("div", {className: "clearfix visible-xs-block separador"}), 
                    React.createElement("div", {className: "col-xs-12"}, 
                        React.createElement("span", {className: "tituloInput"}, "Precio:")
                    ), 
                    React.createElement("div", {className: "col-xs-12"}, 
                        React.createElement("input", {type: "number", defaultValue: this.state.precio, onChange: this.changePrecio})
                    )
                )
            )
        );
    }
});

module.exports = Config;