'use strict';

/* # seccion para declarar utilerias # */
var Clone = require('../../utils/Clone.js');
var Constants = require('../../utils/Constants.js');
var ErrorCodes = require('../../utils/ErrorCodes.js');
/* # seccion para declarar compronentes comunes # */
var PopUpReact = require('../../components/commons/PopUpReact.js');
/* # seccion para declarar servicios # */
/* # Seccion de componentes a los que depende # */
var CargaVisitantes = require('../../components/adminVisitantes/CargaVisitantes.js');
var BusquedaVisitantes = require('../../components/adminVisitantes/BusquedaVisitantes.js');
var AgendaVisitantes = require('../../components/adminVisitantes/AgendaVisitantes.js');

/*################################################*/
/*###       Administración de visitantes       ###*/
/*################################################*/
var AdminVisitantes = React.createClass({displayName: "AdminVisitantes",
  getInitialState: function() {
    return {
      shouldMount: true,
      showComponent: false,
      tabSelected: 1,
    };
  },
  getDefaultProps: function() {
    return {
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
    var self = this;

    if(validaSession()) {

    } else {
      this.setState({
        shouldMount: false
      });
    }
  },
  componentWillReceiveProps: function(nextProps) {
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  onFail: function(err) {
    console.log(err);
    this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
  },
  onError: function(err) {
    console.log(err);
    
    switch(err.errorCode) {
      case ErrorCodes.ERR_01:
        this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
      break;

      case ErrorCodes.ERR_03:
        this.refs.popUpAlert.show(ErrorCodes.ERR_03_MSG);
      break;

      default:
        console.log('Error no categorizado...');
      break;
    } 
  },
  reset: function() {
    this.setState({
      tabSelected: 1,
    });
  },
  show: function() {
    this.setState({
      showComponent: true,
      tabSelected: 1
    });
  },
  hide: function() {
    this.setState({
      showComponent: false,
      tabSelected: 1
    });
  },
  onClickTab: function(tabSelected, evt) {
    switch(tabSelected) {
      case Constants.ADMIN_VISITANTES.TAB_CARGA_VISITANTES:
      break;

      case Constants.ADMIN_VISITANTES.TAB_BUSQUEDA_VISITANTES:
      break;

      case Constants.ADMIN_VISITANTES.TAB_AGENDA_VISITANTES:
      break;
    }

    this.setState({
      tabSelected: tabSelected
    });
  },
  render: function() {
    var self = this;
    var componentShow = 'componentShow';
    var componentHide = 'componentHide';
    var styleTabContent = 'tab_content';

    var activeTabCargaVisitantes = '';
    var styleTabCargaVisitantes = componentHide;
    var onTabCargaVisitantes = undefined;

    var activeTabBusquedaVisitantes = '';
    var styleTabBusquedaVisitantes = componentHide;
    var onTabBusquedaVisitantes = undefined;

    var activeTabAgendaVisitantes = '';
    var styleTabAgendaVisitantes = componentHide;
    var onTabAgendaVisitantes = undefined;

    if(!this.state.shouldMount) {
      return (React.createElement("div", null));
    }
    //que hacer dependiendo del tab seleccionado
    switch(this.state.tabSelected) {
      case Constants.ADMIN_VISITANTES.TAB_CARGA_VISITANTES:
        styleTabCargaVisitantes = (styleTabContent + ' ' + componentShow);
        activeTabCargaVisitantes = 'active';
      break;

      case Constants.ADMIN_VISITANTES.TAB_BUSQUEDA_VISITANTES:
        styleTabBusquedaVisitantes = (styleTabContent + ' ' + componentShow);
        activeTabBusquedaVisitantes = 'active';
      break;

      case Constants.ADMIN_VISITANTES.TAB_AGENDA_VISITANTES:
        styleTabAgendaVisitantes = (styleTabContent + ' ' + componentShow);
        activeTabAgendaVisitantes = 'active';
      break;
    }
    //crear acciones para los tab existentes
    onTabCargaVisitantes = function(tabSelected, evt) {
      self.onClickTab(tabSelected, evt);
    }.bind(self, Constants.ADMIN_VISITANTES.TAB_CARGA_VISITANTES);

    onTabBusquedaVisitantes = function(tabSelected, evt) {
      self.onClickTab(tabSelected, evt);
    }.bind(self, Constants.ADMIN_VISITANTES.TAB_BUSQUEDA_VISITANTES);

    onTabAgendaVisitantes = function(tabSelected, evt) {
      self.onClickTab(tabSelected, evt);
    }.bind(self, Constants.ADMIN_VISITANTES.TAB_AGENDA_VISITANTES);
    //#####################  
    return (
      React.createElement("div", {id: "container-gral"}, 
        React.createElement(PopUpReact, {ref: "popUpAlert", type: "alert", popUpStyle: "PopUpReactAlert"}), 

        React.createElement("nav", null, 
          React.createElement("ul", {id: "menuNav"}, 
            React.createElement("li", {className: activeTabCargaVisitantes, style: {width: '33%'}, onClick: onTabCargaVisitantes}, "Carga de Visitantes"), 
            React.createElement("li", {className: activeTabBusquedaVisitantes, style: {width: '33%'}, onClick: onTabBusquedaVisitantes}, "Búsqueda de visitantes"), 
            React.createElement("li", {className: activeTabAgendaVisitantes, style: {width: '33%'}, onClick: onTabAgendaVisitantes}, "Agenda de visitantes")
          )
        ), 
        
        React.createElement("div", {className: "tab_container"}, 

          React.createElement("div", {className: styleTabCargaVisitantes}, 
            React.createElement("article", {className: "module width_full"}, 
              React.createElement(CargaVisitantes, {ref: "cargaVisitantes"})
            )
          ), 

          React.createElement("div", {className: styleTabBusquedaVisitantes}, 
            React.createElement(BusquedaVisitantes, {ref: "busquedaVisitantes"})
          ), 
          
          React.createElement("div", {className: styleTabAgendaVisitantes}, 
            React.createElement(AgendaVisitantes, {ref: "agendaVisitantes"})
          )

        ), 
        React.createElement("div", {className: "spacer"})
      )
    );
  }
});

module.exports = AdminVisitantes;
