'use strict';

var Router = require('../libs/react-router/index.js');
var Label = require('../utils/Label.js');
var adminService = require('../modulos/MyTAdminService.js');
var ErrorCodes = require('../utils/ErrorCodes.js');
var ComponentState = require('../utils/ComponentState.js');
var Constants = require('../utils/Constants.js');
var Context = require('../utils/Context.js');
var ValidaService = require('../utils/ValidaService.js');
var PopUpReact = require('../components/commons/PopUpReact.js');
var DatePickerReact = require('../components/commons/DatePickerReact.js');
var highCharts = require('../utils/HighCharts.js');

var EstadisticasVisitante = React.createClass({displayName: "EstadisticasVisitante",
  mixins: [ Router.Navigation ],
 	getInitialState: function() {
    return {
      shouldMount: true,
      componentState: ComponentState.NORMAL_STATE,
      filtro: {
        idSector: 0,
        idSubSector: 0,
        idCumplimiento: 0,
        edadMin: 0,
        edadMax:0,
        idMedio: 0,
        idTema: 0,
        idSubTema: 0,
        idNivel: 0,
        idInversion: 0,
        idAccion: 0,
        idTiempo: 0,
        idOtraAccion: 0,
        idEstatusAccion: 0,
        fechaInicio: new Date(),
        fechaFinal: new Date(),
        emailCorrecto: false,
        emailIncorrecto: false,
        emailVacio: false
      },
      pagina: 0,
      resultPerPagina: 10,
      estadisticasOrg: undefined,
      estadisticas: undefined,
      totalResult: 0,
      porcentajeResult: 0.0,
      totalCompromisos: 0,
      catalogos: undefined,
      subSectores: undefined,
      subTemas: undefined,
      acciones: undefined,
      styleTabSubTotalShow: true,
      styleTabResultShow: false,
      styleTabGraficsShow: false,
      subEstadisticaSector: undefined,
      subEstadisticaSubSector: undefined,
      subEstadisticaTiempos: undefined,
      subEstadisticaMedios: undefined,
      subEstadisticaEstatus: undefined,
      subEstadisticaTemas: undefined,
      subEstadisticaSubTemas: undefined,
      subEstadisticaAcciones: undefined,
      subEstadisticaNiveles: undefined,
      subEstadisticaInversiones: undefined,
      subEstadisticaCumplimientos: undefined,
      subEstadisticaOtraAccion: undefined,
      fechaInicioBusqueda: undefined,
      fechaFinalBusqueda: undefined,
      totalCorrectos: 0,
      totalIncorrectos: 0,
      totalVacios: 0,
      isResultBusqueda: false,
      contNoComprometidos: 0
    };
  },
  getDefaultProps: function() {
    return {
      storage: new Context()
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
    var self = this;

    this.props.storage.put('graficas', []);
    this.props.storage.put('graficas64', []);

    var onSuccess = function(res) {
      var catalogos = {};
      catalogos.parametros = res.parametros;
      catalogos.temas = res.temas;
      catalogos.sectores = res.sectores;
      catalogos.tiempos = res.tiempos;
      catalogos.medios = res.medios;
      catalogos.niveles = res.niveles;
      catalogos.inversiones = res.inversiones;
      catalogos.cumplimientos = res.cumplimientos;
      catalogos.estatusCompromisos = res.estatusCompromisos;
      catalogos.acciones = res.acciones;

      self.setState({
        componentState: ComponentState.NORMAL_STATE,
        catalogos: catalogos
      });
    };

    if(validaSession()) {
      var params = {};
      adminService.getCatalogos(params, onSuccess, this.onError, this.onFail);

    } else {
      this.setState({
        shouldMount: false
      });
      this.transitionTo('Login');
    }
  },
  componentWillReceiveProps: function(nextProps) {
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
    this.reflowGrafics();
  },
  componentWillUnmount: function() {
  },
  onFail: function(err) {
    console.log(err);
    this.setState({
      componentState: ComponentState.ERROR_STATE
    });
    this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
  },
  onError: function(err) {
    console.log(err);
    
    switch(err.errorCode) {
      case ErrorCodes.ERR_01:
        this.setState({
          componentState: ComponentState.ERROR_STATE
        });
        this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
      break;

      case ErrorCodes.ERR_03:
        this.setState({
          componentState: ComponentState.ERROR_STATE
        });
        this.refs.popUpAlert.show(ErrorCodes.ERR_03_MSG);
      break;

      default:
        console.log('Error no categorizado...');
      break;
    } 
  },
  onClickLimpiar: function() {
    this.destroyGrafics();
    this.refs.fechaInicio.reset();
    this.refs.fechaFinal.reset();
    this.setState({
      componentState: ComponentState.NORMAL_STATE,
      filtro: {
        idSector: 0,
        idSubSector: 0,
        idCumplimiento: 0,
        edadMin: 0,
        edadMax:0,
        idMedio: 0,
        idTema: 0,
        idSubTema: 0,
        idNivel: 0,
        idInversion: 0,
        idAccion: 0,
        idTiempo: 0,
        idOtraAccion: 0,
        idEstatusAccion: 0,
        fechaInicio: new Date(),
        fechaFinal: new Date(),
        emailCorrecto: false,
        emailIncorrecto: false,
        emailVacio: false
      },
      pagina: 0,
      resultPerPagina: 10,
      estadisticasOrg: undefined,
      estadisticas: undefined,
      totalResult: 0,
      porcentajeResult: 0.0,
      totalCompromisos: 0,
      subSectores: undefined,
      subTemas: undefined,
      acciones: undefined,
      styleTabSubTotalShow: true,
      styleTabResultShow: false,
      styleTabGraficsShow: false,
      subEstadisticaSector: undefined,
      subEstadisticaSubSector: undefined,
      subEstadisticaTiempos: undefined,
      subEstadisticaMedios: undefined,
      subEstadisticaEstatus: undefined,
      subEstadisticaTemas: undefined,
      subEstadisticaSubTemas: undefined,
      subEstadisticaAcciones: undefined,
      subEstadisticaNiveles: undefined,
      subEstadisticaInversiones: undefined,
      subEstadisticaCumplimientos: undefined,
      subEstadisticaOtraAccion: undefined,
      fechaInicioBusqueda: undefined,
      fechaFinalBusqueda: undefined,
      totalCorrectos: 0,
      totalIncorrectos: 0,
      totalVacios: 0,
      isResultBusqueda: false,
      contNoComprometidos: 0
    });
  },
  getPaginado: function(estadisticas, resultPerPagina) {
    var paginado = undefined;
    var pagina = undefined;
    var i = 0;

    if(estadisticas != undefined && estadisticas.length > 0) {
      paginado = [];
      pagina = [];

      estadisticas.forEach(function(estadistica, index) {
        pagina.push(estadistica);
        
        if(i < resultPerPagina) {
          i++
        } 

        if(i == resultPerPagina || index == (estadisticas.length - 1)) {
          paginado.push(pagina);
          pagina = [];
          i = 0;
        }
      });
    }

    return paginado;
  },
  onClickBuscar: function() {
    var self = this;
    var filtro = this.state.filtro;

    var onSuccess = function(res) {
      var componentState = ComponentState.NORMAL_STATE;
      var isResultBusqueda = false;
      var theChart = undefined;
      var totalCorrectos = 0;
      var totalIncorrectos = 0;
      var totalVacios = 0;
      //destriur todas las graficas esxistentes
      self.destroyGrafics();

      if(res.resultadoEstadisticaVisitante.estadisticasVisitante == undefined || res.resultadoEstadisticaVisitante.estadisticasVisitante.length == 0) {
        componentState = ComponentState.INFO_STATE;
        self.refs.popUpAlert.show(Label.NO_RESULTADOS);
        isResultBusqueda = false;

      } else {
        isResultBusqueda = true;
        //AQUI SE ESCRIBE CODIGO PARA CREAR LAS GRAFICAS
        self.createGrafica(res.resultadoEstadisticaVisitante.subEstadisticaSector, 'pieSector', {subTitle: 'Sector', showInLegend: true, showInLegendSerie: true});
        self.createGrafica(res.resultadoEstadisticaVisitante.subEstadisticaSubSector, 'pieSubSector', {subTitle: 'SubSector', showInLegend: true, showInLegendSerie: true});
        self.createGrafica(res.resultadoEstadisticaVisitante.subEstadisticaTiempos, 'pieTiempos', {subTitle: 'Tiempo a invertir', showInLegend: true, showInLegendSerie: true});
        self.createGrafica(res.resultadoEstadisticaVisitante.subEstadisticaMedios, 'pieMedios', {subTitle: 'Medio', showInLegend: true, showInLegendSerie: true});
        self.createGrafica(res.resultadoEstadisticaVisitante.subEstadisticaEstatus, 'pieEstatus', {subTitle: 'Estatus', showInLegend: true, showInLegendSerie: true});
        self.createGrafica(res.resultadoEstadisticaVisitante.subEstadisticaNiveles, 'pieNiveles', {subTitle: 'Nivel de compromiso', showInLegend: true, showInLegendSerie: true});
        self.createGrafica(res.resultadoEstadisticaVisitante.subEstadisticaInversiones, 'pieInversiones', {subTitle: 'Inversión', showInLegend: true, showInLegendSerie: true});
        self.createGrafica(res.resultadoEstadisticaVisitante.subEstadisticaCumplimientos, 'pieCumplimientos', {subTitle: 'Tiempo estimado', showInLegend: true, showInLegendSerie: true});
        self.createGrafica(res.resultadoEstadisticaVisitante.subEstadisticaOtraAccion, 'pieOtraAcciones', {subTitle: 'Otra acción', showInLegend: true, showInLegendSerie: true});
        self.createGrafica(res.resultadoEstadisticaVisitante.subEstadisticaTemas, 'pieTemas', {subTitle: 'Temas', showInLegend: true, showInLegendSerie: true});
        self.createGrafica(res.resultadoEstadisticaVisitante.subEstadisticaSubTemas, 'pieSubTemas', {subTitle: 'SubTemas', showInLegend: true, showInLegendSerie: true});
        //crear graficas de acciones por subtemas (solo las 6 mas importantes)
        var i = 0;
        res.resultadoEstadisticaVisitante.subEstadisticaSubTemas.forEach(function(subTema) {
          self.createGrafica(subTema.subEstadisticas, ('pieAccionesSubTema' + '_' + i), {subTitle: subTema.nombre, showInLegend: true, showInLegendSerie: true});
          i++;
        });
        //calcular el total de estadisticas del estatus del correo
        res.resultadoEstadisticaVisitante.estadisticasVisitante.forEach(function(visitante) {
          switch(visitante.idEstatusCorreo) {
            case Constants.ESTATUS_CORREO.NO_ENVIADO:
              
            break;

            case Constants.ESTATUS_CORREO.ENVIADO:
              totalCorrectos += 1;
            break;

            case Constants.ESTATUS_CORREO.INCORRECTO:
              totalIncorrectos += 1;
            break;

            case Constants.ESTATUS_CORREO.VACIO:
              totalVacios += 1;
            break;
          }
        });
      }

      self.setState({
        componentState: componentState,
        pagina: 0,
        estadisticasOrg: res.resultadoEstadisticaVisitante.estadisticasVisitante,
        estadisticas: self.getPaginado(res.resultadoEstadisticaVisitante.estadisticasVisitante, self.state.resultPerPagina),
        totalResult: res.resultadoEstadisticaVisitante.totalResult,
        porcentajeResult: res.resultadoEstadisticaVisitante.porcentajeResult,
        totalCompromisos: res.resultadoEstadisticaVisitante.totalCompromisos,
        subEstadisticaSector: res.resultadoEstadisticaVisitante.subEstadisticaSector,
        subEstadisticaSubSector: res.resultadoEstadisticaVisitante.subEstadisticaSubSector,
        subEstadisticaTiempos: res.resultadoEstadisticaVisitante.subEstadisticaTiempos,
        subEstadisticaMedios: res.resultadoEstadisticaVisitante.subEstadisticaMedios,
        subEstadisticaEstatus: res.resultadoEstadisticaVisitante.subEstadisticaEstatus,
        subEstadisticaTemas: res.resultadoEstadisticaVisitante.subEstadisticaTemas,
        subEstadisticaSubTemas: res.resultadoEstadisticaVisitante.subEstadisticaSubTemas,
        subEstadisticaAcciones: res.resultadoEstadisticaVisitante.subEstadisticaAcciones,
        subEstadisticaNiveles: res.resultadoEstadisticaVisitante.subEstadisticaNiveles,
        subEstadisticaInversiones: res.resultadoEstadisticaVisitante.subEstadisticaInversiones,
        subEstadisticaCumplimientos: res.resultadoEstadisticaVisitante.subEstadisticaCumplimientos,
        subEstadisticaOtraAccion: res.resultadoEstadisticaVisitante.subEstadisticaOtraAccion,
        fechaInicioBusqueda: self.state.filtro.fechaInicio,
        fechaFinalBusqueda: self.state.filtro.fechaFinal,
        totalCorrectos: totalCorrectos,
        totalIncorrectos: totalIncorrectos,
        totalVacios: totalVacios,
        isResultBusqueda: isResultBusqueda,
        contNoComprometidos: res.resultadoEstadisticaVisitante.contNoComprometidos
      });
    };
    //valida que el usuario capturo numeros en los campos de edad
    if(ValidaService.isEmpty(filtro.edadMin)) {
      filtro.edadMin = 0;
    }

    if(ValidaService.isEmpty(filtro.edadMax)) {
      filtro.edadMax = 0;
    }

    var params = {
      filtroEstadisticaVisitante: filtro
    };

    adminService.getEstadisticasVisitante(params, onSuccess, this.onError, this.onFail);
  },
  createGrafica: function(subEstadistica, idContainer, options) {
    var series = [];
    var theChart = undefined;
    subEstadistica.forEach(function(estadistica, index) {
      series.push({
        name: estadistica.nombre, 
        y: estadistica.valor,
        extraOpts: undefined
      });
    });

    theChart = highCharts.basicPie(idContainer, series, options);
    this.props.storage.get('graficas').push(theChart);
    this.createGraficaToImage(theChart, idContainer);
  },
  onChangeSector: function(evt) {
    var filtro = this.state.filtro;
    var TODOS = 0;
    var subSectores = undefined;

    filtro.idSector = evt.target.value;
    filtro.idSubSector = 0;
    //obtener los subsectores si se eligio un sector
    if(filtro.idSector > TODOS) {
      subSectores = this.getSubSectorByIdSector(filtro.idSector);
    }

    this.setState({
      filtro: filtro,
      subSectores: subSectores
    });
  },
  onChangeSubSector: function(evt) {
    var filtro = this.state.filtro;
    filtro.idSubSector = evt.target.value;

    this.setState({
      filtro: filtro
    });
  },
  getSubSectorByIdSector: function(idSector) {
    var subSectores = undefined;

    this.state.catalogos.sectores.every(function(sector) {
      if(sector.idSector == idSector) {
        subSectores = sector.subSectores;
        return false;
      }

      return true;
    });

    return subSectores;
  },
  onChangeTiempo: function(evt) {
    var filtro = this.state.filtro;

    filtro.idTiempo = evt.target.value;

    this.setState({
      filtro: filtro
    });
  },
  onChangeEdadMin: function(evt) {
    var filtro = this.state.filtro;

    if(ValidaService.isOnlyNumbers(evt.target.value) && !ValidaService.isMaxLength(evt.target.value, {maxLength: evt.target.maxLength})) {
      filtro.edadMin = evt.target.value;
      
    } else if(ValidaService.isEmpty(evt.target.value)) {
      filtro.edadMin = evt.target.value;
    }

    this.setState({
      filtro: filtro
    });
  },
  onChangeEdadMax: function(evt) {
    var filtro = this.state.filtro;

    if(ValidaService.isOnlyNumbers(evt.target.value) && !ValidaService.isMaxLength(evt.target.value, {maxLength: evt.target.maxLength})) {
      filtro.edadMax = evt.target.value;

    } else if(ValidaService.isEmpty(evt.target.value)) {
      filtro.edadMax = evt.target.value;
    }

    this.setState({
      filtro: filtro
    });
  },
  getParamValueByName: function(paramName) {
    var paramValue = undefined;

    this.state.catalogos.parametros.every(function(param) {
      if(param.parametro == paramName) {
        paramValue = param.valor;
        return false;
      }

      return true;
    });

    return paramValue;
  },
  onChangeMedio: function(evt) {
    var filtro = this.state.filtro;
    filtro.idMedio = evt.target.value;

    this.setState({
      filtro: filtro
    });
  },
  onChangeTema: function(evt) {
    var filtro = this.state.filtro;
    var subTemas = undefined;
    var TODOS = 0;
    var acciones = undefined;

    filtro.idTema = evt.target.value;
    filtro.idSubTema = TODOS;
    filtro.idAccion = TODOS;

    if(filtro.idTema > TODOS) {
      subTemas = this.getSubTemaByIdTema(filtro.idTema);
    } 

    this.setState({
      filtro: filtro,
      subTemas: subTemas,
      acciones: this.state.catalogos.acciones
    });
  },
  getSubTemaByIdTema: function(idTema) {
    var subTemas = undefined;

    this.state.catalogos.temas.forEach(function(tema) {
      if(tema.idTema == idTema) {
        subTemas = tema.subTemas;

        return false;
      }

      return true;
    });

    return subTemas;
  },
  onChangeSubTema: function(evt) {
    var self = this;
    var filtro = this.state.filtro;
    var TODOS = 0;

    filtro.idSubTema = evt.target.value;
    filtro.idAccion = TODOS;

    var onSuccess = function(res) {
      console.log('# onSuccess #');
      self.setState({
        filtro: filtro,
        acciones: res.acciones
      });
    };

    if(filtro.idSubTema > TODOS) {
      var params = {
        idTema: filtro.idTema,
        idSubTema: filtro.idSubTema
      };
      adminService.getAccionesByTemas(params, onSuccess, this.onError, this.onFail);

    } else {
      this.setState({
        filtro: filtro,
        acciones: this.state.catalogos.acciones
      });
    }
  },
  onChangeNivel: function(evt) {
    var filtro = this.state.filtro;
    filtro.idNivel = evt.target.value;

    this.setState({
      filtro: filtro
    });
  },
  onClickInversion: function(evt) {
    var filtro = this.state.filtro;
    filtro.idInversion = evt.target.value;

    this.setState({
      filtro: filtro
    });
  },
  onChangeEstatusCompromiso: function(evt) {
    var filtro = this.state.filtro;
    filtro.idEstatusAccion = evt.target.value;

    this.setState({
      filtro: filtro
    });
  },
  onChangeOtraAccion: function(evt) {
    var filtro = this.state.filtro;

    if(evt.target.checked) {
      if(evt.target.value == new String(Constants.HasOtraAccion.SI)) {
        filtro.idOtraAccion = Constants.HasOtraAccion.SI;
          
      } else if(evt.target.value == new String(Constants.HasOtraAccion.NO)) {
        filtro.idOtraAccion = Constants.HasOtraAccion.NO;
      }

      this.setState({
        filtro: filtro
      });
    }
  },
  onChangeAccion: function(evt) {
    var filtro = this.state.filtro;
    filtro.idAccion = evt.target.value;

    this.setState({
      filtro: filtro
    });
  },
  onClickTabSubTotal: function() {
    this.setState({
      styleTabSubTotalShow: true,
      styleTabResultShow: false,
      styleTabGraficsShow: false
    });
  },
  onClickTabResult: function() {
    this.setState({
      styleTabResultShow: true,
      styleTabSubTotalShow: false,
      styleTabGraficsShow: false
    });
  },
  onClickTabGrafics: function() {
    this.setState({
      styleTabResultShow: false,
      styleTabSubTotalShow: false,
      styleTabGraficsShow: true
    });
  },
  onChangeCumplimiento: function(evt) {
    var filtro = this.state.filtro;
    filtro.idCumplimiento = evt.target.value;

    this.setState({
      filtro: filtro
    });
  },
  onClickNextPage: function(evt) {
    evt.preventDefault();
    var nextPage = 0;

    if(this.state.estadisticas != undefined) {
      nextPage = (this.state.pagina + 1);

      if(nextPage <= (this.state.estadisticas.length - 1) ) {
        this.setState({
          pagina: nextPage
        });  
      }
    }
  },
  onClickBackPage: function(evt) {
    evt.preventDefault();
    var backPage = 0;
    var firstPage = 0;

    if(this.state.estadisticas != undefined) {
      backPage = (this.state.pagina - 1);

      if(backPage >= firstPage) {
        this.setState({
          pagina: backPage
        });  
      }
    }
  },
  onClickFirstPage: function(evt) {
    evt.preventDefault();
    var firstPage = 0;

    if(this.state.estadisticas != undefined) {
      if(firstPage != this.state.pagina) {
        this.setState({
          pagina: firstPage
        });  
      }
    }
  },
  onClickLastPage: function(evt) {
    evt.preventDefault();
    var lastPage = 0;

    if(this.state.estadisticas != undefined) {
      lastPage = (this.state.estadisticas.length - 1);

      if(lastPage != this.state.pagina) {
        this.setState({
          pagina: lastPage
        });
      }
    }
  },
  onChangeResultPerPagina: function(evt) {
    if(this.state.estadisticasOrg != undefined) {
      this.setState({
        pagina: 0,
        resultPerPagina: evt.target.value,
        estadisticas: this.getPaginado(this.state.estadisticasOrg, evt.target.value)
      });
    }
  },
  onDatePickedFechaInicio: function(datePicked, evt) {
    var filtro = this.state.filtro;
    filtro.fechaInicio = datePicked;

    this.setState({
      filtro: filtro
    });
  },
  onDatePickedFechaFinal: function(datePicked, evt) {
    var filtro = this.state.filtro;
    filtro.fechaFinal = datePicked;

    this.setState({
      filtro: filtro
    });
  },
  b64toBlob: function (b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];
    var blob = undefined;

    try {
      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);
        var byteNumbers = new Array(slice.length);

        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
      }

      blob = new Blob(byteArrays, {type: contentType});

    } catch(err) {
      // TypeError old chrome and FF
      window.BlobBuilder = window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder || window.MSBlobBuilder;
      if(err.name == 'TypeError' && window.BlobBuilder){
        var bb = new BlobBuilder();
        bb.append(byteArrays);
        blob = bb.getBlob(contentType);
      }
      else if(err.name == 'InvalidStateError'){
        // InvalidStateError (FF13 WinXP)
        blob = new Blob(byteArrays, {type : contentType});
      }
      else{
      }
    }

    return blob;
  },
  createGraficaToImage: function(grafica, idContainer) {
    var grafica64 = '';
    var data = grafica.container.children[0];
    var xml = new XMLSerializer().serializeToString(data);
    var svg64 = btoa(unescape(encodeURIComponent(xml)));
    var b64Start = 'data:image/svg+xml;base64,';
    var image64 = b64Start + svg64;
    var img = document.createElement('img');
    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext('2d');
    var scale = 85;

    img.src = image64;
    canvas.width = img.width;
    canvas.height = img.height;
    ctx.drawImage(img, 0, 0, ((img.width*scale)/100), ((img.height*scale)/100));
    grafica64 = canvas.toDataURL('image/png');
    this.props.storage.get('graficas64').push(grafica64);
  },
  onClickExportExcel: function(evt) {
    evt.preventDefault();
    var self = this;

    var onSuccess = function(res) {
      var blob = self.b64toBlob(res.exportToExcel, Constants.EXCEL_CONTENCT_TYPE);
      var URL = window.URL || window.webkitURL || window.mozURL || window.msURL;
      var blobUrl = URL.createObjectURL(blob);
      var ventana = window.open(undefined, '_blank', 'titlebar=no, width=320, height=240');

      if(ventana != undefined) {
        //otra forma de descargar archivos
        //ventana.document.write('Su descarga iniciar&aacute; en un instante... <a id=descarga href=' + blobUrl + 
        //                        ' download='EstadisticasVisitante.xlsx'>estadisticasVisitante.xlsx</a>');
        //ventana.document.getElementById('descarga').click();

        //otra forma de descargar archivos
        var pom = document.createElement('a');
        pom.setAttribute('id', 'descarga');
        pom.setAttribute('href', blobUrl);
        pom.setAttribute('download', 'EstadisticasVisitante.xlsx');
        pom.click();
        ventana.close();
        
      } else {
        console.log('Error - Ventanas emergentes no permitidas.');
        self.refs.popUpAlert.show(Label.NO_VENTANAS_EMERGENTES);
      }
    };

    var params = {
      resultadoEstadisticaVisitante : {
        totalResult: this.state.totalResult,
        porcentajeResult: this.state.porcentajeResult,
        totalCompromisos: this.state.totalCompromisos,
        subEstadisticaSector: this.state.subEstadisticaSector,
        subEstadisticaSubSector: this.state.subEstadisticaSubSector,
        subEstadisticaTiempos: this.state.subEstadisticaTiempos,
        subEstadisticaMedios: this.state.subEstadisticaMedios,
        subEstadisticaEstatus: this.state.subEstadisticaEstatus,
        subEstadisticaTemas: this.state.subEstadisticaTemas,
        subEstadisticaSubTemas: this.state.subEstadisticaSubTemas,
        subEstadisticaAcciones: this.state.subEstadisticaAcciones,
        subEstadisticaNiveles: this.state.subEstadisticaNiveles,
        subEstadisticaInversiones: this.state.subEstadisticaInversiones,
        subEstadisticaCumplimientos: this.state.subEstadisticaCumplimientos,
        subEstadisticaOtraAccion: this.state.subEstadisticaOtraAccion,
        fechaInicioBusqueda: this.state.filtro.fechaInicio,
        fechaFinalBusqueda: this.state.filtro.fechaFinal,
        totalCorrectos: this.state.totalCorrectos,
        totalIncorrectos: this.state.totalIncorrectos,
        totalVacios: this.state.totalVacios,
        graficas: this.props.storage.get('graficas64')
      }
    };
    adminService.exportEstadistciaVisitante(params, onSuccess, this.onError, this.onFail);
  },
  destroyGrafics: function() {
    this.props.storage.get('graficas').forEach(function(grafica) {
      grafica.destroy();
    });
    this.props.storage.put('graficas', []);
    this.props.storage.put('graficas64', []);
  },
  reflowGrafics: function() {
    this.props.storage.get('graficas').forEach(function(grafica) {
      grafica.reflow();
    });
  },
  onChangeCheckEmailCorrecto: function(evt) {
    var filtro = this.state.filtro;
    filtro.emailCorrecto = evt.target.checked;

    this.setState({
      filtro: filtro
    });
  },
  onChangeCheckEmailIncorrecto: function(evt) {
    var filtro = this.state.filtro;
    filtro.emailIncorrecto = evt.target.checked;

    this.setState({
      filtro: filtro
    });
  },
  onChangeCheckEmailVacio: function(evt) {
    var filtro = this.state.filtro;
    filtro.emailVacio = evt.target.checked;

    this.setState({
      filtro: filtro
    });
  },
  render: function() {
    var resultadosHTML = [];
    var sectores = [];
    var subSectores = [];
    var tiempos = [];
    var edadMin = 0;
    var edadMax = 0;
    var medios = [];
    var temas = [];
    var subTemas = [];
    var niveles = [];
    var checkedIntersionSI = false;
    var checkedIntersionNO = false;
    var cumplimientos = [];
    var estatusCompromisos = [];
    var checkedOtraAccionSI = false;
    var checkedOtraAccionNO = false;
    var acciones = [];
    var styleShow = {
      display: 'block',
      visibility: 'visible'
    };
    var styleHide = {
      display: 'none',
      visibility: 'hidden'
    };
    var styleTabResult = {};
    var styleTabSubTotal = {};
    var activeTabResult = '';
    var activeTabSubTotal = '';
    var styleTabGrafics = {};
    var activeTabGrafics = '';
    var subEstadisticaSector = [];
    var subEstadisticaSubSector = [];
    var subEstadisticaTiempos = [];
    var subEstadisticaMedios = [];
    var subEstadisticaEstatus = [];
    var subEstadisticaTemas = [];
    var subEstadisticaSubTemas = [];
    var subEstadisticaAcciones = [];
    var subEstadisticaNiveles = [];
    var subEstadisticaInversiones = [];
    var subEstadisticaCumplimientos = [];
    var subEstadisticaOtraAccion = [];
    var totalPaginado = '0 de 0 Páginas';
    var fechaInicioFormat = '';
    var fechaFinalFormat = '';
    var exportExcel = '';

    if(!this.state.shouldMount) {
      return (React.createElement("div", null));
    }
    //estadisticas detalle
    if(this.state.estadisticas != undefined) {
      totalPaginado = ((this.state.pagina + 1) + ' de ' +  this.state.estadisticas.length + ' Páginas')
      this.state.estadisticas[this.state.pagina].forEach(function(estadistica) {
        resultadosHTML.push(
          React.createElement("tr", null, 
            React.createElement("td", null, estadistica.nombre), 
            React.createElement("td", null, estadistica.emailFace), 
            React.createElement("td", null, estadistica.edad), 
            React.createElement("td", null, estadistica.accion), 
            React.createElement("td", null, estadistica.estatusCompromiso)
          )
        );
      });
    }
    //sub estadisticas sectores
    if(this.state.subEstadisticaSector != undefined) {
      this.state.subEstadisticaSector.forEach(function(sector) {
        subEstadisticaSector.push(
          React.createElement("div", {className: "subtotales"}, 
            React.createElement("span", {className: "tipo"}, sector.nombre), 
            React.createElement("span", {className: "resultado"}, sector.valor)
          )
        );
      });
    }
    //sub estadisticas subsectores
    if(this.state.subEstadisticaSubSector != undefined) {
      this.state.subEstadisticaSubSector.forEach(function(subSector) {
        subEstadisticaSubSector.push(
          React.createElement("div", {className: "subtotales"}, 
            React.createElement("span", {className: "tipo"}, subSector.nombre), 
            React.createElement("span", {className: "resultado"}, subSector.valor)
          )
        );
      });
    }
    //sub estadisticas tiempos
    if(this.state.subEstadisticaTiempos != undefined) {
      this.state.subEstadisticaTiempos.forEach(function(tiempo) {
        subEstadisticaTiempos.push(
          React.createElement("div", {className: "subtotales"}, 
            React.createElement("span", {className: "tipo"}, tiempo.nombre), 
            React.createElement("span", {className: "resultado"}, tiempo.valor)
          )
        );
      });
    }
    //sub estadisticas medios
    if(this.state.subEstadisticaMedios != undefined) {
      this.state.subEstadisticaMedios.forEach(function(medio) {
        subEstadisticaMedios.push(
          React.createElement("div", {className: "subtotales"}, 
            React.createElement("span", {className: "tipo"}, medio.nombre), 
            React.createElement("span", {className: "resultado"}, medio.valor)
          )
        );
      });
    }
    //sub estadisticas estatus
    if(this.state.subEstadisticaEstatus != undefined) {
      this.state.subEstadisticaEstatus.forEach(function(estatus) {
        subEstadisticaEstatus.push(
          React.createElement("div", {className: "subtotales"}, 
            React.createElement("span", {className: "tipo"}, estatus.nombre), 
            React.createElement("span", {className: "resultado"}, estatus.valor)
          )
        );
      });
    }
    //sub estadisticas temas
    if(this.state.subEstadisticaTemas != undefined) {
      this.state.subEstadisticaTemas.forEach(function(tema) {
        subEstadisticaTemas.push(
          React.createElement("div", {className: "subtotales"}, 
            React.createElement("span", {className: "tipo"}, tema.nombre), 
            React.createElement("span", {className: "resultado"}, tema.valor)
          )
        );
      });
    }
    //sub estadisticas subTemas
    if(this.state.subEstadisticaSubTemas != undefined) {
      this.state.subEstadisticaSubTemas.forEach(function(subTema) {
        subEstadisticaSubTemas.push(
          React.createElement("div", {className: "subtotales"}, 
            React.createElement("span", {className: "tipo"}, subTema.nombre), 
            React.createElement("span", {className: "resultado"}, subTema.valor)
          )
        );
      });
    }
    //sub estadisticas acciones
    if(this.state.subEstadisticaAcciones != undefined) {
      this.state.subEstadisticaAcciones.forEach(function(accion) {
        subEstadisticaAcciones.push(
          React.createElement("div", {className: "subtotales"}, 
            React.createElement("span", {className: "tipo"}, accion.nombre), 
            React.createElement("span", {className: "resultado"}, accion.valor)
          )
        );
      });
    }
    //sub estadisticas niveles
    if(this.state.subEstadisticaNiveles != undefined) {
      this.state.subEstadisticaNiveles.forEach(function(nivel) {
        subEstadisticaNiveles.push(
          React.createElement("div", {className: "subtotales"}, 
            React.createElement("span", {className: "tipo"}, nivel.nombre), 
            React.createElement("span", {className: "resultado"}, nivel.valor)
          )
        );
      });
    }
    //sub estadisticas inversiones
    if(this.state.subEstadisticaInversiones != undefined) {
      this.state.subEstadisticaInversiones.forEach(function(inversion) {
        subEstadisticaInversiones.push(
          React.createElement("div", {className: "subtotales"}, 
            React.createElement("span", {className: "tipo"}, inversion.nombre), 
            React.createElement("span", {className: "resultado"}, inversion.valor)
          )
        );
      });
    }
    //sub estadisticas cumplimientos
    if(this.state.subEstadisticaCumplimientos != undefined) {
      this.state.subEstadisticaCumplimientos.forEach(function(cumplimiento) {
        subEstadisticaCumplimientos.push(
          React.createElement("div", {className: "subtotales"}, 
            React.createElement("span", {className: "tipo"}, cumplimiento.nombre), 
            React.createElement("span", {className: "resultado"}, cumplimiento.valor)
          )
        );
      });
    }
    //sub estadistica Otra Accion
    if(this.state.subEstadisticaOtraAccion != undefined) {
      this.state.subEstadisticaOtraAccion.forEach(function(otraAccion) {
        subEstadisticaOtraAccion.push(
          React.createElement("div", {className: "subtotales"}, 
            React.createElement("span", {className: "tipo"}, otraAccion.nombre), 
            React.createElement("span", {className: "resultado"}, otraAccion.valor)
          )
        );
      });
    }
    //hacer algo dependiendo el estado del componente
    switch(this.state.componentState) {
      case ComponentState.NORMAL_STATE:
      break;

      case ComponentState.EXITO_STATE:
      break;

      case ComponentState.ERROR_STATE:
      break;

      case ComponentState.INFO_STATE:
      break;

      case ComponentState.ALERT_STATE:
      break;

      case ComponentState.LOADING_STATE:
      break;

      case ComponentState.ASK_STATE:
      break;
    }

    if(this.state.catalogos != undefined) {
      //construir sectores
      this.state.catalogos.sectores.forEach(function(sector) {
        sectores.push(React.createElement("option", {value: sector.idSector}, sector.descripcion));
      });
      //construir los tiempos a invertir
      this.state.catalogos.tiempos.forEach(function(tiempo) {
        tiempos.push(React.createElement("option", {value: tiempo.idTiempo}, tiempo.descripcion));
      });
      //construir parametros de configuracion
      edadMin = this.getParamValueByName(Constants.Parametros.EDAD_MINIMA);
      edadMax = this.getParamValueByName(Constants.Parametros.EDAD_MAXIMA);
      //construr medios
      this.state.catalogos.medios.forEach(function(medio) {
        medios.push(React.createElement("option", {value: medio.idMedio}, medio.descripcion));
      });
      //crear temas de interes
      this.state.catalogos.temas.forEach(function(tema) {
        temas.push(React.createElement("option", {value: tema.idTema}, tema.descripcion));
      });
      //construir niveles
      this.state.catalogos.niveles.forEach(function(nivel) {
        niveles.push(React.createElement("option", {value: nivel.idNivel}, nivel.descripcion));
      });
      //construir complimientos o tiempo estimado para cumplir la accion
      this.state.catalogos.cumplimientos.forEach(function(cumplimiento) {
        cumplimientos.push(React.createElement("option", {value: cumplimiento.idCumplimiento}, cumplimiento.descripcion));
      });
      //construir estatus cumplimientos
      this.state.catalogos.estatusCompromisos.forEach(function(estatusCompromiso) {
        estatusCompromisos.push(React.createElement("option", {value: estatusCompromiso.idEstatusCompromiso}, estatusCompromiso.descripcion));
      });
    }
    //construir los subsectores
    if(this.state.subSectores != undefined) {
      this.state.subSectores.forEach(function(subSector) {
        subSectores.push(React.createElement("option", {value: subSector.idSubSector}, subSector.descripcion));
      });
    }
    //construir subtemas
    if(this.state.subTemas != undefined) {
      this.state.subTemas.forEach(function(subTema) {
        subTemas.push(React.createElement("option", {value: subTema.idSubTema}, subTema.descripcion));
      });
    }
    //construir acciones
    if(this.state.acciones != undefined) {
      this.state.acciones.forEach(function(accion) {
        acciones.push(React.createElement("option", {value: accion.idAccion}, accion.descripcion));
      });

    } else {
      if(this.state.catalogos != undefined) {
        this.state.catalogos.acciones.forEach(function(accion) {
          acciones.push(React.createElement("option", {value: accion.idAccion}, accion.descripcion));
        });
      }
    }
    //controlar flujo de inversion
    if(this.state.filtro.idInversion == Constants.Inversion.SI) {
      checkedIntersionSI = true;
      checkedIntersionNO = false;

    } else if(this.state.filtro.idInversion == Constants.Inversion.NO) {
      checkedIntersionSI = false;
      checkedIntersionNO = true;

    } else {
      checkedIntersionSI = false;
      checkedIntersionNO = false;
    }
    //controlar flujo de otra accion
    if(this.state.filtro.idOtraAccion == Constants.HasOtraAccion.SI) {
      checkedOtraAccionSI = true;
      checkedOtraAccionNO = false;

    } else if(this.state.filtro.idOtraAccion == Constants.HasOtraAccion.NO) {
      checkedOtraAccionSI = false;
      checkedOtraAccionNO = true;

    } else {
      checkedOtraAccionSI = false;
      checkedOtraAccionNO = false;
    }

    if(this.state.styleTabSubTotalShow) {
      styleTabSubTotal = styleShow;
      activeTabSubTotal = 'active';

    } else {
      styleTabSubTotal = styleHide;
    }

    if(this.state.styleTabResultShow) {
      styleTabResult = styleShow;
      activeTabResult = 'active';

    } else {
      styleTabResult = styleHide;
    }

    if(this.state.styleTabGraficsShow) {
      styleTabGrafics = styleShow;
      activeTabGrafics = 'active';

    } else {
      styleTabGrafics = styleHide;
    }
    //formaterar las fechas de busqueda para mostrarlas
    if(this.state.fechaInicioBusqueda != undefined) {
      fechaInicioFormat = (this.state.fechaInicioBusqueda.getDate() + '/' + (this.state.fechaInicioBusqueda.getMonth() + 1) + '/' + this.state.fechaInicioBusqueda.getFullYear());
    }

    if(this.state.fechaFinalBusqueda != undefined) {
      fechaFinalFormat = (this.state.fechaFinalBusqueda.getDate() + '/' + (this.state.fechaFinalBusqueda.getMonth() + 1) + '/' + this.state.fechaFinalBusqueda.getFullYear());
    }

    if(this.state.isResultBusqueda) {
      exportExcel = (React.createElement("a", {className: "IconExcelStyle", href: "#", onClick: this.onClickExportExcel}));
    }

    return (
      React.createElement("div", {id: "container-gral"}, 
        React.createElement(PopUpReact, {ref: "popUpAlert", type: "alert", popUpStyle: "PopUpReactAlert"}), 

        React.createElement("article", {className: "module width_full"}, 
          React.createElement("header", null, React.createElement("h3", null, "Filtros de Búsqueda")), 
            React.createElement("div", {className: "module_content"}, 
              React.createElement("fieldset", {style: {width: '48%', float: 'left', marginRight: '3%'}}, 
                React.createElement("label", null, "Sector"), 
                React.createElement("select", {style: {width:'92%'}, value: this.state.filtro.idSector, onChange: this.onChangeSector}, 
                  React.createElement("option", {value: "0"}, "Todos"), 
                  sectores
                )
              ), 

              React.createElement("fieldset", {style: {width:'48%', float:'left'}}, 
                React.createElement("label", null, "Sub-Sector"), 
                React.createElement("select", {style: {width:'92%'}, value: this.state.filtro.idSubSector, onChange: this.onChangeSubSector}, 
                  React.createElement("option", {value: "0"}, "Todos"), 
                  subSectores
                )
              ), 

              React.createElement("fieldset", {style: {width:'48%', float:'left', marginRight: '3%'}}, 
                React.createElement("label", null, "Tiempo a invertir"), 
                React.createElement("select", {style: {width:'92%'}, value: this.state.filtro.idTiempo, onChange: this.onChangeTiempo}, 
                  React.createElement("option", {value: "0"}, "Todos"), 
                  tiempos
                )
              ), 

              React.createElement("fieldset", {style: {width:'48%', float:'left'}}, 
                React.createElement("div", {style: {width:'45%', float:'left', marginRight:'3%', marginLeft:'3%'}}, 
                  React.createElement("label", null, "Edad Mínima"), 
                  React.createElement("input", {type: "text", style: {width:'92%'}, name: "edad", min: edadMin, max: edadMax, maxLength: "2", size: "2", step: "1", value: this.state.filtro.edadMin, onChange: this.onChangeEdadMin})
                ), 
                React.createElement("div", {style: {width:'48%', float:'left'}}, 
                  React.createElement("label", null, "Edad Máxima"), 
                  React.createElement("input", {type: "text", style: {width:'92%'}, name: "edad", min: edadMin, max: edadMax, maxLength: "2", size: "2", step: "1", value: this.state.filtro.edadMax, onChange: this.onChangeEdadMax})
                )
              ), 

              React.createElement("fieldset", {style: {width:'48%', float:'left', marginRight:'3%'}}, 
                React.createElement("label", null, "Medio"), 
                React.createElement("select", {style: {width:'92%'}, value: this.state.filtro.idMedio, onChange: this.onChangeMedio}, 
                  React.createElement("option", {value: "0"}, "Todos"), 
                  medios
                )
              ), 

              React.createElement("fieldset", {style: {width:'48%', float:'left'}}, 
                React.createElement("label", null, "Estatus"), 
                React.createElement("select", {style: {width:'92%'}, value: this.state.filtro.idEstatusAccion, onChange: this.onChangeEstatusCompromiso}, 
                  React.createElement("option", {value: "0"}, "Todos"), 
                  estatusCompromisos
                )
              ), 

              React.createElement("fieldset", {style: {width:'99%', float:'left', marginRight: '3%'}}, 
                React.createElement("div", {style: {width:'48%', float:'left', marginRight: '3%'}}, 
                  React.createElement("label", null, "Temas de Interés"), 
                  React.createElement("select", {style: {width:'92%'}, value: this.state.filtro.idTema, onChange: this.onChangeTema}, 
                    React.createElement("option", {value: "0"}, "Todos"), 
                    temas
                  )
                ), 
                React.createElement("div", {style: {width:'48%', float:'left', marginRight:'1%'}}, 
                  React.createElement("label", null, "Sub-Temas"), 
                  React.createElement("select", {style: {width:'92%'}, value: this.state.filtro.idSubTema, onChange: this.onChangeSubTema}, 
                    React.createElement("option", {value: "0"}, "Todos"), 
                    subTemas
                  )
                ), 
                React.createElement("div", {style: {width:'99%', float:'left', marginRight:'3%', marginTop:'2%'}}, 
                  React.createElement("label", null, "Acción"), 
                  React.createElement("select", {style: {width:'96%'}, value: this.state.filtro.idAccion, onChange: this.onChangeAccion}, 
                    React.createElement("option", {value: "0"}, "Todos"), 
                    acciones
                  )
                )
              ), 

              React.createElement("fieldset", {style: {width:'48%', float:'left', marginRight:'3%'}}, 
                React.createElement("div", {style: {width:'48%', float:'left', marginRight:'3%'}}, 
                  React.createElement("label", null, "Nivel de Compromiso"), 
                  React.createElement("select", {style: {width:'92%'}, value: this.state.filtro.idNivel, onChange: this.onChangeNivel}, 
                    React.createElement("option", {value: "0"}, "Todos"), 
                    niveles
                  )
                ), 

                React.createElement("div", {style: {width:'48%', float:'left'}}, 
                  React.createElement("label", null, "Inversión"), 
                  React.createElement("div", {style: {width:'20%', float:'left', marginRight:'3%', marginLeft:'3%'}}, 
                    React.createElement("input", {type: "radio", name: "radio1", value: Constants.Inversion.SI, checked: checkedIntersionSI, onClick: this.onClickInversion}), 
                    React.createElement("span", null, "Si")
                  ), 
                  React.createElement("div", {style: {width:'20%', float:'left', marginRight:'3%'}}, 
                    React.createElement("input", {type: "radio", name: "radio1", value: Constants.Inversion.NO, checked: checkedIntersionNO, onClick: this.onClickInversion}), 
                    React.createElement("span", null, "No")
                  )
                )
              ), 

              React.createElement("fieldset", {style: {width:'48%', float:'left'}}, 
                React.createElement("label", null, "Tiempo Estimado"), 
                React.createElement("select", {style: {width:'92%'}, value: this.state.filtro.idCumplimiento, onChange: this.onChangeCumplimiento}, 
                  React.createElement("option", {value: "0"}, "Todos"), 
                  cumplimientos
                )
              ), 

              React.createElement("fieldset", {style: {width:'48%', float:'left', marginRight: '3%'}}, 
                React.createElement("div", {style: {width:'100%', float:'left', marginRight: '3%'}}, 
                  React.createElement("label", {style: {width: '100%'}}, "Comprometido con otra acción"), 
                  React.createElement("div", {style: {width:'20%', float:'left', marginRight: '3%', marginLeft: '3%'}}, 
                    React.createElement("input", {type: "radio", name: "radio2", value: Constants.HasOtraAccion.SI, checked: checkedOtraAccionSI, onChange: this.onChangeOtraAccion}), 
                    React.createElement("span", null, "Si")
                  ), 
                  React.createElement("div", {style: {width:'20%', float:'left', marginRight: '3%'}}, 
                    React.createElement("input", {type: "radio", name: "radio2", value: Constants.HasOtraAccion.NO, checked: checkedOtraAccionNO, onChange: this.onChangeOtraAccion}), 
                    React.createElement("span", null, "No")
                  )
                )
              ), 

              React.createElement("fieldset", {style: {width:'48%', float:'left'}}, 
                React.createElement("div", {style: {width:'100%', float:'left', marginRight: '0%'}}, 
                  React.createElement("label", {style: {width: '100%'}}, "Rango de fechas"), 
                  React.createElement("div", {style: {width:'100%', float:'left', marginRight: '0%'}}, 
                    React.createElement(DatePickerReact, {ref: "fechaInicio", idCal: "fechaInicio", inputLabel: "Fecha Inicio:", onDatePicked: this.onDatePickedFechaInicio})
                  ), 
                  React.createElement("div", {style: {width:'100%', float:'left', marginRight: '0%'}}, 
                    React.createElement(DatePickerReact, {ref: "fechaFinal", idCal: "fechaFinal", inputLabel: "Fecha Final:", onDatePicked: this.onDatePickedFechaFinal})
                  )
                )
              ), 

              React.createElement("fieldset", {style: {width:'99%', float:'left', marginLeft: '0px'}}, 
                React.createElement("div", {style: {width: '30%', float: 'left', marginLeft: '10px', marginRight: '10px'}}, 
                  React.createElement("div", {style: {float: 'left', marginLeft: '10px', marginRight: '10px'}}, 
                    React.createElement("input", {type: "checkbox", value: this.state.filtro.emailCorrecto, checked: this.state.filtro.emailCorrecto, onChange: this.onChangeCheckEmailCorrecto})
                  ), 
                  React.createElement("div", {style: {width: '70%', marginLeft: '10px', float: 'left'}}, 
                    "Email correcto"
                  )
                ), 

                React.createElement("div", {style: {width: '30%', float: 'left', marginLeft: '10px', marginRight: '10px'}}, 
                  React.createElement("div", {style: {float: 'left', marginLeft: '10px', marginRight: '10px'}}, 
                    React.createElement("input", {type: "checkbox", value: this.state.filtro.emailIncorrecto, checked: this.state.filtro.emailIncorrecto, onChange: this.onChangeCheckEmailIncorrecto})
                  ), 
                  React.createElement("div", {style: {width: '70%', marginLeft: '10px', float: 'left'}}, 
                    "Email Incorrecto"
                  )
                ), 

                React.createElement("div", {style: {width: '30%', float: 'left', marginLeft: '10px', marginRight: '10px'}}, 
                  React.createElement("div", {style: {float: 'left', marginLeft: '10px', marginRight: '10px'}}, 
                    React.createElement("input", {type: "checkbox", value: this.state.filtro.emailVacio, checked: this.state.filtro.emailVacio, onChange: this.onChangeCheckEmailVacio})
                  ), 
                  React.createElement("div", {style: {width: '70%', marginLeft: '10px', float: 'left'}}, 
                    "Email Vacio"
                  )
                )
              ), 

              React.createElement("div", {className: "clear"})
            ), 
          React.createElement("footer", null, 
            React.createElement("div", {className: "submit_link"}, 
              React.createElement("input", {type: "button", value: "BUSCAR", className: "alt_btn", onClick: this.onClickBuscar}), 
              React.createElement("input", {type: "button", value: "LIMPIAR", onClick: this.onClickLimpiar})
            )
          )
        ), 

        React.createElement("nav", null, 
          React.createElement("ul", {id: "menuNav"}, 
            React.createElement("li", {className: activeTabSubTotal, style: {width: '33%'}, onClick: this.onClickTabSubTotal}, "Subtotales"), 
            React.createElement("li", {className: activeTabResult, style: {width: '33%'}, onClick: this.onClickTabResult}, "Detalle"), 
            React.createElement("li", {className: activeTabGrafics, style: {width: '33%'}, onClick: this.onClickTabGrafics}, "Gráficas")
          )
        ), 
        
        React.createElement("article", {className: "module width_full"}, 
          React.createElement("header", null, 
            React.createElement("div", {className: "AlignContainer"}, 
              React.createElement("div", {className: "AlignLeft", style: {width: '95%'}}, 
                React.createElement("h3", {className: "tabs_involved"}, "Resultado de la Búsqueda")
              ), 
              React.createElement("div", {className: "AlignRight", style: {width: '5%', marginTop: '5px', textAlign: 'center'}}, 
                exportExcel
              )
            )
          ), 

          React.createElement("article", {className: "result"}, 
            React.createElement("div", {className: "overview"}, 
              React.createElement("p", {className: "overview_title"}, "Número de Resultados"), 
              React.createElement("p", {className: "overview_count"}, this.state.totalResult), 
              React.createElement("p", {className: "overview_type"}, "Compromisos")
            ), 
            React.createElement("div", {className: "overview"}, 
              React.createElement("p", {className: "overview_title"}, "Porcentaje Total"), 
              React.createElement("p", {className: "overview_count"}, this.state.porcentajeResult.toFixed(2), "%"), 
              React.createElement("p", {className: "overview_type"}, "Porciento")
            ), 
            React.createElement("div", {className: "overview"}, 
              React.createElement("p", {className: "overview_title"}, "Total de Compromisos"), 
              React.createElement("p", {className: "overview_count"}, this.state.totalCompromisos), 
              React.createElement("p", {className: "overview_type"}, "Compromisos")
            )
          ), 

          React.createElement("article", {className: "result"}, 
            React.createElement("div", {className: "overview", style: {width: '50%', height: '52px'}}, 
              React.createElement("p", {className: "overview_title"}, "Fecha Inicio"), 
              React.createElement("p", {className: "overview_count"}, fechaInicioFormat)
            ), 
            React.createElement("div", {className: "overview", style: {width: '50%', height: '52px'}}, 
              React.createElement("p", {className: "overview_title"}, "Fecha Final"), 
              React.createElement("p", {className: "overview_count"}, fechaFinalFormat)
            )
          ), 

          React.createElement("article", {className: "result", style: {paddingBottom: '10px'}}, 
            React.createElement("div", {className: "overview", style: {width: '25%', height: '52px'}}, 
              React.createElement("p", {className: "overview_title"}, "Email Correctos"), 
              React.createElement("p", {className: "overview_count"}, this.state.totalCorrectos)
            ), 
            React.createElement("div", {className: "overview", style: {width: '25%', height: '52px'}}, 
              React.createElement("p", {className: "overview_title"}, "Email Incorrectos"), 
              React.createElement("p", {className: "overview_count"}, this.state.totalIncorrectos)
            ), 
            React.createElement("div", {className: "overview", style: {width: '25%', height: '52px'}}, 
              React.createElement("p", {className: "overview_title"}, "Email Vacios"), 
              React.createElement("p", {className: "overview_count"}, this.state.totalVacios)
            ), 
            React.createElement("div", {className: "overview", style: {width: '25%', height: '52px'}}, 
              React.createElement("p", {className: "overview_title"}, "Contador No Comprometidos"), 
              React.createElement("p", {className: "overview_count"}, this.state.contNoComprometidos)
            )
          ), 

          React.createElement("div", {className: "tab_container"}, 
            React.createElement("div", {className: "tab_content", style: styleTabResult}, 
              React.createElement("table", {className: "tablesorter", cellSpacing: "0"}, 
                React.createElement("thead", null, 
                  React.createElement("tr", null, 
                    React.createElement("th", null, "Nombre"), 
                    React.createElement("th", null, "Email"), 
                    React.createElement("th", null, "Edad"), 
                    React.createElement("th", null, "Acción"), 
                    React.createElement("th", null, "Estatus")
                  )
                ), 
                React.createElement("tbody", null, 
                  resultadosHTML
                ), 
                React.createElement("tfoot", null, 
                  React.createElement("tr", null, 
                    React.createElement("td", {colSpan: "100%"}, 
                      React.createElement("div", {id: "pag"}, 
                        React.createElement("select", {id: "soflow-color", value: this.state.resultPerPagina, onChange: this.onChangeResultPerPagina}, 
                          React.createElement("option", {value: "5"}, "5"), 
                          React.createElement("option", {value: "10"}, "10"), 
                          React.createElement("option", {value: "15"}, "15"), 
                          React.createElement("option", {value: "20"}, "20"), 
                          React.createElement("option", {value: "25"}, "25")
                        ), 
                        React.createElement("ul", {id: "pagination"}, 
                          React.createElement("li", {className: "previous"}, React.createElement("a", {href: "#", onClick: this.onClickFirstPage}, "<< Primero")), 
                          React.createElement("li", {className: "previous"}, React.createElement("a", {href: "#", onClick: this.onClickBackPage}, "< Anterior")), 
                          React.createElement("li", {className: "active"}, totalPaginado), 
                          React.createElement("li", {className: "next"}, React.createElement("a", {href: "#", onClick: this.onClickNextPage}, "Siguiente >")), 
                          React.createElement("li", {className: "next"}, React.createElement("a", {href: "#", onClick: this.onClickLastPage}, "Último >>"))
                        )
                      )
                    )
                  )
                )
              )
            ), 

            React.createElement("div", {className: "tab_content", style: styleTabSubTotal}, 
              React.createElement("table", {className: "tablesorter", cellSpacing: "0"}, 
                React.createElement("tr", null, 
                  React.createElement("td", null, 
                    React.createElement("article", {className: "module width_1_3_quarter", style: {height: '221px'}}, 
                      React.createElement("header", null, React.createElement("h3", null, "Sector")), 
                      React.createElement("div", {className: "innerDiv scroll_y"}, 
                        subEstadisticaSector
                      )
                    ), 
                    React.createElement("article", {className: "module width_1_3_quarter", style: {height: '221px'}}, 
                      React.createElement("header", null, React.createElement("h3", null, "SubSector")), 
                      React.createElement("div", {className: "innerDiv scroll_y"}, 
                        subEstadisticaSubSector
                      )
                    ), 
                    React.createElement("article", {className: "module width_1_3_quarter", style: {height: '221px'}}, 
                      React.createElement("header", null, React.createElement("h3", null, "Tiempo a invertir")), 
                      React.createElement("div", {className: "innerDiv"}, 
                        subEstadisticaTiempos
                      )
                    ), 
                    React.createElement("article", {className: "module width_1_3_quarter", style: {height: '150px'}}, 
                      React.createElement("header", null, React.createElement("h3", null, "Medio")), 
                      React.createElement("div", {className: "innerDiv"}, 
                        subEstadisticaMedios
                      )
                    ), 
                    React.createElement("article", {className: "module width_1_3_quarter", style: {height: '150px'}}, 
                      React.createElement("header", null, React.createElement("h3", null, "Estatus")), 
                      React.createElement("div", {className: "innerDiv"}, 
                        subEstadisticaEstatus
                      )
                    ), 
                    React.createElement("article", {className: "module width_1_3_quarter", style: {height: '150px'}}, 
                      React.createElement("header", null, React.createElement("h3", null, "Nivel de compromiso")), 
                      React.createElement("div", {className: "innerDiv"}, 
                        subEstadisticaNiveles
                      )
                    ), 
                    React.createElement("article", {className: "module width_1_3_quarter", style: {height: '150px'}}, 
                      React.createElement("header", null, React.createElement("h3", null, "Inversión")), 
                      React.createElement("div", {className: "innerDiv"}, 
                        subEstadisticaInversiones
                      )
                    ), 
                    React.createElement("article", {className: "module width_1_3_quarter", style: {height: '150px'}}, 
                      React.createElement("header", null, React.createElement("h3", null, "Tiempo estimado")), 
                      React.createElement("div", {className: "innerDiv"}, 
                        subEstadisticaCumplimientos
                      )
                    ), 
                    React.createElement("article", {className: "module width_1_3_quarter", style: {height: '150px'}}, 
                      React.createElement("header", null, React.createElement("h3", null, "Otra acción")), 
                      React.createElement("div", {className: "innerDiv"}, 
                        subEstadisticaOtraAccion
                      )
                    ), 
                    React.createElement("article", {className: "module width_half", style: {height: '221px'}}, 
                      React.createElement("header", null, React.createElement("h3", null, "Temas de interés")), 
                      React.createElement("div", {className: "innerDiv scroll_y"}, 
                        subEstadisticaTemas
                      )
                    ), 
                    React.createElement("article", {className: "module width_half", style: {height: '221px'}}, 
                      React.createElement("header", null, React.createElement("h3", null, "Sub-temas")), 
                      React.createElement("div", {className: "innerDiv scroll_y"}, 
                        subEstadisticaSubTemas
                      )
                    ), 
                    React.createElement("article", {className: "module width_2_full", style: {height: '400px'}}, 
                      React.createElement("header", null, React.createElement("h3", null, "Acción")), 
                      React.createElement("div", {className: "innerDiv scroll_y", style: {height: '357px'}}, 
                        subEstadisticaAcciones
                      )
                    )
                  )
                )
              )
            ), 

            React.createElement("div", {className: "tab_content", style: styleTabGrafics}, 
              React.createElement("table", {className: "tablesorter", cellSpacing: "0"}, 
                React.createElement("tr", null, 
                  React.createElement("td", null, 
                    React.createElement("article", {className: "module width_half", style: {height: '300px'}}, 
                      React.createElement("div", {id: "pieSector", style: {minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}})
                    ), 
                    React.createElement("article", {className: "module width_half", style: {height: '300px'}}, 
                      React.createElement("div", {id: "pieSubSector", style: {minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}})
                    ), 
                    React.createElement("article", {className: "module width_half", style: {height: '300px'}}, 
                      React.createElement("div", {id: "pieTiempos", style: {minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}})
                    ), 
                    React.createElement("article", {className: "module width_half", style: {height: '300px'}}, 
                      React.createElement("div", {id: "pieMedios", style: {minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}})
                    ), 
                    React.createElement("article", {className: "module width_half", style: {height: '300px'}}, 
                      React.createElement("div", {id: "pieCumplimientos", style: {minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}})
                    ), 
                    React.createElement("article", {className: "module width_half", style: {height: '300px'}}, 
                      React.createElement("div", {id: "pieEstatus", style: {minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}})
                    ), 
                    React.createElement("article", {className: "module width_half", style: {height: '300px'}}, 
                      React.createElement("div", {id: "pieNiveles", style: {minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}})
                    ), 
                    React.createElement("article", {className: "module width_half", style: {height: '300px'}}, 
                      React.createElement("div", {id: "pieInversiones", style: {minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}})
                    ), 
                    React.createElement("article", {className: "module width_half", style: {height: '300px'}}, 
                      React.createElement("div", {id: "pieOtraAcciones", style: {minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}})
                    ), 
                    React.createElement("article", {className: "module width_half", style: {height: '300px'}}, 
                      React.createElement("div", {id: "pieTemas", style: {minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}})
                    ), 
                    React.createElement("article", {className: "module width_2_full", style: {height: '400px'}}, 
                      React.createElement("div", {id: "pieSubTemas", style: {minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}})
                    ), 

                    React.createElement("article", {className: "module width_2_full", style: {height: '400px'}}, 
                      React.createElement("div", {id: "pieAccionesSubTema_0", style: {minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}})
                    ), 
                    React.createElement("article", {className: "module width_2_full", style: {height: '400px'}}, 
                      React.createElement("div", {id: "pieAccionesSubTema_1", style: {minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}})
                    ), 
                    React.createElement("article", {className: "module width_2_full", style: {height: '400px'}}, 
                      React.createElement("div", {id: "pieAccionesSubTema_2", style: {minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}})
                    ), 
                    React.createElement("article", {className: "module width_2_full", style: {height: '400px'}}, 
                      React.createElement("div", {id: "pieAccionesSubTema_3", style: {minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}})
                    ), 
                    React.createElement("article", {className: "module width_2_full", style: {height: '400px'}}, 
                      React.createElement("div", {id: "pieAccionesSubTema_4", style: {minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}})
                    ), 
                    React.createElement("article", {className: "module width_2_full", style: {height: '400px'}}, 
                      React.createElement("div", {id: "pieAccionesSubTema_5", style: {minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}})
                    ), 
                    React.createElement("article", {className: "module width_2_full", style: {height: '400px'}}, 
                      React.createElement("div", {id: "pieAccionesSubTema_6", style: {minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}})
                    ), 
                    React.createElement("article", {className: "module width_2_full", style: {height: '400px'}}, 
                      React.createElement("div", {id: "pieAccionesSubTema_7", style: {minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}})
                    ), 
                    React.createElement("article", {className: "module width_2_full", style: {height: '400px'}}, 
                      React.createElement("div", {id: "pieAccionesSubTema_8", style: {minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}})
                    )
                  )
                )
              )
            )

          )
        ), 
        React.createElement("div", {className: "spacer"})
      )
    );
  }
});

module.exports = EstadisticasVisitante;