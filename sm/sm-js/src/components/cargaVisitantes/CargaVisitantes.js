'use strict';

var Router = require('../../libs/react-router/index.js');
var Label = require('../../utils/Label.js');
var ErrorCodes = require('../../utils/ErrorCodes.js');
var ComponentState = require('../../utils/ComponentState.js');
var Constants = require('../../utils/Constants.js');
var Context = require('../../utils/Context.js');
var ValidaService = require('../../utils/ValidaService.js');
var PopUpReact = require('../../components/commons/PopUpReact.js');
var DatePickerReact = require('../../components/commons/DatePickerReact.js');
var highCharts = require('../../utils/HighCharts.js');

var adminService = require('../../modulos/MyTAdminService.js');

var CargaVisitante = React.createClass({displayName: "CargaVisitante",
  mixins: [ Router.Navigation ],
 	getInitialState: function() {
    return {
      shouldMount: true,
      componentState: ComponentState.NORMAL_STATE
    };
  },
  getDefaultProps: function() {
    return {
      storage: new Context()
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
    var self = this;

    if(validaSession()) {

    } else {
      this.setState({
        shouldMount: false
      });
      this.transitionTo('Login');
    }
  },
  componentWillReceiveProps: function(nextProps) {
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  onFail: function(err) {
    console.log(err);
    this.setState({
      componentState: ComponentState.ERROR_STATE
    });
    this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
  },
  onError: function(err) {
    console.log(err);
    
    switch(err.errorCode) {
      case ErrorCodes.ERR_01:
        this.setState({
          componentState: ComponentState.ERROR_STATE
        });
        this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
      break;

      case ErrorCodes.ERR_03:
        this.setState({
          componentState: ComponentState.ERROR_STATE
        });
        this.refs.popUpAlert.show(ErrorCodes.ERR_03_MSG);
      break;

      default:
        console.log('Error no categorizado...');
      break;
    } 
  },
  onClickReset: function() {
    this.refs.fechaInicio.reset();
    this.refs.fechaFinal.reset();
    this.setState({
      shouldMount: true,
      componentState: ComponentState.NORMAL_STATE
    });
  },
  onClickBuscar: function() {
    var self = this;

    var onSuccess = function(res) {
      var componentState = ComponentState.NORMAL_STATE;

      self.setState({

      });
    };


    var params = {
    };

    adminCargaVisitanteService.getCargasVisitante(params, onSuccess, this.onError, this.onFail);
  },
  onDatePickedFechaInicio: function(datePicked, evt) {
    var filtro = this.state.filtro;
    filtro.fechaInicio = datePicked;

    this.setState({
      filtro: filtro
    });
  },
  onDatePickedFechaFinal: function(datePicked, evt) {
    var filtro = this.state.filtro;
    filtro.fechaFinal = datePicked;

    this.setState({
      filtro: filtro
    });
  },
  render: function() {
    var styleShow = '';
    var styleHide = '';
    var fechaInicioFormat = '';
    var fechaFinalFormat = '';

    if(!this.state.shouldMount) {
      return (React.createElement("div", null));
    }
    //hacer algo dependiendo el estado del componente
    switch(this.state.componentState) {
      case ComponentState.NORMAL_STATE:
      break;

      case ComponentState.EXITO_STATE:
      break;

      case ComponentState.ERROR_STATE:
      break;

      case ComponentState.INFO_STATE:
      break;

      case ComponentState.ALERT_STATE:
      break;

      case ComponentState.LOADING_STATE:
      break;

      case ComponentState.ASK_STATE:
      break;
    }
    //formaterar las fechas de busqueda para mostrarlas
    if(this.state.fechaInicioBusqueda != undefined) {
      fechaInicioFormat = (this.state.fechaInicioBusqueda.getDate() + '/' + (this.state.fechaInicioBusqueda.getMonth() + 1) + '/' + this.state.fechaInicioBusqueda.getFullYear());
    }

    if(this.state.fechaFinalBusqueda != undefined) {
      fechaFinalFormat = (this.state.fechaFinalBusqueda.getDate() + '/' + (this.state.fechaFinalBusqueda.getMonth() + 1) + '/' + this.state.fechaFinalBusqueda.getFullYear());
    }

    if(this.state.isResultBusqueda) {
      exportExcel = (React.createElement("a", {className: "IconExcelStyle", href: "#", onClick: this.onClickExportExcel}));
    }

    return (
      React.createElement("div", {id: "container-gral"}, 
        React.createElement(PopUpReact, {ref: "popUpAlert", type: "alert", popUpStyle: "PopUpReactAlert"}), 

        React.createElement("article", {className: "module width_full"}, 
          React.createElement("header", null, React.createElement("h3", null, "Filtros de Búsqueda")), 
          React.createElement("div", {className: "module_content"}, 

            React.createElement("fieldset", {style: {width:'48%', float:'left'}}, 
              React.createElement("div", {style: {width:'100%', float:'left', marginRight: '0%'}}, 
                React.createElement("label", {style: {width: '100%'}}, "Rango de fechas"), 
                React.createElement("div", {style: {width:'100%', float:'left', marginRight: '0%'}}, 
                  React.createElement(DatePickerReact, {ref: "fechaInicio", idCal: "fechaInicio", inputLabel: "Fecha Inicio:", onDatePicked: this.onDatePickedFechaInicio})
                ), 
                React.createElement("div", {style: {width:'100%', float:'left', marginRight: '0%'}}, 
                  React.createElement(DatePickerReact, {ref: "fechaFinal", idCal: "fechaFinal", inputLabel: "Fecha Final:", onDatePicked: this.onDatePickedFechaFinal})
                )
              )
            ), 

            React.createElement("div", {className: "clear"})
          ), 
          React.createElement("footer", null, 
            React.createElement("div", {className: "submit_link"}, 
              React.createElement("input", {type: "button", value: "BUSCAR", className: "alt_btn", onClick: this.onClickBuscar}), 
              React.createElement("input", {type: "button", value: "LIMPIAR", onClick: this.onClickReset})
            )
          )
        ), 

        React.createElement("nav", null, 
          React.createElement("ul", {id: "menuNav"}, 
            React.createElement("li", {className: activeTabSubTotal, style: {width: '50%'}, onClick: this.onClickTabCargaVisitantes}, "Carga de visitantes"), 
            React.createElement("li", {className: activeTabResult, style: {width: '50%'}, onClick: this.onClickTabGraficasEstadisticas}, "Gráfias y estadísticas")
          )
        ), 
        
        React.createElement("article", {className: "module width_full"}, 
          React.createElement("header", null, 
            React.createElement("div", {className: "AlignContainer"}, 
              React.createElement("div", {className: "AlignLeft", style: {width: '100%'}}, 
                React.createElement("h3", {className: "tabs_involved"}, "Carga de visitantes")


              )
            )
          ), 

          React.createElement("div", {className: "tab_container"}, 
            React.createElement("div", {className: "tab_content", style: styleTabResult}
              
            ), 

            React.createElement("div", {className: "tab_content", style: styleTabSubTotal}
              
            )

          )
        ), 
        React.createElement("div", {className: "spacer"})
      )
    );
  }
});

module.exports = EstadisticasVisitante;