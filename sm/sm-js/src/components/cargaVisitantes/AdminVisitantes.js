'use strict';

var Router = require('../../libs/react-router/index.js');
/* # seccion para declarar utilerias # */
/* # seccion para declarar compronentes comunes # */
/* # seccion para declarar servicios # */
/* # Seccion de componentes a los que depende # */
var CargaVisitantes = require('../../components/adminVisitantes/CargaVisitantes.js');
/*################################################*/
/*###       Administracion de acciones         ###*/
/*################################################*/
var AdminVisitantes = React.createClass({displayName: "AdminVisitantes",
  mixins: [ Router.Navigation ],
  getInitialState: function() {
    return {
      usuario: ctx.get('usuario'),
      shouldMount: true
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
    var self = this;

    if(validaSession()) {
      this.refs.cargaVisitantes.show();

    } else {
      this.setState({
        shouldMount: false
      });
      this.transitionTo('Login');
    }
  },
  componentWillReceiveProps: function(nextProps) {
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  render: function() {
    var self = this;

    if(!this.state.shouldMount) {
      return (React.createElement("div", null));
    }

    return (
      React.createElement("div", {style: {width: '100%', height: '100%'}}, 
        React.createElement(CargaVisitantes, {ref: "cargaVisitantes"})
      )
    );
  }
});

module.exports = AdminVisitantes;