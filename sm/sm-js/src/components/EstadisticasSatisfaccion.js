'use strict';

var Router = require('../libs/react-router/index.js');
var adminService = require('../modulos/MyTAdminService.js');
var ErrorCodes = require('../utils/ErrorCodes.js');
var ComponentState = require('../utils/ComponentState.js');
var Constants = require('../utils/Constants.js');
var PopUpReact = require('../components/commons/PopUpReact.js');

var EstadisticasSatisfaccion = React.createClass({displayName: "EstadisticasSatisfaccion",
  mixins: [ Router.Navigation ],
 	getInitialState: function() {
    return {
      shouldMount: true,
      componentState: ComponentState.NORMAL_STATE,
      preguntas: undefined
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
    var self = this;

    var onSuccess = function(res) {
      self.setState({
        preguntas: res.preguntas
      });
    };

    if(validaSession()) {
      var params = {};
      adminService.getEstadisticasSatisfaccion(params, onSuccess, this.onError, this.onFail);

    } else {
      this.setState({
        shouldMount: false
      });
      this.transitionTo('Login');
    }
  },
  componentWillReceiveProps: function(nextProps) {
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  onFail: function(err) {
    console.log(err);
    this.setState({
      componentState: ComponentState.ERROR_STATE
    });
    this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
  },
  onError: function(err) {
    console.log(err);

    switch(err.errorCode) {
      case ErrorCodes.ERR_01:
        this.setState({
          componentState: ComponentState.ERROR_STATE
        });
        this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
      break;

      case ErrorCodes.ERR_03:
        this.setState({
          componentState: ComponentState.ERROR_STATE
        });
        this.refs.popUpAlert.show(ErrorCodes.ERR_03_MSG);
      break;

      default:
        console.log('Error no categorizado...');
      break;
    } 

    scrollMainTo();
  },
  render: function() {
    var preguntasHTML = [];

    if(!this.state.shouldMount) {
      return (React.createElement("div", null));
    }

    switch(this.state.componentState) {
      case ComponentState.NORMAL_STATE:
      break;

      case ComponentState.EXITO_STATE:
      break;

      case ComponentState.ERROR_STATE:
      break;

      case ComponentState.INFO_STATE:
      break;

      case ComponentState.ALERT_STATE:
      break;
    }
    //crear secciones de las preguntas
    if(this.state.preguntas != undefined) {
      this.state.preguntas.forEach(function(pregunta) {
        var calificacionesHTML = [];
        var respuestasHTML = [];
        var porcentajesHTML = [];
        var TOTAL_CALIFICACIONES = 10;
        var i = 0;
        var pocentajeTotal = 0.0;

        var getResultado = function(calificacion) {
          var resultado = 0;
          pregunta.respuestas.every(function(resp) {
            if(resp.calificacion == calificacion) {
              resultado = resp.resultado;
              return false;
            }

            return true;
          });

          return resultado;
        };

        var getPocentaje = function(calificacion) {
          var porcentaje = 0;
          pregunta.respuestas.every(function(resp) {
            if(resp.calificacion == calificacion) {
              porcentaje = resp.porcentaje;
              return false;
            }

            return true;
          });

          return porcentaje;
        };

        for(i = 0; i <= TOTAL_CALIFICACIONES; i++) {
          var resultado = getResultado(i);
          var porcentaje = getPocentaje(i);

          calificacionesHTML.push(React.createElement("th", null, i));
          respuestasHTML.push(React.createElement("td", null, resultado));
          porcentajesHTML.push(React.createElement("td", null, porcentaje, "%"));
          pocentajeTotal += porcentaje;
        }

        calificacionesHTML.push(React.createElement("th", null, "TOTAL"));
        respuestasHTML.push(React.createElement("td", null, pregunta.total));
        porcentajesHTML.push(React.createElement("td", null, Math.round(pocentajeTotal.toFixed(2)), "%"));

        preguntasHTML.push(
          React.createElement("div", {className: "tab_container"}, 
            React.createElement("div", {id: "tab1", className: "tab_content", style: {paddingBottom: '15px'}}, 
              React.createElement("table", {className: "tablesorter", cellSpacing: "0"}, 
                React.createElement("thead", null, 
                  React.createElement("tr", null, 
                    React.createElement("th", {colSpan: "13", className: "question"}, pregunta.descripcion)
                  ), 
                  React.createElement("tr", null, 
                    React.createElement("th", null, "CALIFICACIÓN:"), 
                    calificacionesHTML
                  )
                ), 
                React.createElement("tbody", null, 
                  React.createElement("tr", null, 
                    React.createElement("td", null, "RESPUESTAS:"), 
                    respuestasHTML
                  ), 
                  React.createElement("tr", null, 
                    React.createElement("td", null, "PORCENTAJE:"), 
                    porcentajesHTML
                  )
                )
              )
            )
          )
        );
      });
    }

    return (
      React.createElement("div", {id: "container-gral"}, 
        React.createElement(PopUpReact, {ref: "popUpAlert", type: "alert", popUpStyle: "PopUpReactAlert"}), 
        React.createElement("article", {className: "module width_full"}, 
          React.createElement("header", null, 
            React.createElement("h3", {className: "tabs_involved"}, "Resultado de la Búsqueda")
          ), 
          preguntasHTML
        ), 
        React.createElement("div", {className: "spacer"})
      )
    );
  }
});

module.exports = EstadisticasSatisfaccion;