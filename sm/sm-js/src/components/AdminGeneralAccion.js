'use strict';

var Router = require('../libs/react-router/index.js');
/* # seccion para declarar utilerias # */
var Clone = require('../utils/Clone.js');
var Context = require('../utils/Context.js');
var Label = require('../utils/Label.js');
var ErrorCodes = require('../utils/ErrorCodes.js');
var ErrorMsg = require('../utils/ErrorMsg.js');
var ComponentState = require('../utils/ComponentState.js');
var Constants = require('../utils/Constants.js');
/* # seccion para declarar compronentes comunes # */
var PopUpReact = require('../components/commons/PopUpReact.js');
/* # seccion para declarar servicios # */
var generalAccionService = require('../modulos/GeneralAccionService.js');
var validaService = require('../utils/ValidaService.js');

var AdminGeneralAccion = React.createClass({displayName: "AdminGeneralAccion",
  mixins: [ Router.Navigation ],
  getInitialState: function() {
    return {
      usuario: ctx.get('usuario'),
      shouldMount: true,
      componentState: ComponentState.NORMAL_STATE,
      generalAcciones: undefined,
      filtroGeneralAccion: ''
    };
  },
  getDefaultProps: function() {
    return {
      isNormal: 1,
      isEditar: 2,
      storage: new Context()
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
    var self = this;

    var onSuccess = function(res) {
      //estableser todos los parametros a un estado normal
      res.generalAcciones.forEach(function(generalAccion) {
        generalAccion.isState = self.props.isNormal; 
      });

      self.setState({
        generalAcciones: res.generalAcciones
      });
    };

    if(validaSession()) {
      var params = {};
      generalAccionService.getGeneralAcciones(params, onSuccess, this.onError, this.onFail);

    } else {
      this.setState({
        shouldMount: false
      });
      this.transitionTo('Login');
    }
  },
  componentWillReceiveProps: function(nextProps) {
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  onFail: function(err) {
    console.log(err);
    this.props.storage.clean();
    this.setState({
      componentState: ComponentState.ERROR_STATE,
    });
    this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
  },
  onError: function(err) {
    console.log(err);
    this.props.storage.clean();

    switch(err.errorCode) {
      case ErrorCodes.ERR_01:
        this.setState({
          componentState: ComponentState.ERROR_STATE,
        });
        this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
      break;

      case ErrorCodes.ERR_03:
        this.setState({
          componentState: ComponentState.ERROR_STATE,
        });
        this.refs.popUpAlert.show(ErrorCodes.ERR_03_MSG);
      break;

      case ErrorCodes.ERR_04:
        this.setState({
          componentState: ComponentState.ERROR_STATE,
        });
        this.refs.popUpAlert.show(err.message);
      break;

      default:
        console.log('Error no categorizado...');
      break;
    }

    scrollMainTo();
  },
  onClickIrEditar: function(generalAccion, index, evt) {
    evt.preventDefault();
    var generalAcciones = this.state.generalAcciones;

    generalAccion.nombreOrg = generalAccion.nombre;
    generalAccion.isState = this.props.isEditar;
    generalAcciones[index] = generalAccion;

    this.setState({
      generalAcciones: generalAcciones
    });
  },
  onClickSiEditar: function(generalAccion, index, evt) {
    evt.preventDefault();

    var result = this.validaGeneralAccion(generalAccion);

    if(!result.isError) {
      this.props.storage.put('GENERAL_ACCION_TO_UPDATE', Clone.clone(generalAccion));
      this.refs.popUpAsk.show(Label.ASK_MODIF_GENERAL_ACCION + '  [NOMBRE = ' + generalAccion.nombreOrg + ', NUEVO = ' + generalAccion.nombre + ']' + ' ?');

    } else {
      this.refs.popUpAlert.show(result.errMsg);
    }
  },
  validaGeneralAccion: function(generalAccion) {
    var result = {
      isError: false,
      errMsg: '',
      ejemploFormato: ''
    };

    if(validaService.isEmpty(generalAccion.nombre.trim())) {
      result = {
        isError: true,
        errMsg: ErrorMsg.ERROR_GENERAL_ACCION_FORMATO_INCORRECTO,
        ejemploFormato: 'Nombre General Accion'
      };
    }

    return result;
  },
  onClickNoEditar: function(generalAccion, index, evt) {
    evt.preventDefault();
    var generalAcciones = this.state.generalAcciones;

    this.props.storage.clean();
    generalAccion.nombre = generalAccion.nombreOrg;
    generalAccion.nombreOrg = undefined;
    generalAccion.isState = this.props.isNormal;
    generalAcciones[index] = generalAccion;

    this.setState({
      generalAcciones: generalAcciones
    });
  },
  onChangeGeneralAccionValue: function(generalAccion, index, evt) {
    var generalAcciones = this.state.generalAcciones;
    generalAccion.nombre = evt.target.value;
    generalAcciones[index] = generalAccion;

    this.setState({
      generalAcciones: generalAcciones
    });
  },
  onClickAceptarEditar: function(evt) {
    var self = this;
    this.refs.popUpAsk.hide();
    var generalAccionToUpdate = this.props.storage.get('GENERAL_ACCION_TO_UPDATE');

    var onSuccess = function(res) {
      var generalAcciones = self.state.generalAcciones;
      var indexTmp = self.getIndexGeneralAccion(generalAccionToUpdate.idGeneralAccion);
      var generalAccionUpdated = self.state.generalAcciones[indexTmp];

      self.props.storage.clean();
      generalAccionUpdated.isState = self.props.isNormal;
      generalAccionUpdated.nombreOrg = undefined;
      generalAcciones[indexTmp] = generalAccionUpdated;

      self.setState({
        generalAcciones: generalAcciones
      });

      self.refs.popUpAlert.show(Label.REGISTRO_MODIF_SUCCESS);
    };

    var params = {
      idGeneralAccion: generalAccionToUpdate.idGeneralAccion,
      nombre: generalAccionToUpdate.nombre
    };
    generalAccionService.updateGeneralAccion(params, onSuccess, this.onError, this.onFail);
  },
  onClickCancelarEditar: function(evt) {
    this.refs.popUpAsk.hide();
    this.refs.popUpAskActivo.hide();
    this.props.storage.clean();
  },
  getIndexGeneralAccion: function(idGeneralAccion) {
    var indexTmp = undefined;

    if(this.state.generalAcciones != undefined) {
      this.state.generalAcciones.every(function(generalAccion, index) {
        if(generalAccion.idGeneralAccion == idGeneralAccion) {
          indexTmp = index;
          return false;
        }

        return true;
      });
    }

    return indexTmp;
  },
  onChangeActivo: function(generalAccion, index, evt) {
    evt.preventDefault();
    this.props.storage.clean();

    this.props.storage.put('GENERAL_ACCION_TO_UPDATE', Clone.clone(generalAccion));
    if(generalAccion.activo) {
      //preguntar por desactivar
      this.refs.popUpAskActivo.show(Label.ASK_DESACTIVAR_GENERAL_ACCION + generalAccion.nombre + ' ?');

    } else {
      //preguntar por activas
      this.refs.popUpAskActivo.show(Label.ASK_ACTIVAR_GENERAL_ACCION + generalAccion.nombre + ' ?');
    }
  },
  onClickAceptarActivo: function(evt) {
    this.refs.popUpAskActivo.hide();
    var self = this;
    var generalAccionToUpdate = this.props.storage.get('GENERAL_ACCION_TO_UPDATE');
    var irActivo = false;

    var onSuccess = function(res) {
      var generalAcciones = self.state.generalAcciones;
      var indexTmp = self.getIndexGeneralAccion(generalAccionToUpdate.idGeneralAccion);
      var generalAccionUpdated = self.state.generalAcciones[indexTmp];

      self.props.storage.clean();
      generalAccionUpdated.isState = self.props.isNormal;
      generalAccionUpdated.activo = irActivo;
      generalAcciones[indexTmp] = generalAccionUpdated;

      self.setState({
        generalAcciones: generalAcciones
      });

      self.refs.popUpAlert.show(Label.REGISTRO_MODIF_SUCCESS);
    };

    if(generalAccionToUpdate.activo) {
      irActivo = false;

    } else {
      irActivo = true;
    }

    var params = {
      idGeneralAccion: generalAccionToUpdate.idGeneralAccion,
      activo: irActivo
    };
    generalAccionService.changeEstatusGeneralAccion(params, onSuccess, this.onError, this.onFail);
  },
  onChangeFiltroGeneralAccion: function(evt) {
    var self = this;
    this.props.storage.clean();

    var onSuccess = function(res) {
      //estableser todos los parametros a un estado normal
      res.generalAcciones.forEach(function(generalAccion) {
        generalAccion.isState = self.props.isNormal; 
      });

      self.setState({
        generalAcciones: res.generalAcciones
      });
    };

    this.setState({
      filtroGeneralAccion: evt.target.value
    });

    var params = {
      nombre: evt.target.value
    };
    generalAccionService.getGeneralAccionesByFiltro(params, onSuccess, this.onError, this.onFail);    
  },
  onClickNewGeneralAccion: function(evt) {
    evt.preventDefault();
    this.refs.popUpAskNewGeneralAccion.show();
  },
  onChangeNewGeneralAccion: function(evt) {
    this.setState({
      newGeneralAccion: evt.target.value
    });
  },
  onClickCancelarNewGeneralAccion: function(evt) {
    this.refs.popUpAskNewGeneralAccion.hide();
    this.setState({
      newGeneralAccion: ''
    });
  },
  onClickAceptarNewGeneralAccion: function(evt) {
    var self = this;

    var onSuccess = function(res) {
      self.refs.popUpAskNewGeneralAccion.hide();
      self.recargaGeneralAccion();
      self.refs.popUpAlert.show(Label.REGISTRO_CREATE_SUCCESS);
    };

    if(!validaService.isEmpty(this.state.newGeneralAccion)) {
      var params = {
        nombre: this.state.newGeneralAccion
      };
      generalAccionService.createGeneralAccion(params, onSuccess, this.onError, this.onFail);
      
    } else {
      this.refs.popUpAlert.show(ErrorMsg.ERROR_GENERAL_ACCION_FORMATO_INCORRECTO);
    }
  },
  onClickEliminarGeneralAccion: function(generalAccion, index, evt) {
    evt.preventDefault();
    this.props.storage.put('GENERAL_ACCION_TO_DELETE', Clone.clone(generalAccion));
    this.refs.popUpAskEliminarGeneralAccion.show(Label.ASK_ELIMINAR_GENERAL_ACCION + ' ' + generalAccion.nombre + ' ?');
  },
  onAceptaEliminarGeneralAccion: function(evt) {
    var self = this;
    var generalAccion = this.props.storage.get('GENERAL_ACCION_TO_DELETE');

    var onSuccess = function(res) {
      //estableser todos los parametros a un estado normal
      self.props.storage.clean();
      self.refs.popUpAskEliminarGeneralAccion.hide();
      self.recargaGeneralAccion();
      self.refs.popUpAlert.show(Label.REGISTRO_ELIMINADO_SUCCESS);
    };

    var params = {
      idGeneralAccion: generalAccion.idGeneralAccion
    };
    generalAccionService.eliminarGeneralAccion(params, onSuccess, this.onError, this.onFail);
  },
  onCancelarEliminarGeneralAccion: function(evt) {
    this.props.storage.clean();
    this.refs.popUpAskEliminarGeneralAccion.hide();
  },
  recargaGeneralAccion: function() {
    var self = this;

    var onSuccess = function(res) {
      //estableser todos los parametros a un estado normal
      res.generalAcciones.forEach(function(generalAccion) {
        generalAccion.isState = self.props.isNormal; 
      });

      self.setState({
        generalAcciones: res.generalAcciones
      });
    };

    var params = {};
    generalAccionService.getGeneralAcciones(params, onSuccess, this.onError, this.onFail);
  },
  render: function() {
    var self = this;
    var generalAccionesRows = [];
    var newGeneralAccionHeaderHTML = [];
    var newGeneralAccionBodyHTML = [];
    var eliminarGeneralAccionHeaderHTML = [];
    var eliminarGeneralAccionBodyHTML = [];

    if(!this.state.shouldMount) {
      return (React.createElement("div", null));
    }

    switch(this.state.componentState) {
      case ComponentState.NORMAL_STATE:
      break;

      case ComponentState.EXITO_STATE:
      break;

      case ComponentState.ERROR_STATE:
      break;

      case ComponentState.INFO_STATE:
      break;

      case ComponentState.ALERT_STATE:
      break;
    }
    //crear lista de etiquetas tr para la tabla de general acciones
    if(this.state.generalAcciones != undefined) {
      this.state.generalAcciones.forEach(function(generalAccion, index) {
        var accionesHTML = [];
        var valorHTML = [];
        var onClickIrEditar = function(generalAccion, index, evt) {
          self.onClickIrEditar(generalAccion, index, evt);
        }.bind(self, generalAccion, index);

        var onChangeGeneralAccionValue = function(generalAccion, index, evt) {
          self.onChangeGeneralAccionValue(generalAccion, index, evt);
        }.bind(self, generalAccion, index);

        var onClickSiEditar = function(generalAccion, index, evt) {
          self.onClickSiEditar(generalAccion, index, evt);
        }.bind(self, generalAccion, index);

        var onClickNoEditar = function(generalAccion, index, evt) {
          self.onClickNoEditar(generalAccion, index, evt);
        }.bind(self, generalAccion, index);

        var onChangeActivo = function(generalAccion, index, evt) {
          self.onChangeActivo(generalAccion, index, evt);
        }.bind(self, generalAccion, index);

        var onClickEliminarGeneralAccion = function(generalAccion, index, evt) {
          self.onClickEliminarGeneralAccion(generalAccion, index, evt);
        }.bind(self, generalAccion, index);

        if(generalAccion.isState == self.props.isNormal) {
          valorHTML.push(generalAccion.nombre);
          accionesHTML.push(React.createElement("a", {className: "IconEditStyle", href: "#", onClick: onClickIrEditar}));
          accionesHTML.push(React.createElement("a", {className: "IconTrashStyle", href: "#", onClick: onClickEliminarGeneralAccion}));
          accionesHTML.push(
            React.createElement("label", {className: "switch switch-green"}, 
              React.createElement("input", {type: "checkbox", className: "switch-input", checked: generalAccion.activo, onChange: onChangeActivo}), 
              React.createElement("span", {className: "switch-label", "data-on": "On", "data-off": "Off"}), 
              React.createElement("span", {className: "switch-handle"})
            )
          );

        } else if(generalAccion.isState == self.props.isEditar) {
          valorHTML.push(
            React.createElement("input", {type: "text", style: {width:'50%'}, placeholder: "Nombre de General Acción", 
              maxLength: "100", value: generalAccion.nombre, onChange: onChangeGeneralAccionValue})
          );
          accionesHTML.push(React.createElement("a", {className: "IconOKStyle", href: "#", onClick: onClickSiEditar}));
          accionesHTML.push(React.createElement("a", {className: "IconCancelStyle", href: "#", onClick: onClickNoEditar}));
        }

        generalAccionesRows.push(
          React.createElement("tr", null, 
            React.createElement("td", {style: {paddingTop: '5px', paddingBottom: '5px'}}, 
              valorHTML
            ), 
            React.createElement("td", {style: {paddingTop: '5px', paddingBottom: '5px'}}, 
              accionesHTML
            )
          ) 
        );
      });
    }
    //crear contendo html para popup nuevo general accion
    newGeneralAccionHeaderHTML.push(
      React.createElement("p", null, "Crear nuevo general de acción")
    );
    newGeneralAccionBodyHTML.push(
      React.createElement("div", null, 
        React.createElement("label", {htmlFor: "NombreGeneralAccion"}, "Nombre: "), 
        React.createElement("input", {name: "NombreGeneralAccion", type: "text", style: {width:'100%'}, placeholder: "Nombre General Acción", 
          maxLength: "100", value: this.state.newGeneralAccion, onChange: this.onChangeNewGeneralAccion})
      )
    );
    //crear contenido html para la eliminacion de general accion
    eliminarGeneralAccionHeaderHTML.push(
      React.createElement("p", null, "Eliminación de general acción")
    );
   
    return (
      React.createElement("div", {id: "container-gral"}, 
        React.createElement(PopUpReact, {ref: "popUpAlert", type: "alert", popUpStyle: "PopUpReactAlert"}), 
        React.createElement(PopUpReact, {ref: "popUpAsk", type: "ask", 
          buttons: [
            {name: 'Aceptar', label: 'Aceptar', onClick: this.onClickAceptarEditar},
            {name: 'Cancelar', label: 'Cancelar', onClick: this.onClickCancelarEditar}
        ]}), 
        React.createElement(PopUpReact, {ref: "popUpAskActivo", type: "ask", 
          buttons: [
            {name: 'Aceptar', label: 'Aceptar', onClick: this.onClickAceptarActivo},
            {name: 'Cancelar', label: 'Cancelar', onClick: this.onClickCancelarEditar}
        ]}), 
        React.createElement(PopUpReact, {ref: "popUpAskNewGeneralAccion", type: "ask", htmlHeaderContent: newGeneralAccionHeaderHTML, 
          htmlBodyContent: newGeneralAccionBodyHTML, 
          buttons: [
            {name: 'Aceptar', label: 'Aceptar', onClick: this.onClickAceptarNewGeneralAccion},
            {name: 'Cancelar', label: 'Cancelar', onClick: this.onClickCancelarNewGeneralAccion}
        ]}), 
        React.createElement(PopUpReact, {ref: "popUpAskEliminarGeneralAccion", type: "ask", htmlHeaderContent: eliminarGeneralAccionHeaderHTML, 
          buttons: [
            {name: 'Aceptar', label: 'Aceptar', onClick: this.onAceptaEliminarGeneralAccion},
            {name: 'Cancelar', label: 'Cancelar', onClick: this.onCancelarEliminarGeneralAccion}
        ]}), 

        React.createElement("article", {className: "module width_full"}, 
          React.createElement("input", {type: "text", style: {width:'100%'}, placeholder: "Buscar...", 
            maxLength: "100", value: this.state.filtroGeneralAccion, onChange: this.onChangeFiltroGeneralAccion})
        ), 
        
        React.createElement("article", {className: "module width_full"}, 
          React.createElement("header", null, 
            React.createElement("div", {className: "AlignContainer"}, 
              React.createElement("div", {className: "AlignLeft", style: {width: '95%'}}, 
                React.createElement("h3", {className: "tabs_involved"}, "Lista de general de acciones")
              ), 
              React.createElement("div", {className: "AlignRight", style: {width: '5%', marginTop: '5px', textAlign: 'center'}}, 
                React.createElement("a", {className: "IconNewStyle", href: "#", onClick: this.onClickNewGeneralAccion})
              )
            )
          ), 
          React.createElement("div", {className: "tab_container"}, 
            React.createElement("div", {id: "tab1", className: "tab_content"}, 
              React.createElement("table", {className: "tablesorter", cellSpacing: "0"}, 
                React.createElement("thead", null, 
                  React.createElement("tr", null, 
                    React.createElement("th", {style: {width: '40%'}}, "NOMBRE"), 
                    React.createElement("th", {style: {width: '10%'}})
                  )
                ), 
                React.createElement("tbody", null, 
                  generalAccionesRows
                )
              )
            )
          )
        ), 
        React.createElement("div", {className: "spacer"})
      )
    );
  }
});

module.exports = AdminGeneralAccion;