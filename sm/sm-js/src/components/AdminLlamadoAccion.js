'use strict';

var Router = require('../libs/react-router/index.js');
/* # seccion para declarar utilerias # */
var Clone = require('../utils/Clone.js');
var Context = require('../utils/Context.js');
var Label = require('../utils/Label.js');
var ErrorCodes = require('../utils/ErrorCodes.js');
var ErrorMsg = require('../utils/ErrorMsg.js');
var ComponentState = require('../utils/ComponentState.js');
var Constants = require('../utils/Constants.js');
/* # seccion para declarar compronentes comunes # */
var PopUpReact = require('../components/commons/PopUpReact.js');
/* # seccion para declarar servicios # */
var llamadoAccionService = require('../modulos/LlamadoAccionService.js');
var validaService = require('../utils/ValidaService.js');

var AdminLlamadoAccion = React.createClass({displayName: "AdminLlamadoAccion",
  mixins: [ Router.Navigation ],
  getInitialState: function() {
    return {
      usuario: ctx.get('usuario'),
      shouldMount: true,
      componentState: ComponentState.NORMAL_STATE,
      llamadoAcciones: undefined,
      filtroLlamadoAccionesAccion: ''
    };
  },
  getDefaultProps: function() {
    return {
      isNormal: 1,
      isEditar: 2,
      storage: new Context()
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
    var self = this;

    var onSuccess = function(res) {
      //estableser todos los parametros a un estado normal
      res.llamadoAcciones.forEach(function(llamadoAccion) {
        llamadoAccion.isState = self.props.isNormal; 
      });

      self.setState({
        llamadoAcciones: res.llamadoAcciones
      });
    };

    if(validaSession()) {
      var params = {};
      llamadoAccionService.getLlamadoAcciones(params, onSuccess, this.onError, this.onFail);

    } else {
      this.setState({
        shouldMount: false
      });
      this.transitionTo('Login');
    }
  },
  componentWillReceiveProps: function(nextProps) {
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  onFail: function(err) {
    console.log(err);
    this.props.storage.clean();
    this.setState({
      componentState: ComponentState.ERROR_STATE,
    });
    this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
  },
  onError: function(err) {
    console.log(err);
    this.props.storage.clean();

    switch(err.errorCode) {
      case ErrorCodes.ERR_01:
        this.setState({
          componentState: ComponentState.ERROR_STATE,
        });
        this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
      break;

      case ErrorCodes.ERR_03:
        this.setState({
          componentState: ComponentState.ERROR_STATE,
        });
        this.refs.popUpAlert.show(ErrorCodes.ERR_03_MSG);
      break;

      case ErrorCodes.ERR_05:
        this.setState({
          componentState: ComponentState.ERROR_STATE,
        });
        this.refs.popUpAlert.show(err.message);
      break;

      default:
        console.log('Error no categorizado...');
      break;
    }

    scrollMainTo();
  },
  onClickIrEditar: function(llamadoAccion, index, evt) {
    evt.preventDefault();
    var llamadoAcciones = this.state.llamadoAcciones;

    llamadoAccion.descripcionOrg = llamadoAccion.descripcion;
    llamadoAccion.isState = this.props.isEditar;
    llamadoAcciones[index] = llamadoAccion;

    this.setState({
      llamadoAcciones: llamadoAcciones
    });
  },
  onClickSiEditar: function(llamadoAccion, index, evt) {
    evt.preventDefault();

    var result = this.validaLlamadoAccion(llamadoAccion);

    if(!result.isError) {
      this.props.storage.put('LLAMADO_ACCION_TO_UPDATE', Clone.clone(llamadoAccion));
      this.refs.popUpAsk.show(Label.ASK_MODIF_LLAMADO_ACCION + '  [DESCRIPCION = ' + llamadoAccion.descripcionOrg + ', NUEVO = ' + llamadoAccion.descripcion + ']' + ' ?');

    } else {
      this.refs.popUpAlert.show(result.errMsg);
    }
  },
  validaLlamadoAccion: function(llamadoAccion) {
    var result = {
      isError: false,
      errMsg: '',
      ejemploFormato: ''
    };

    if(validaService.isEmpty(llamadoAccion.descripcion.trim())) {
      result = {
        isError: true,
        errMsg: ErrorMsg.ERROR_LLAMADO_ACCION_FORMATO_INCORRECTO,
        ejemploFormato: 'Descripcion Llamado Accion'
      };
    }

    return result;
  },
  onClickNoEditar: function(llamadoAccion, index, evt) {
    evt.preventDefault();
    var llamadoAcciones = this.state.llamadoAcciones;

    this.props.storage.clean();
    llamadoAccion.descripcion = llamadoAccion.descripcionOrg;
    llamadoAccion.descripcionOrg = undefined;
    llamadoAccion.isState = this.props.isNormal;
    llamadoAcciones[index] = llamadoAccion;

    this.setState({
      llamadoAcciones: llamadoAcciones
    });
  },
  onChangeLlamadoAccionValue: function(llamadoAccion, index, evt) {
    var llamadoAcciones = this.state.llamadoAcciones;
    llamadoAccion.descripcion = evt.target.value;
    llamadoAcciones[index] = llamadoAccion;

    this.setState({
      llamadoAcciones: llamadoAcciones
    });
  },
  onClickAceptarEditar: function(evt) {
    var self = this;
    this.refs.popUpAsk.hide();
    var llamadoAccionToUpdate = this.props.storage.get('LLAMADO_ACCION_TO_UPDATE');

    var onSuccess = function(res) {
      var llamadoAcciones = self.state.llamadoAcciones;
      var indexTmp = self.getIndexLlamadoAccion(llamadoAccionToUpdate.idLlamadoAccion);
      var llamadoAccionUpdated = self.state.llamadoAcciones[indexTmp];

      self.props.storage.clean();
      llamadoAccionUpdated.isState = self.props.isNormal;
      llamadoAccionUpdated.descripcionOrg = undefined;
      llamadoAcciones[indexTmp] = llamadoAccionUpdated;

      self.setState({
        llamadoAcciones: llamadoAcciones
      });

      self.refs.popUpAlert.show(Label.REGISTRO_MODIF_SUCCESS);
    };

    var params = {
      idLlamadoAccion: llamadoAccionToUpdate.idLlamadoAccion,
      descripcion: llamadoAccionToUpdate.descripcion
    };
    llamadoAccionService.updateLlamadoAccion(params, onSuccess, this.onError, this.onFail);
  },
  onClickCancelarEditar: function(evt) {
    this.refs.popUpAsk.hide();
    this.refs.popUpAskActivo.hide();
    this.props.storage.clean();
  },
  getIndexLlamadoAccion: function(idLlamadoAccion) {
    var indexTmp = undefined;

    if(this.state.llamadoAcciones != undefined) {
      this.state.llamadoAcciones.every(function(llamadoAccion, index) {
        if(llamadoAccion.idLlamadoAccion == idLlamadoAccion) {
          indexTmp = index;
          return false;
        }

        return true;
      });
    }

    return indexTmp;
  },
  onChangeActivo: function(llamadoAccion, index, evt) {
    evt.preventDefault();
    this.props.storage.clean();

    this.props.storage.put('LLAMADO_ACCION_TO_UPDATE', Clone.clone(llamadoAccion));
    if(llamadoAccion.activo) {
      //preguntar por desactivar
      this.refs.popUpAskActivo.show(Label.ASK_DESACTIVAR_LLAMADO_ACCION + llamadoAccion.descripcion + ' ?');

    } else {
      //preguntar por activas
      this.refs.popUpAskActivo.show(Label.ASK_ACTIVAR_LLAMADO_ACCION + llamadoAccion.descripcion + ' ?');
    }
  },
  onClickAceptarActivo: function(evt) {
    this.refs.popUpAskActivo.hide();
    var self = this;
    var llamadoAccionToUpdate = this.props.storage.get('LLAMADO_ACCION_TO_UPDATE');
    var irActivo = false;

    var onSuccess = function(res) {
      var llamadoAcciones = self.state.llamadoAcciones;
      var indexTmp = self.getIndexLlamadoAccion(llamadoAccionToUpdate.idLlamadoAccion);
      var llamadoAccionUpdated = self.state.llamadoAcciones[indexTmp];

      self.props.storage.clean();
      llamadoAccionUpdated.isState = self.props.isNormal;
      llamadoAccionUpdated.activo = irActivo;
      llamadoAcciones[indexTmp] = llamadoAccionUpdated;

      self.setState({
        llamadoAcciones: llamadoAcciones
      });

      self.refs.popUpAlert.show(Label.REGISTRO_MODIF_SUCCESS);
    };

    if(llamadoAccionToUpdate.activo) {
      irActivo = false;

    } else {
      irActivo = true;
    }

    var params = {
      idLlamadoAccion: llamadoAccionToUpdate.idLlamadoAccion,
      activo: irActivo
    };
    llamadoAccionService.changeEstatusLlamadoAccion(params, onSuccess, this.onError, this.onFail);
  },
  onChangeFiltroLlamadoAccion: function(evt) {
    var self = this;
    this.props.storage.clean();

    var onSuccess = function(res) {
      //estableser todos los parametros a un estado normal
      res.llamadoAcciones.forEach(function(llamadoAccion) {
        llamadoAccion.isState = self.props.isNormal; 
      });

      self.setState({
        llamadoAcciones: res.llamadoAcciones
      });
    };

    this.setState({
      filtroLlamadoAccion: evt.target.value
    });

    var params = {
      descripcion: evt.target.value
    };
    llamadoAccionService.getLlamadoAccionesByFiltro(params, onSuccess, this.onError, this.onFail);    
  },
  onClickNewLlamadoAccion: function(evt) {
    evt.preventDefault();
    this.refs.popUpAskNewLlamadoAccion.show();
  },
  onChangeNewLlamadoAccion: function(evt) {
    this.setState({
      newLlamadoAccion: evt.target.value
    });
  },
  onClickCancelarNewLlamadoAccion: function(evt) {
    this.refs.popUpAskNewLlamadoAccion.hide();
    this.setState({
      newLlamadoAccion: ''
    });
  },
  onClickAceptarNewLlamadoAccion: function(evt) {
    var self = this;

    var onSuccess = function(res) {
      self.refs.popUpAskNewLlamadoAccion.hide();
      self.recargaLlamadoAccion();
      self.refs.popUpAlert.show(Label.REGISTRO_CREATE_SUCCESS);
    };

    if(!validaService.isEmpty(this.state.newLlamadoAccion)) {
      var params = {
        descripcion: this.state.newLlamadoAccion
      };
      llamadoAccionService.createLlamadoAccion(params, onSuccess, this.onError, this.onFail);
      
    } else {
      this.refs.popUpAlert.show(ErrorMsg.ERROR_LLAMADO_ACCION_FORMATO_INCORRECTO);
    }
  },
  onClickEliminarLlamadoAccion: function(llamadoAccion, index, evt) {
    evt.preventDefault();
    this.props.storage.put('LLAMADO_ACCION_TO_DELETE', Clone.clone(llamadoAccion));
    this.refs.popUpAskEliminarLlamadoAccion.show(Label.ASK_ELIMINAR_LLAMADO_ACCION + ' ' + llamadoAccion.descripcion + ' ?');
  },
  onAceptaEliminarLlamadoAccion: function(evt) {
    var self = this;
    var llamadoAccion = this.props.storage.get('LLAMADO_ACCION_TO_DELETE');

    var onSuccess = function(res) {
      //estableser todos los parametros a un estado normal
      self.props.storage.clean();
      self.refs.popUpAskEliminarLlamadoAccion.hide();
      self.recargaLlamadoAccion();
      self.refs.popUpAlert.show(Label.REGISTRO_ELIMINADO_SUCCESS);
    };

    var params = {
      idLlamadoAccion: llamadoAccion.idLlamadoAccion
    };
    llamadoAccionService.eliminarLlamadoAccion(params, onSuccess, this.onError, this.onFail);
  },
  onCancelarEliminarLlamadoAccion: function(evt) {
    this.props.storage.clean();
    this.refs.popUpAskEliminarLlamadoAccion.hide();
  },
  recargaLlamadoAccion: function() {
    var self = this;

    var onSuccess = function(res) {
      //estableser todos los parametros a un estado normal
      res.llamadoAcciones.forEach(function(llamadoAccion) {
        llamadoAccion.isState = self.props.isNormal; 
      });

      self.setState({
        llamadoAcciones: res.llamadoAcciones
      });
    };

    var params = {};
    llamadoAccionService.getLlamadoAcciones(params, onSuccess, this.onError, this.onFail);
  },
  render: function() {
    var self = this;
    var llamadoAccionesRows = [];
    var newLlamadoAccionHeaderHTML = [];
    var newLlamadoAccionBodyHTML = [];
    var eliminarLlamadoAccionHeaderHTML = [];
    var eliminarLlamadoAccionBodyHTML = [];

    if(!this.state.shouldMount) {
      return (React.createElement("div", null));
    }

    switch(this.state.componentState) {
      case ComponentState.NORMAL_STATE:
      break;

      case ComponentState.EXITO_STATE:
      break;

      case ComponentState.ERROR_STATE:
      break;

      case ComponentState.INFO_STATE:
      break;

      case ComponentState.ALERT_STATE:
      break;
    }
    //crear lista de etiquetas tr para la tabla de general acciones
    if(this.state.llamadoAcciones != undefined) {
      this.state.llamadoAcciones.forEach(function(llamadoAccion, index) {
        var accionesHTML = [];
        var valorHTML = [];
        var onClickIrEditar = function(llamadoAccion, index, evt) {
          self.onClickIrEditar(llamadoAccion, index, evt);
        }.bind(self, llamadoAccion, index);

        var onChangeLlamadoAccionValue = function(llamadoAccion, index, evt) {
          self.onChangeLlamadoAccionValue(llamadoAccion, index, evt);
        }.bind(self, llamadoAccion, index);

        var onClickSiEditar = function(llamadoAccion, index, evt) {
          self.onClickSiEditar(llamadoAccion, index, evt);
        }.bind(self, llamadoAccion, index);

        var onClickNoEditar = function(llamadoAccion, index, evt) {
          self.onClickNoEditar(llamadoAccion, index, evt);
        }.bind(self, llamadoAccion, index);

        var onChangeActivo = function(llamadoAccion, index, evt) {
          self.onChangeActivo(llamadoAccion, index, evt);
        }.bind(self, llamadoAccion, index);

        var onClickEliminarLlamadoAccion = function(llamadoAccion, index, evt) {
          self.onClickEliminarLlamadoAccion(llamadoAccion, index, evt);
        }.bind(self, llamadoAccion, index);

        if(llamadoAccion.isState == self.props.isNormal) {
          valorHTML.push(llamadoAccion.descripcion);
          accionesHTML.push(React.createElement("a", {className: "IconEditStyle", href: "#", onClick: onClickIrEditar}));
          accionesHTML.push(React.createElement("a", {className: "IconTrashStyle", href: "#", onClick: onClickEliminarLlamadoAccion}));
          accionesHTML.push(
            React.createElement("label", {className: "switch switch-green"}, 
              React.createElement("input", {type: "checkbox", className: "switch-input", checked: llamadoAccion.activo, onChange: onChangeActivo}), 
              React.createElement("span", {className: "switch-label", "data-on": "On", "data-off": "Off"}), 
              React.createElement("span", {className: "switch-handle"})
            )
          );

        } else if(llamadoAccion.isState == self.props.isEditar) {
          valorHTML.push(
            React.createElement("input", {type: "text", style: {width:'50%'}, placeholder: "Descripcion de Llamado Acción", 
              maxLength: "100", value: llamadoAccion.descripcion, onChange: onChangeLlamadoAccionValue})
          );
          accionesHTML.push(React.createElement("a", {className: "IconOKStyle", href: "#", onClick: onClickSiEditar}));
          accionesHTML.push(React.createElement("a", {className: "IconCancelStyle", href: "#", onClick: onClickNoEditar}));
        }

        llamadoAccionesRows.push(
          React.createElement("tr", null, 
            React.createElement("td", {style: {paddingTop: '5px', paddingBottom: '5px'}}, 
              valorHTML
            ), 
            React.createElement("td", {style: {paddingTop: '5px', paddingBottom: '5px'}}, 
              accionesHTML
            )
          ) 
        );
      });
    }
    //crear contendo html para popup nuevo general accion
    newLlamadoAccionHeaderHTML.push(
      React.createElement("p", null, "Crear nuevo llamado de acción")
    );
    newLlamadoAccionBodyHTML.push(
      React.createElement("div", null, 
        React.createElement("label", {htmlFor: "NombreLlamadoAccion"}, "Llamado acción: "), 
        React.createElement("input", {name: "NombreLlamadoAccion", type: "text", style: {width:'100%'}, placeholder: "Descripcion Llamado Acción", 
          maxLength: "100", value: this.state.newLlamadoAccion, onChange: this.onChangeNewLlamadoAccion})
      )
    );
    //crear contenido html para la eliminacion de general accion
    eliminarLlamadoAccionHeaderHTML.push(
      React.createElement("p", null, "Eliminación de llamado acción")
    );
   
    return (
      React.createElement("div", {id: "container-gral"}, 
        React.createElement(PopUpReact, {ref: "popUpAlert", type: "alert", popUpStyle: "PopUpReactAlert"}), 
        React.createElement(PopUpReact, {ref: "popUpAsk", type: "ask", 
          buttons: [
            {name: 'Aceptar', label: 'Aceptar', onClick: this.onClickAceptarEditar},
            {name: 'Cancelar', label: 'Cancelar', onClick: this.onClickCancelarEditar}
        ]}), 
        React.createElement(PopUpReact, {ref: "popUpAskActivo", type: "ask", 
          buttons: [
            {name: 'Aceptar', label: 'Aceptar', onClick: this.onClickAceptarActivo},
            {name: 'Cancelar', label: 'Cancelar', onClick: this.onClickCancelarEditar}
        ]}), 
        React.createElement(PopUpReact, {ref: "popUpAskNewLlamadoAccion", type: "ask", htmlHeaderContent: newLlamadoAccionHeaderHTML, 
          htmlBodyContent: newLlamadoAccionBodyHTML, 
          buttons: [
            {name: 'Aceptar', label: 'Aceptar', onClick: this.onClickAceptarNewLlamadoAccion},
            {name: 'Cancelar', label: 'Cancelar', onClick: this.onClickCancelarNewLlamadoAccion}
        ]}), 
        React.createElement(PopUpReact, {ref: "popUpAskEliminarLlamadoAccion", type: "ask", htmlHeaderContent: eliminarLlamadoAccionHeaderHTML, 
          buttons: [
            {name: 'Aceptar', label: 'Aceptar', onClick: this.onAceptaEliminarLlamadoAccion},
            {name: 'Cancelar', label: 'Cancelar', onClick: this.onCancelarEliminarLlamadoAccion}
        ]}), 

        React.createElement("article", {className: "module width_full"}, 
          React.createElement("input", {type: "text", style: {width:'100%'}, placeholder: "Buscar...", 
            maxLength: "100", value: this.state.filtroLlamadoAccion, onChange: this.onChangeFiltroLlamadoAccion})
        ), 
        
        React.createElement("article", {className: "module width_full"}, 
          React.createElement("header", null, 
            React.createElement("div", {className: "AlignContainer"}, 
              React.createElement("div", {className: "AlignLeft", style: {width: '95%'}}, 
                React.createElement("h3", {className: "tabs_involved"}, "Lista de llamado de acciones")
              ), 
              React.createElement("div", {className: "AlignRight", style: {width: '5%', marginTop: '5px', textAlign: 'center'}}, 
                React.createElement("a", {className: "IconNewStyle", href: "#", onClick: this.onClickNewLlamadoAccion})
              )
            )
          ), 
          React.createElement("div", {className: "tab_container"}, 
            React.createElement("div", {id: "tab1", className: "tab_content"}, 
              React.createElement("table", {className: "tablesorter", cellSpacing: "0"}, 
                React.createElement("thead", null, 
                  React.createElement("tr", null, 
                    React.createElement("th", {style: {width: '40%'}}, "LLAMADO ACCION"), 
                    React.createElement("th", {style: {width: '10%'}})
                  )
                ), 
                React.createElement("tbody", null, 
                  llamadoAccionesRows
                )
              )
            )
          )
        ), 
        React.createElement("div", {className: "spacer"})
      )
    );
  }
});

module.exports = AdminLlamadoAccion;