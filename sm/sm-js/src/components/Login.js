"use strict";

var Router = require('../libs/react-router/index.js');
var ErrorCodes = require('../utils/ErrorCodes.js');
var Usuario = require('../modelo/Usuario.js');
var loginService = require('../modulos/LoginService.js');

var Login = React.createClass({displayName: "Login",
	mixins: [ Router.Navigation ],
	getInitialState: function() {
    return {
      nombre: '',
      password: '',
      isErrorState: false,
      errorMsg: undefined
    };
  },
  onFail: function(err) {
    console.log(err);
    this.setState({
      isErrorState: true,
      errorMsg: ErrorCodes.ERR_01_MSG
    });
  },
  onError: function(err) {
    console.log(err);

    switch(err.errorCode) {
      case ErrorCodes.ERR_02:
      this.setState({
        isErrorState: true,
        errorMsg: ErrorCodes.ERR_02_MSG
      });
      break;

      case ErrorCodes.ERR_01:
      this.setState({
        isErrorState: true,
        errorMsg: ErrorCodes.ERR_01_MSG
      });
      break;

      default:
        console.log('Error no categorizado...');
      break;
    }
  },
  clearReact: function() {
    this.setState({
      nombre: '',
      password: '',
      isErrorState: false,
      errorMsg: undefined
    });
  },
  onChangeNombre: function(evt) {
    this.setState({
      nombre: evt.target.value
    });
  },
  onChangePassword: function(evt) {
    this.setState({
      password: evt.target.value
    });
  },
  onClickLogin: function() {
    var self = this;
    var error = undefined;

    var onSuccess = function(res) {
      var usuario = new Usuario();
      usuario.setIdUsuario(res.usuario.idUsuario);
      usuario.setNombre(res.usuario.nombre);
      usuario.setToken(res.usuario.token);
      ctx.put('usuario', usuario);      
      self.transitionTo('Home');
    };

    error = this.validaLogin();

    if(!error.isError) {
      var params = {
        nombre: this.state.nombre,
        password: this.state.password
      };

      loginService.login(params, onSuccess, this.onError, this.onFail);

    } else {
      this.setState({
        isErrorState: true,
        errorMsg: error.message
      });
    }
  },
  validaLogin: function() {
    var error = {
      isError: false,
      message: ''
    };

    if(this.state.nombre.trim() == '') {
      error = {
        isError: true,
        message: 'El nombre de usuario y contrase\u00f1a es requerido.'
      };      
    }

    if(this.state.password.trim() == '') {
      error = {
        isError: true,
        message: 'El nombre de usuario y contrase\u00f1a es requerido.'
      };      
    }

    return error;
  },
 	render: function() {
    var errorHTML = '';
    var popUpShow = "componentHide";
    var popUpLoadingShow = "componentHide";

    if(this.state.isErrorState) {
      errorHTML = (React.createElement("h4", {className: "alert_error"}, this.state.errorMsg));
    }

	  return (
      React.createElement("section", {id: "bglogin"}, 
        React.createElement("div", {id: "backgroundEspecial", className: popUpShow}, 
          React.createElement("span", {id: "loading"})
        ), 
        React.createElement("div", {className: "container", id: "login"}, 
          React.createElement("div", {className: "row"}, 
            React.createElement("div", {className: "col-lg-12 text-center"}, 
              React.createElement("img", {src: "images/logoMyT.svg", alt: "Museo Memoria y Tolerancia", className: "logo"})
            )
          ), 
          React.createElement("div", {className: "row"}, 
            React.createElement("div", {className: "col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4"}, 
              React.createElement("div", {className: "panel panel-default", id: "main"}, 
                React.createElement("div", {className: "panel-heading text-center"}, 
                  React.createElement("h4", null, "Login")
                ), 
                React.createElement("div", {className: "panel-body"}, 
                  React.createElement("form", {className: "form-horizontal", role: "form"}, 
                    React.createElement("div", {className: "form-group"}, 
                      React.createElement("label", {htmlFor: "inputEmail3", className: "col-sm-3 control-label"}, "Nombre"), 
                      React.createElement("div", {className: "col-sm-9"}, 
                        React.createElement("input", {type: "text", className: "form-control", id: "inputEmail3", placeholder: "Nombre de usuario", required: true, 
                          maxLength: "100", 
                          value: this.state.nombre, onChange: this.onChangeNombre})
                      )
                    ), 
                    React.createElement("div", {className: "form-group"}, 
                      React.createElement("label", {htmlFor: "inputPassword3", className: "col-sm-3 control-label"}, 
                        "Contraseña"
                      ), 
                      React.createElement("div", {className: "col-sm-9"}, 
                        React.createElement("input", {type: "password", className: "form-control", id: "inputPassword3", placeholder: "Contraseña", required: true, 
                          maxLength: "100", 
                          value: this.state.password, onChange: this.onChangePassword})
                      )
                    ), 
                    React.createElement("div", {className: "form-group last"}, 
                      React.createElement("div", {className: "col-sm-offset-3 col-sm-9"}, 
                        React.createElement("button", {type: "button", className: "btn btn-success btn-md btn-block", onClick: this.onClickLogin}, "Entrar")
                      )
                    )
                  ), 
                  errorHTML
                )
              )
            )
          )
        )
      )
	  );
  }
});

module.exports = Login;