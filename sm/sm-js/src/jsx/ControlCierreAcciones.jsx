"use strict";

var Router = require('../libs/react-router/index.js');
var Label = require('../utils/Label.js');
var adminService = require('../modulos/MyTAdminService.js');
var ErrorCodes = require('../utils/ErrorCodes.js');
var ComponentState = require('../utils/ComponentState.js');
var Constants = require('../utils/Constants.js');
var PopUpReact = require('../components/commons/PopUpReact.js');

var ControlCierreAcciones = React.createClass({
  mixins: [ Router.Navigation ],
 	getInitialState: function() {
    return {
      usuario: ctx.get('usuario'),
      shouldMount: true,
      componentState: ComponentState.NORMAL_STATE,
      pagina: 0,
      resultPerPagina: 10,
      estatusCompromisos: undefined,
      idEstatusCompromiso: 0,
      emailFace: '',
      nombreVisitante: '',
      accionesCierreOrg: undefined,
      accionesCierre: undefined
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
    var self = this;

    var onSuccess = function(res) {
      self.setState({
        estatusCompromisos: res.estatusCompromisos
      });
    };

    if(validaSession()) {
      var params = {};
      adminService.getEstatusCompromisos(params, onSuccess, this.onError, this.onFail);

    } else {
      this.setState({
        shouldMount: false
      });
      this.transitionTo('Login');
    }
  },
  componentWillReceiveProps: function(nextProps) {
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  onFail: function(err) {
    console.log(err);
    this.setState({
      componentState: ComponentState.ERROR_STATE
    });
    this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
  },
  onError: function(err) {
    console.log(err);

    switch(err.errorCode) {
      case ErrorCodes.ERR_01:
        this.setState({
          componentState: ComponentState.ERROR_STATE
        });
        this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
      break;

      case ErrorCodes.ERR_03:
        this.setState({
          componentState: ComponentState.ERROR_STATE
        });
        this.refs.popUpAlert.show(ErrorCodes.ERR_03_MSG);
      break;

      default:
        console.log('Error no categorizado...');
      break;
    }

    scrollMainTo();
  },
  onClickLimpiar: function() {
    this.setState({
      componentState: ComponentState.NORMAL_STATE,
      idEstatusCompromiso: 0,
      emailFace: '',
      nombreVisitante: '',
      resultPerPagina: 10,
      accionesCierreOrg: undefined,
      accionesCierre: undefined
    });
  },
  getPaginado: function(accionesCierre, resultPerPagina) {
    var paginado = undefined;
    var pagina = undefined;
    var i = 0;
    var self = this;

    if(accionesCierre != undefined && accionesCierre.length > 0) {
      paginado = [];
      pagina = [];

      accionesCierre.forEach(function(accionCierre, index) {
        pagina.push(accionCierre);
        
        if(i < resultPerPagina) {
          i++
        } 

        if(i == resultPerPagina || index == (accionesCierre.length - 1)) {
          paginado.push(pagina);
          pagina = [];
          i = 0;
        }
      });
    }

    return paginado;
  },
  onClickBuscar: function() {
    var self = this;

    var onSuccess = function(res) {
      var componentState = ComponentState.NORMAL_STATE;

      if(res.accionesCierre == undefined || res.accionesCierre.length == 0) {
        componentState = ComponentState.INFO_STATE;
        self.refs.popUpAlert.show(Label.NO_RESULTADOS);
      }

      self.setState({
        componentState: componentState,
        pagina: 0,
        accionesCierreOrg: res.accionesCierre,
        accionesCierre: self.getPaginado(res.accionesCierre, self.state.resultPerPagina)
      });
    };

    var params = {
      emailFace: this.state.emailFace,
      nombreVisitante: this.state.nombreVisitante,
      estatusCompromiso: this.state.idEstatusCompromiso
    };
    adminService.getAccionesCierre(params, onSuccess, this.onError, this.onFail);
  },
  onChangeEmailFace: function(evt) {
    this.setState({
      emailFace: evt.target.value
    });
  },
  onChangeNombreVisitante: function(evt) {
    this.setState({
      nombreVisitante: evt.target.value
    });
  },
  onChangeEstatusCompromiso: function(evt) {
    this.setState({
      idEstatusCompromiso: evt.target.value
    });
  },
  onChangeCierreCompromiso: function(accionCierre, index, evt) {
    var idEstatusCompromiso = evt.target.value;
    var self = this;
    var accionesCierre = this.state.accionesCierre;

    var onSuccess = function(res) {
      var componentState = undefined;
      accionCierre.idEstatusCompromiso = parseInt(idEstatusCompromiso);
      accionesCierre[self.state.pagina][index] = accionCierre;
      componentState = ComponentState.EXITO_STATE;

      self.setState({
        componentState: componentState,
        accionesCierre: accionesCierre
      });
      self.refs.popUpAlert.show(Label.REGISTRO_MODIF_SUCCESS);
    };

    var params = {
      idUsuario: this.state.usuario.getIdUsuario(),
      idCompromiso: accionCierre.idCompromiso,
      idEstatusCompromiso: evt.target.value
    };
    adminService.updateEstatusCompromiso(params, onSuccess, this.onError, this.onFail);
  },
  onClickNextPage: function(evt) {
    evt.preventDefault();
    var nextPage = 0;

    if(this.state.accionesCierre != undefined) {
      nextPage = (this.state.pagina + 1);

      if(nextPage <= (this.state.accionesCierre.length - 1) ) {
        this.setState({
          pagina: nextPage
        });  
      }
    }
  },
  onClickBackPage: function(evt) {
    evt.preventDefault();
    var backPage = 0;
    var firstPage = 0;

    if(this.state.accionesCierre != undefined) {
      backPage = (this.state.pagina - 1);

      if(backPage >= firstPage) {
        this.setState({
          pagina: backPage
        });  
      }
    }
  },
  onClickFirstPage: function(evt) {
    evt.preventDefault();
    var firstPage = 0;

    if(this.state.accionesCierre != undefined) {
      if(firstPage != this.state.pagina) {
        this.setState({
          pagina: firstPage
        });  
      }
    }
  },
  onClickLastPage: function(evt) {
    evt.preventDefault();
    var lastPage = 0;

    if(this.state.accionesCierre != undefined) {
      lastPage = (this.state.accionesCierre.length - 1);

      if(lastPage != this.state.pagina) {
        this.setState({
          pagina: lastPage
        });  
      }
    }
  },
  onChangeResultPerPagina: function(evt) {
    if(this.state.accionesCierreOrg != undefined) {
      this.setState({
        pagina: 0,
        resultPerPagina: evt.target.value,
        accionesCierre: this.getPaginado(this.state.accionesCierreOrg, evt.target.value)
      });
    }
  },
  render: function() {
    var self = this;
    var estatusCompromisos = [];
    var acciones = [];
    var totalPaginado = '0 de 0 Páginas';

    if(!this.state.shouldMount) {
      return (<div></div>);
    }

    switch(this.state.componentState) {
      case ComponentState.NORMAL_STATE:
      break;

      case ComponentState.EXITO_STATE:
      break;

      case ComponentState.ERROR_STATE:
      break;

      case ComponentState.INFO_STATE:
      break;

      case ComponentState.ALERT_STATE:
      break;
    }
    //construr estatus de los compromisos
    if(this.state.estatusCompromisos != undefined) {
      this.state.estatusCompromisos.forEach(function(estatusCompromiso) {
        estatusCompromisos.push(<option value={estatusCompromiso.idEstatusCompromiso}>{estatusCompromiso.descripcion}</option>);
      });
    }
    //construir las acciones
    if(this.state.accionesCierre != undefined) {
      totalPaginado = ((this.state.pagina + 1) + ' de ' +  this.state.accionesCierre.length + ' Páginas')
      this.state.accionesCierre[this.state.pagina].forEach(function(accionCierre, index) {
        var EN_TIEMPO = 'SI'
        var FUERA_TIEMPO = 'NO';
        var enTiempo = '';

        var onChangeCierreCompromiso = function(accionCierre, index, evt) {
          self.onChangeCierreCompromiso(accionCierre, index, evt);
        }.bind(self, accionCierre, index);

        if(accionCierre.enTiempo) {
          enTiempo = EN_TIEMPO;

        } else {
          enTiempo = FUERA_TIEMPO;
        }

        acciones.push(
          <tr> 
            <td>{accionCierre.nombreVisitante}</td> 
            <td>{accionCierre.email}</td> 
            <td>{accionCierre.accion}</td> 
            <td>
              <select value={accionCierre.idEstatusCompromiso} defaultValue={accionCierre.idEstatusCompromiso} onChange={onChangeCierreCompromiso}>
                {estatusCompromisos}
              </select>
            </td>
            <td>{enTiempo}</td> 
          </tr> 
        );
      });
    }

    return (
      <div id="container-gral">
        <PopUpReact ref='popUpAlert' type='alert' popUpStyle='PopUpReactAlert'/>

        <article className="module width_full">
          <header><h3>Filtros de Búsqueda</h3></header>
            <div className="module_content">
              <fieldset style={{width:'48%', float:'left', marginRight: '3%'}}>
                <label>Facebook o E-mail</label>
                <input type="text" style={{width:'92%'}} placeholder="Ingresa correo de Faceboook o E-mail" 
                  maxLength="100"
                  value={this.state.emailFace} onChange={this.onChangeEmailFace}/>
              </fieldset>
              <fieldset style={{width:'48%', float:'left'}}>
                <label>Estatus de la Acción</label>
                <select style={{width:'92%'}} value={this.state.idEstatusCompromiso} onChange={this.onChangeEstatusCompromiso}>
                  <option value="0">Todos</option>
                  {estatusCompromisos}
                </select>
              </fieldset>
              <fieldset style={{width:'99%', float:'left', marginRight: '3%'}}>
                <label>Nombre</label>
                <input type="text" style={{width:'96%'}} placeholder="Ingresa el nombre del visitante" 
                  maxLength="100"
                  value={this.state.nombreVisitante} onChange={this.onChangeNombreVisitante}/>
              </fieldset>
              <div className="clear"></div>
            </div>
          <footer>
            <div className="submit_link">
              <input type="button" value="BUSCAR" className="alt_btn" onClick={this.onClickBuscar}/>
              <input type="button" value="LIMPIAR" onClick={this.onClickLimpiar}/>
            </div>
          </footer>
        </article>
        
        <article className="module width_full">
          <header>
            <h3 className="tabs_involved">Acciones encontradas</h3>
          </header>
          <div className="tab_container">
            <div id="tab1" className="tab_content">
              <table className="tablesorter" cellSpacing="0"> 
                <thead> 
                  <tr>
                    <th>Nombre visitante</th> 
                    <th>Email</th> 
                    <th>Acción</th> 
                    <th>Estatus</th> 
                    <th>En tiempo</th>
                  </tr> 
                </thead> 
                <tbody> 
                  {acciones}
                </tbody>
                <tfoot>
                  <tr>
                    <td colSpan="100%">
                      <div id="pag">
                        <select id="soflow-color" value={this.state.resultPerPagina} onChange={this.onChangeResultPerPagina}>
                          <option value="5">5</option>
                          <option value="10">10</option>
                          <option value="15">15</option>
                          <option value="20">20</option>
                          <option value="25">25</option>
                        </select>
                        <ul id="pagination">
                          <li className="previous"><a href="#" onClick={this.onClickFirstPage}>&lt;&lt; Primero</a></li>
                          <li className="previous"><a href="#" onClick={this.onClickBackPage}>&lt; Anterior</a></li>
                          <li className="active">{totalPaginado}</li>
                          <li className="next"><a href="#" onClick={this.onClickNextPage}>Siguiente &gt;</a></li>
                          <li className="next"><a href="#" onClick={this.onClickLastPage}>Último &gt;&gt;</a></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                </tfoot> 
              </table>
            </div>
          </div>
        </article>
        <div className="spacer"></div>
      </div>
    );
  }
});

module.exports = ControlCierreAcciones;