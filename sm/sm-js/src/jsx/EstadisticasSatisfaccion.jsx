'use strict';

var Router = require('../libs/react-router/index.js');
var adminService = require('../modulos/MyTAdminService.js');
var ErrorCodes = require('../utils/ErrorCodes.js');
var ComponentState = require('../utils/ComponentState.js');
var Constants = require('../utils/Constants.js');
var PopUpReact = require('../components/commons/PopUpReact.js');

var EstadisticasSatisfaccion = React.createClass({
  mixins: [ Router.Navigation ],
 	getInitialState: function() {
    return {
      shouldMount: true,
      componentState: ComponentState.NORMAL_STATE,
      preguntas: undefined
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
    var self = this;

    var onSuccess = function(res) {
      self.setState({
        preguntas: res.preguntas
      });
    };

    if(validaSession()) {
      var params = {};
      adminService.getEstadisticasSatisfaccion(params, onSuccess, this.onError, this.onFail);

    } else {
      this.setState({
        shouldMount: false
      });
      this.transitionTo('Login');
    }
  },
  componentWillReceiveProps: function(nextProps) {
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  onFail: function(err) {
    console.log(err);
    this.setState({
      componentState: ComponentState.ERROR_STATE
    });
    this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
  },
  onError: function(err) {
    console.log(err);

    switch(err.errorCode) {
      case ErrorCodes.ERR_01:
        this.setState({
          componentState: ComponentState.ERROR_STATE
        });
        this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
      break;

      case ErrorCodes.ERR_03:
        this.setState({
          componentState: ComponentState.ERROR_STATE
        });
        this.refs.popUpAlert.show(ErrorCodes.ERR_03_MSG);
      break;

      default:
        console.log('Error no categorizado...');
      break;
    } 

    scrollMainTo();
  },
  render: function() {
    var preguntasHTML = [];

    if(!this.state.shouldMount) {
      return (<div></div>);
    }

    switch(this.state.componentState) {
      case ComponentState.NORMAL_STATE:
      break;

      case ComponentState.EXITO_STATE:
      break;

      case ComponentState.ERROR_STATE:
      break;

      case ComponentState.INFO_STATE:
      break;

      case ComponentState.ALERT_STATE:
      break;
    }
    //crear secciones de las preguntas
    if(this.state.preguntas != undefined) {
      this.state.preguntas.forEach(function(pregunta) {
        var calificacionesHTML = [];
        var respuestasHTML = [];
        var porcentajesHTML = [];
        var TOTAL_CALIFICACIONES = 10;
        var i = 0;
        var pocentajeTotal = 0.0;

        var getResultado = function(calificacion) {
          var resultado = 0;
          pregunta.respuestas.every(function(resp) {
            if(resp.calificacion == calificacion) {
              resultado = resp.resultado;
              return false;
            }

            return true;
          });

          return resultado;
        };

        var getPocentaje = function(calificacion) {
          var porcentaje = 0;
          pregunta.respuestas.every(function(resp) {
            if(resp.calificacion == calificacion) {
              porcentaje = resp.porcentaje;
              return false;
            }

            return true;
          });

          return porcentaje;
        };

        for(i = 0; i <= TOTAL_CALIFICACIONES; i++) {
          var resultado = getResultado(i);
          var porcentaje = getPocentaje(i);

          calificacionesHTML.push(<th>{i}</th>);
          respuestasHTML.push(<td>{resultado}</td>);
          porcentajesHTML.push(<td>{porcentaje}%</td>);
          pocentajeTotal += porcentaje;
        }

        calificacionesHTML.push(<th>TOTAL</th>);
        respuestasHTML.push(<td>{pregunta.total}</td>);
        porcentajesHTML.push(<td>{Math.round(pocentajeTotal.toFixed(2))}%</td>);

        preguntasHTML.push(
          <div className='tab_container'>
            <div id='tab1' className='tab_content' style={{paddingBottom: '15px'}}>
              <table className='tablesorter' cellSpacing='0'> 
                <thead> 
                  <tr> 
                    <th colSpan='13' className='question'>{pregunta.descripcion}</th> 
                  </tr>
                  <tr>
                    <th>CALIFICACIÓN:</th>
                    {calificacionesHTML}
                  </tr> 
                </thead> 
                <tbody> 
                  <tr> 
                    <td>RESPUESTAS:</td>
                    {respuestasHTML}
                  </tr> 
                  <tr> 
                    <td>PORCENTAJE:</td>
                    {porcentajesHTML}
                  </tr> 
                </tbody> 
              </table>
            </div>
          </div>
        );
      });
    }

    return (
      <div id='container-gral'>
        <PopUpReact ref='popUpAlert' type='alert' popUpStyle='PopUpReactAlert'/>
        <article className='module width_full'>
          <header>
            <h3 className='tabs_involved'>Resultado de la Búsqueda</h3>
          </header>
          {preguntasHTML}
        </article>
        <div className='spacer'></div>
      </div>
    );
  }
});

module.exports = EstadisticasSatisfaccion;