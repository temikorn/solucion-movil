"use strict";

var Router = require('../libs/react-router/index.js');
var RouteHandler = require('../libs/react-router/components/RouteHandler.js');

var MyTApp = React.createClass({
	mixins: [ Router.Navigation ],
	getInitialState: function() {
    return {
    	usuario: ctx.get('usuario')
    };
  },
  componentWillMount: function() {
		if(this.state.usuario == undefined || !this.state.usuario.isValidToken()) {
  		this.transitionTo('Login');
		}
  },
  componentDidMount: function() {
  },
  componentWillReceiveProps: function(nextProps) {
  },
  shouldComponentUpdate: function() {
		return true;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  setUsuario: function(usuario) {
  	this.setState({
  		usuario: usuario
  	});
  },
	render: function() {
	  return (
    	<RouteHandler />
	  );
	}
});

module.exports = MyTApp;