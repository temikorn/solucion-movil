'use strict';

var Router = require('../libs/react-router/index.js');
var Route = require('../libs/react-router/components/Route.js');
var DefaultRoute = require('../libs/react-router/components/DefaultRoute.js');
var Context = require('../utils/Context.js');
var Usuario = require('../modelo/Usuario.js');
var MyTApp = require('../components/MyTApp.js');
var Login = require('../components/Login.js');
var Home = require('../components/Home.js');
var EstadisticasVisitante = require('../components/EstadisticasVisitante.js');
var EstadisticasSatisfaccion = require('../components/EstadisticasSatisfaccion.js');
var ControlCierreAcciones = require('../components/ControlCierreAcciones.js');
var ConfigParametros = require('../components/ConfigParametros.js');
var AdminGeneralAccion = require('../components/AdminGeneralAccion.js');
var AdminAcciones = require('../components/adminAcciones/AdminAcciones.js');
var AdminVisitantes = require('../components/adminVisitantes/AdminVisitantes.js');
var AdminLlamadoAccion = require('../components/AdminLlamadoAccion.js');

window.resetApp = function () {
  ctx.put('usuario', new Usuario());
};

window.validaSession = function() {
  var usuario = ctx.get('usuario');
  var isSessionValida = false;

  if(usuario != undefined && usuario.isValidToken()) {
    isSessionValida = true;

  } else {
    ctx.clean();
  }

  return isSessionValida;
};

window.showLoading = function() {
  $('#backgroundEspecial').removeClass('componentHide');
  $('#backgroundEspecial').addClass('componentShow');
};

window.hideLoading = function() {
  $('#backgroundEspecial').removeClass('componentShow');
  $('#backgroundEspecial').addClass('componentHide');
};

window.scrollMainTo = function() {
  $(('#home')).scrollTop(0);
};

window.getContextPath = function() {
  //var server_back_Prod = 'http://192.168.1.36:8080';
  //var server_back_Dev = 'http://127.0.0.1:8080';
  var base = document.getElementsByTagName('base')[0];
  var contextPath = undefined;
  
  if (base && base.href && (base.href.length > 0)) {
      base = base.href;
      
  } else {
      base = document.URL;
  }

  contextPath = base.substr(0, base.indexOf('/', base.indexOf('//') + 2));
  //contextPath = server_back_Dev;
  return contextPath;
};

window.initApp = function () {
  var x = window.innerWidth;
  var y = window.innerHeight;

  document.querySelector('body').style.width = x + 'px';
  document.querySelector('body').style.height = y + 'px';
  window.addEventListener('resize', function() {
    x = window.innerWidth;
    y = window.innerHeight;

    document.querySelector('body').style.width = x + 'px';
    document.querySelector('body').style.height = y + 'px';
  });

  window.ctx = new Context();
  window.ctx.put('usuario', new Usuario());
  window.ctx.put('CONTEXT_PATH', window.getContextPath());

  var routes = (
    <Route name='MyTApp' handler={MyTApp} path='/'>
      <Route name='Home' handler={Home} path='/home'>
        <Route name='EstadisticasSatisfaccion' handler={EstadisticasSatisfaccion}/>
        <Route name='ControlCierreAcciones' handler={ControlCierreAcciones}/>
        <Route name='ConfigParametros' handler={ConfigParametros}/>
        <Route name='AdminGeneralAccion' handler={AdminGeneralAccion}/>
        <Route name='AdminAcciones' handler={AdminAcciones}/>
        <Route name='AdminVisitantes' handler={AdminVisitantes}/>
        <Route name='AdminLlamadoAccion' handler={AdminLlamadoAccion}/>
        <DefaultRoute name='EstadisticasVisitante' handler={EstadisticasVisitante}/>
      </Route>
      <DefaultRoute name='Login' handler={Login} />
    </Route>
  );

  Router.run(routes, function (Handler) {
    React.render(<Handler />, document.body);
  });
};
