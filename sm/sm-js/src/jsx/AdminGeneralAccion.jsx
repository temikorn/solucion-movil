'use strict';

var Router = require('../libs/react-router/index.js');
/* # seccion para declarar utilerias # */
var Clone = require('../utils/Clone.js');
var Context = require('../utils/Context.js');
var Label = require('../utils/Label.js');
var ErrorCodes = require('../utils/ErrorCodes.js');
var ErrorMsg = require('../utils/ErrorMsg.js');
var ComponentState = require('../utils/ComponentState.js');
var Constants = require('../utils/Constants.js');
/* # seccion para declarar compronentes comunes # */
var PopUpReact = require('../components/commons/PopUpReact.js');
/* # seccion para declarar servicios # */
var generalAccionService = require('../modulos/GeneralAccionService.js');
var validaService = require('../utils/ValidaService.js');

var AdminGeneralAccion = React.createClass({
  mixins: [ Router.Navigation ],
  getInitialState: function() {
    return {
      usuario: ctx.get('usuario'),
      shouldMount: true,
      componentState: ComponentState.NORMAL_STATE,
      generalAcciones: undefined,
      filtroGeneralAccion: ''
    };
  },
  getDefaultProps: function() {
    return {
      isNormal: 1,
      isEditar: 2,
      storage: new Context()
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
    var self = this;

    var onSuccess = function(res) {
      //estableser todos los parametros a un estado normal
      res.generalAcciones.forEach(function(generalAccion) {
        generalAccion.isState = self.props.isNormal; 
      });

      self.setState({
        generalAcciones: res.generalAcciones
      });
    };

    if(validaSession()) {
      var params = {};
      generalAccionService.getGeneralAcciones(params, onSuccess, this.onError, this.onFail);

    } else {
      this.setState({
        shouldMount: false
      });
      this.transitionTo('Login');
    }
  },
  componentWillReceiveProps: function(nextProps) {
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  onFail: function(err) {
    console.log(err);
    this.props.storage.clean();
    this.setState({
      componentState: ComponentState.ERROR_STATE,
    });
    this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
  },
  onError: function(err) {
    console.log(err);
    this.props.storage.clean();

    switch(err.errorCode) {
      case ErrorCodes.ERR_01:
        this.setState({
          componentState: ComponentState.ERROR_STATE,
        });
        this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
      break;

      case ErrorCodes.ERR_03:
        this.setState({
          componentState: ComponentState.ERROR_STATE,
        });
        this.refs.popUpAlert.show(ErrorCodes.ERR_03_MSG);
      break;

      case ErrorCodes.ERR_04:
        this.setState({
          componentState: ComponentState.ERROR_STATE,
        });
        this.refs.popUpAlert.show(err.message);
      break;

      default:
        console.log('Error no categorizado...');
      break;
    }

    scrollMainTo();
  },
  onClickIrEditar: function(generalAccion, index, evt) {
    evt.preventDefault();
    var generalAcciones = this.state.generalAcciones;

    generalAccion.nombreOrg = generalAccion.nombre;
    generalAccion.isState = this.props.isEditar;
    generalAcciones[index] = generalAccion;

    this.setState({
      generalAcciones: generalAcciones
    });
  },
  onClickSiEditar: function(generalAccion, index, evt) {
    evt.preventDefault();

    var result = this.validaGeneralAccion(generalAccion);

    if(!result.isError) {
      this.props.storage.put('GENERAL_ACCION_TO_UPDATE', Clone.clone(generalAccion));
      this.refs.popUpAsk.show(Label.ASK_MODIF_GENERAL_ACCION + '  [NOMBRE = ' + generalAccion.nombreOrg + ', NUEVO = ' + generalAccion.nombre + ']' + ' ?');

    } else {
      this.refs.popUpAlert.show(result.errMsg);
    }
  },
  validaGeneralAccion: function(generalAccion) {
    var result = {
      isError: false,
      errMsg: '',
      ejemploFormato: ''
    };

    if(validaService.isEmpty(generalAccion.nombre.trim())) {
      result = {
        isError: true,
        errMsg: ErrorMsg.ERROR_GENERAL_ACCION_FORMATO_INCORRECTO,
        ejemploFormato: 'Nombre General Accion'
      };
    }

    return result;
  },
  onClickNoEditar: function(generalAccion, index, evt) {
    evt.preventDefault();
    var generalAcciones = this.state.generalAcciones;

    this.props.storage.clean();
    generalAccion.nombre = generalAccion.nombreOrg;
    generalAccion.nombreOrg = undefined;
    generalAccion.isState = this.props.isNormal;
    generalAcciones[index] = generalAccion;

    this.setState({
      generalAcciones: generalAcciones
    });
  },
  onChangeGeneralAccionValue: function(generalAccion, index, evt) {
    var generalAcciones = this.state.generalAcciones;
    generalAccion.nombre = evt.target.value;
    generalAcciones[index] = generalAccion;

    this.setState({
      generalAcciones: generalAcciones
    });
  },
  onClickAceptarEditar: function(evt) {
    var self = this;
    this.refs.popUpAsk.hide();
    var generalAccionToUpdate = this.props.storage.get('GENERAL_ACCION_TO_UPDATE');

    var onSuccess = function(res) {
      var generalAcciones = self.state.generalAcciones;
      var indexTmp = self.getIndexGeneralAccion(generalAccionToUpdate.idGeneralAccion);
      var generalAccionUpdated = self.state.generalAcciones[indexTmp];

      self.props.storage.clean();
      generalAccionUpdated.isState = self.props.isNormal;
      generalAccionUpdated.nombreOrg = undefined;
      generalAcciones[indexTmp] = generalAccionUpdated;

      self.setState({
        generalAcciones: generalAcciones
      });

      self.refs.popUpAlert.show(Label.REGISTRO_MODIF_SUCCESS);
    };

    var params = {
      idGeneralAccion: generalAccionToUpdate.idGeneralAccion,
      nombre: generalAccionToUpdate.nombre
    };
    generalAccionService.updateGeneralAccion(params, onSuccess, this.onError, this.onFail);
  },
  onClickCancelarEditar: function(evt) {
    this.refs.popUpAsk.hide();
    this.refs.popUpAskActivo.hide();
    this.props.storage.clean();
  },
  getIndexGeneralAccion: function(idGeneralAccion) {
    var indexTmp = undefined;

    if(this.state.generalAcciones != undefined) {
      this.state.generalAcciones.every(function(generalAccion, index) {
        if(generalAccion.idGeneralAccion == idGeneralAccion) {
          indexTmp = index;
          return false;
        }

        return true;
      });
    }

    return indexTmp;
  },
  onChangeActivo: function(generalAccion, index, evt) {
    evt.preventDefault();
    this.props.storage.clean();

    this.props.storage.put('GENERAL_ACCION_TO_UPDATE', Clone.clone(generalAccion));
    if(generalAccion.activo) {
      //preguntar por desactivar
      this.refs.popUpAskActivo.show(Label.ASK_DESACTIVAR_GENERAL_ACCION + generalAccion.nombre + ' ?');

    } else {
      //preguntar por activas
      this.refs.popUpAskActivo.show(Label.ASK_ACTIVAR_GENERAL_ACCION + generalAccion.nombre + ' ?');
    }
  },
  onClickAceptarActivo: function(evt) {
    this.refs.popUpAskActivo.hide();
    var self = this;
    var generalAccionToUpdate = this.props.storage.get('GENERAL_ACCION_TO_UPDATE');
    var irActivo = false;

    var onSuccess = function(res) {
      var generalAcciones = self.state.generalAcciones;
      var indexTmp = self.getIndexGeneralAccion(generalAccionToUpdate.idGeneralAccion);
      var generalAccionUpdated = self.state.generalAcciones[indexTmp];

      self.props.storage.clean();
      generalAccionUpdated.isState = self.props.isNormal;
      generalAccionUpdated.activo = irActivo;
      generalAcciones[indexTmp] = generalAccionUpdated;

      self.setState({
        generalAcciones: generalAcciones
      });

      self.refs.popUpAlert.show(Label.REGISTRO_MODIF_SUCCESS);
    };

    if(generalAccionToUpdate.activo) {
      irActivo = false;

    } else {
      irActivo = true;
    }

    var params = {
      idGeneralAccion: generalAccionToUpdate.idGeneralAccion,
      activo: irActivo
    };
    generalAccionService.changeEstatusGeneralAccion(params, onSuccess, this.onError, this.onFail);
  },
  onChangeFiltroGeneralAccion: function(evt) {
    var self = this;
    this.props.storage.clean();

    var onSuccess = function(res) {
      //estableser todos los parametros a un estado normal
      res.generalAcciones.forEach(function(generalAccion) {
        generalAccion.isState = self.props.isNormal; 
      });

      self.setState({
        generalAcciones: res.generalAcciones
      });
    };

    this.setState({
      filtroGeneralAccion: evt.target.value
    });

    var params = {
      nombre: evt.target.value
    };
    generalAccionService.getGeneralAccionesByFiltro(params, onSuccess, this.onError, this.onFail);    
  },
  onClickNewGeneralAccion: function(evt) {
    evt.preventDefault();
    this.refs.popUpAskNewGeneralAccion.show();
  },
  onChangeNewGeneralAccion: function(evt) {
    this.setState({
      newGeneralAccion: evt.target.value
    });
  },
  onClickCancelarNewGeneralAccion: function(evt) {
    this.refs.popUpAskNewGeneralAccion.hide();
    this.setState({
      newGeneralAccion: ''
    });
  },
  onClickAceptarNewGeneralAccion: function(evt) {
    var self = this;

    var onSuccess = function(res) {
      self.refs.popUpAskNewGeneralAccion.hide();
      self.recargaGeneralAccion();
      self.refs.popUpAlert.show(Label.REGISTRO_CREATE_SUCCESS);
    };

    if(!validaService.isEmpty(this.state.newGeneralAccion)) {
      var params = {
        nombre: this.state.newGeneralAccion
      };
      generalAccionService.createGeneralAccion(params, onSuccess, this.onError, this.onFail);
      
    } else {
      this.refs.popUpAlert.show(ErrorMsg.ERROR_GENERAL_ACCION_FORMATO_INCORRECTO);
    }
  },
  onClickEliminarGeneralAccion: function(generalAccion, index, evt) {
    evt.preventDefault();
    this.props.storage.put('GENERAL_ACCION_TO_DELETE', Clone.clone(generalAccion));
    this.refs.popUpAskEliminarGeneralAccion.show(Label.ASK_ELIMINAR_GENERAL_ACCION + ' ' + generalAccion.nombre + ' ?');
  },
  onAceptaEliminarGeneralAccion: function(evt) {
    var self = this;
    var generalAccion = this.props.storage.get('GENERAL_ACCION_TO_DELETE');

    var onSuccess = function(res) {
      //estableser todos los parametros a un estado normal
      self.props.storage.clean();
      self.refs.popUpAskEliminarGeneralAccion.hide();
      self.recargaGeneralAccion();
      self.refs.popUpAlert.show(Label.REGISTRO_ELIMINADO_SUCCESS);
    };

    var params = {
      idGeneralAccion: generalAccion.idGeneralAccion
    };
    generalAccionService.eliminarGeneralAccion(params, onSuccess, this.onError, this.onFail);
  },
  onCancelarEliminarGeneralAccion: function(evt) {
    this.props.storage.clean();
    this.refs.popUpAskEliminarGeneralAccion.hide();
  },
  recargaGeneralAccion: function() {
    var self = this;

    var onSuccess = function(res) {
      //estableser todos los parametros a un estado normal
      res.generalAcciones.forEach(function(generalAccion) {
        generalAccion.isState = self.props.isNormal; 
      });

      self.setState({
        generalAcciones: res.generalAcciones
      });
    };

    var params = {};
    generalAccionService.getGeneralAcciones(params, onSuccess, this.onError, this.onFail);
  },
  render: function() {
    var self = this;
    var generalAccionesRows = [];
    var newGeneralAccionHeaderHTML = [];
    var newGeneralAccionBodyHTML = [];
    var eliminarGeneralAccionHeaderHTML = [];
    var eliminarGeneralAccionBodyHTML = [];

    if(!this.state.shouldMount) {
      return (<div></div>);
    }

    switch(this.state.componentState) {
      case ComponentState.NORMAL_STATE:
      break;

      case ComponentState.EXITO_STATE:
      break;

      case ComponentState.ERROR_STATE:
      break;

      case ComponentState.INFO_STATE:
      break;

      case ComponentState.ALERT_STATE:
      break;
    }
    //crear lista de etiquetas tr para la tabla de general acciones
    if(this.state.generalAcciones != undefined) {
      this.state.generalAcciones.forEach(function(generalAccion, index) {
        var accionesHTML = [];
        var valorHTML = [];
        var onClickIrEditar = function(generalAccion, index, evt) {
          self.onClickIrEditar(generalAccion, index, evt);
        }.bind(self, generalAccion, index);

        var onChangeGeneralAccionValue = function(generalAccion, index, evt) {
          self.onChangeGeneralAccionValue(generalAccion, index, evt);
        }.bind(self, generalAccion, index);

        var onClickSiEditar = function(generalAccion, index, evt) {
          self.onClickSiEditar(generalAccion, index, evt);
        }.bind(self, generalAccion, index);

        var onClickNoEditar = function(generalAccion, index, evt) {
          self.onClickNoEditar(generalAccion, index, evt);
        }.bind(self, generalAccion, index);

        var onChangeActivo = function(generalAccion, index, evt) {
          self.onChangeActivo(generalAccion, index, evt);
        }.bind(self, generalAccion, index);

        var onClickEliminarGeneralAccion = function(generalAccion, index, evt) {
          self.onClickEliminarGeneralAccion(generalAccion, index, evt);
        }.bind(self, generalAccion, index);

        if(generalAccion.isState == self.props.isNormal) {
          valorHTML.push(generalAccion.nombre);
          accionesHTML.push(<a className='IconEditStyle' href='#' onClick={onClickIrEditar}></a>);
          accionesHTML.push(<a className='IconTrashStyle' href='#' onClick={onClickEliminarGeneralAccion}></a>);
          accionesHTML.push(
            <label className='switch switch-green'>
              <input type='checkbox' className='switch-input' checked={generalAccion.activo} onChange={onChangeActivo} />
              <span className='switch-label' data-on='On' data-off='Off'></span>
              <span className='switch-handle'></span>
            </label>
          );

        } else if(generalAccion.isState == self.props.isEditar) {
          valorHTML.push(
            <input type='text' style={{width:'50%'}} placeholder='Nombre de General Acción' 
              maxLength='100' value={generalAccion.nombre} onChange={onChangeGeneralAccionValue}/>
          );
          accionesHTML.push(<a className='IconOKStyle' href='#' onClick={onClickSiEditar}></a>);
          accionesHTML.push(<a className='IconCancelStyle' href='#' onClick={onClickNoEditar}></a>);
        }

        generalAccionesRows.push(
          <tr> 
            <td style={{paddingTop: '5px', paddingBottom: '5px'}}>
              {valorHTML}
            </td> 
            <td style={{paddingTop: '5px', paddingBottom: '5px'}}>
              {accionesHTML}
            </td> 
          </tr> 
        );
      });
    }
    //crear contendo html para popup nuevo general accion
    newGeneralAccionHeaderHTML.push(
      <p>Crear nuevo general de acción</p>
    );
    newGeneralAccionBodyHTML.push(
      <div>
        <label htmlFor='NombreGeneralAccion'>Nombre: </label>
        <input name='NombreGeneralAccion' type='text' style={{width:'100%'}} placeholder='Nombre General Acción' 
          maxLength='100' value={this.state.newGeneralAccion} onChange={this.onChangeNewGeneralAccion}/>
      </div>
    );
    //crear contenido html para la eliminacion de general accion
    eliminarGeneralAccionHeaderHTML.push(
      <p>Eliminación de general acción</p>
    );
   
    return (
      <div id='container-gral'>
        <PopUpReact ref='popUpAlert' type='alert' popUpStyle='PopUpReactAlert'/>
        <PopUpReact ref='popUpAsk' type='ask' 
          buttons={[
            {name: 'Aceptar', label: 'Aceptar', onClick: this.onClickAceptarEditar},
            {name: 'Cancelar', label: 'Cancelar', onClick: this.onClickCancelarEditar}
        ]}/>
        <PopUpReact ref='popUpAskActivo' type='ask' 
          buttons={[
            {name: 'Aceptar', label: 'Aceptar', onClick: this.onClickAceptarActivo},
            {name: 'Cancelar', label: 'Cancelar', onClick: this.onClickCancelarEditar}
        ]}/>
        <PopUpReact ref='popUpAskNewGeneralAccion' type='ask' htmlHeaderContent={newGeneralAccionHeaderHTML}
          htmlBodyContent={newGeneralAccionBodyHTML}
          buttons={[
            {name: 'Aceptar', label: 'Aceptar', onClick: this.onClickAceptarNewGeneralAccion},
            {name: 'Cancelar', label: 'Cancelar', onClick: this.onClickCancelarNewGeneralAccion}
        ]}/>
        <PopUpReact ref='popUpAskEliminarGeneralAccion' type='ask' htmlHeaderContent={eliminarGeneralAccionHeaderHTML}
          buttons={[
            {name: 'Aceptar', label: 'Aceptar', onClick: this.onAceptaEliminarGeneralAccion},
            {name: 'Cancelar', label: 'Cancelar', onClick: this.onCancelarEliminarGeneralAccion}
        ]}/>

        <article className='module width_full'>
          <input type='text' style={{width:'100%'}} placeholder='Buscar...' 
            maxLength='100' value={this.state.filtroGeneralAccion} onChange={this.onChangeFiltroGeneralAccion}/>
        </article>
        
        <article className='module width_full'>
          <header>
            <div className='AlignContainer'>
              <div className='AlignLeft' style={{width: '95%'}}>
                <h3 className='tabs_involved'>Lista de general de acciones</h3>
              </div>
              <div className='AlignRight' style={{width: '5%', marginTop: '5px', textAlign: 'center'}}>
                <a className='IconNewStyle' href='#' onClick={this.onClickNewGeneralAccion}></a>
              </div>
            </div>
          </header>
          <div className='tab_container'>
            <div id='tab1' className='tab_content'>
              <table className='tablesorter' cellSpacing='0'> 
                <thead> 
                  <tr>
                    <th style={{width: '40%'}}>NOMBRE</th> 
                    <th style={{width: '10%'}}></th> 
                  </tr> 
                </thead> 
                <tbody> 
                  {generalAccionesRows}
                </tbody>
              </table>
            </div>
          </div>
        </article>
        <div className='spacer'></div>
      </div>
    );
  }
});

module.exports = AdminGeneralAccion;