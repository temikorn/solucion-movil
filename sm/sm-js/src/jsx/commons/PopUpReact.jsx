'use strict';

var Constants = require('../../utils/Constants.js');

var PopUpReact = React.createClass({
  getInitialState: function() {
    return {
      isShow: false,
      message: '',
      buttons: this.props.buttons,
      htmlHeaderContent: this.props.htmlHeaderContent,
      htmlBodyContent: this.props.htmlBodyContent,
      popUpStyle: this.props.popUpStyle
    };
  },
  getDefaultProps: function() {
    return {
      defaultPopUpStyle: 'PopUpReact',
      onClickAceptar: undefined
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
  },
  componentWillReceiveProps: function(nextProps) {
    this.setState({
      htmlBodyContent: nextProps.htmlBodyContent,
      htmlHeaderContent: nextProps.htmlHeaderContent
    });
  },
  shouldComponentUpdate: function() {
    return true;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  show: function(message) {
    this.setState({
      isShow: true,
      message: message
    });
  },
  hide: function() {
    this.setState({
      isShow: false,
      message: ''
    });
  },
  onClickAceptar: function(evt) {
    this.hide();

    if(this.props.onClickAceptar != undefined) {
      this.props.onClickAceptar(evt);
    }
  },
  render: function() {  
    var self = this;
    var showStyle = 'componentShow';
    var hideStyle = 'componentHide';
    var componentShow = hideStyle;
    var buttons = [];
    var htmlBodyContent = this.state.htmlBodyContent;
    var htmlHeaderContent = this.state.htmlHeaderContent;
    var popUpStyle = undefined;

    if(this.state.isShow) {
      componentShow = showStyle;

    } else {
      componentShow = hideStyle;
    }

    if(this.state.popUpStyle != undefined) {
      popUpStyle = this.state.popUpStyle + ' ' + componentShow;

    } else {
      popUpStyle = this.props.defaultPopUpStyle + ' ' + componentShow;
    }

    if(this.state.htmlHeaderContent == undefined) {
      htmlHeaderContent = '';
    }

    if(this.state.buttons != undefined) {
        this.state.buttons.forEach(function(button, index) {
        var onClickButton = function(button, index, evt) {
          button.onClick(evt);
        }.bind(self, button, index);

        buttons.push(
          <button type='button' className='btn btn-success btn-md col-sm-3' onClick={onClickButton}>
            {button.label}
          </button>
        );
      });

    } else {
      //crear un boton de aceptar por default
      buttons.push(
        <button type='button' className='btn btn-success btn-md col-sm-3' onClick={this.onClickAceptar}>
          Aceptar
        </button>
      );
    }
    //crear contenido html o en forma de mensaje
    if(htmlBodyContent == undefined) {
      htmlBodyContent = (<p className='mensaje'>{this.state.message}</p>);
    }

    return (
      <div className={popUpStyle}>
        <section>
          <div className='container PopUp'>
              <div className='row'>
                  <div className='col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4'>
                      <div className='panel panel-default' id='main'>
                        <div className='panel-header'>
                          {htmlHeaderContent}
                        </div>
                        <div className='panel-body'>
                          <div style={{width: '100%', marginBottom: '5px'}}>
                            {htmlBodyContent}
                          </div>
                          <div className='form-group last'>
                            <div className='col-sm-12'>
                              {buttons}
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
});

module.exports = PopUpReact;