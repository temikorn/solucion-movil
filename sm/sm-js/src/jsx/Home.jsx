'use strict';

var Router = require('../libs/react-router/index.js');
var RouteHandler = require('../libs/react-router/components/RouteHandler.js');
var Label = require('../utils/Label.js');
var Constants = require('../utils/Constants.js');

var Home = React.createClass({
  mixins: [ Router.Navigation ],
 	getInitialState: function() {
    return {
      usuario: ctx.get('usuario'),
      shouldMount: true, 
      menuActivo: false,
      sectionTitle: Label.TITLE_ESTADISTICAS_VISITANTE,
      menuItemSelected: Constants.MenuItems.TITLE_ESTADISTICAS_VISITANTE
    };
  },
  componentWillMount: function() {
    if(!validaSession()) {
      this.setState({
        shouldMount: false
      });
      this.transitionTo('Login');
    }
  },
  componentDidMount: function() {
    if(!validaSession()) {
      this.setState({
        shouldMount: false
      });
      this.transitionTo('Login');
    }
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillReceiveProps: function(nextProps) {
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
    resetApp();
  },
  onClickEstadisticasSatisfaccion: function(evt) {
    evt.preventDefault();
    this.setState({
      sectionTitle: Label.TITLE_ESTADISTICAS_SATISFACCION,
      menuItemSelected: Constants.MenuItems.ESTADISTICAS_SATISFACCION
    });
    this.transitionTo('EstadisticasSatisfaccion');
  },
  onClickControlCierreAccion: function(evt) {
    evt.preventDefault();
    this.setState({
      sectionTitle: Label.TITLE_CONTROL_CIERRE,
      menuItemSelected: Constants.MenuItems.CONTROL_CIERRE
    });
    this.transitionTo('ControlCierreAcciones');
  },
  onClickConfigParametros: function(evt) {
    evt.preventDefault();
    this.setState({
      sectionTitle: Label.TITLE_CONFIG_PARAMETROS,
      menuItemSelected: Constants.MenuItems.CONFIG_PARAMETROS
    });
    this.transitionTo('ConfigParametros');
  },
  onClickAdminGeneralAccion: function(evt) {
    evt.preventDefault();
    this.setState({
      sectionTitle: Label.TITLE_ADMIN_GENERAL_ACCION,
      menuItemSelected: Constants.MenuItems.ADMIN_GENERAL_ACCION
    });
    this.transitionTo('AdminGeneralAccion');
  },
  onClickAdminLlamadoAccion: function(evt) {
    evt.preventDefault();
    this.setState({
      sectionTitle: Label.TITLE_ADMIN_LLAMADO_ACCION,
      menuItemSelected: Constants.MenuItems.ADMIN_LLAMADO_ACCION
    });
    this.transitionTo('AdminLlamadoAccion');
  },
  onClickAdminAcciones: function(evt) {
    evt.preventDefault();
    this.setState({
      sectionTitle: Label.TITLE_ADMIN_ACCIONES,
      menuItemSelected: Constants.MenuItems.ADMIN_ACCIONES
    });
    this.transitionTo('AdminAcciones');
  },
  onClickAdminVisitantes: function(evt) {
    evt.preventDefault();
    this.setState({
      sectionTitle: Label.TITLE_ADMIN_VISITANTES,
      menuItemSelected: Constants.MenuItems.ADMIN_VISITANTES
    });
    this.transitionTo('AdminVisitantes');
  },
  onClickHome: function(evt) {
    this.setState({
      sectionTitle: Label.TITLE_ESTADISTICAS_VISITANTE,
      menuItemSelected: Constants.MenuItems.ESTADISTICAS_VISITANTE
    });
  },
  onClickMenuItem: function(evt) {
    evt.preventDefault();
  },
  render: function() {
    var nombreUsuario = this.state.usuario.getNombre();
    var popUpLoadingHide = 'componentHide';
    var MENU_ITEM_ACTIVO = 'activoMenu';
    var MENU_ITEM_NO_ACTIVO = '';
    var styleActiveEstadisticaVisitante = MENU_ITEM_NO_ACTIVO;
    var styleActiveEstadisticaSatisfaccion = MENU_ITEM_NO_ACTIVO;
    var styleActiveConfigParametros = MENU_ITEM_NO_ACTIVO;
    var styleActiveControlCierre = MENU_ITEM_NO_ACTIVO;
    var styleActiveAdminGeneralAccion = MENU_ITEM_NO_ACTIVO;
    var styleActiveAdminAcciones = MENU_ITEM_NO_ACTIVO;
    var styleActiveAdminVisitantes = MENU_ITEM_NO_ACTIVO;
    var styleActiveAdminLlamadoAccion = MENU_ITEM_NO_ACTIVO;

    if(!this.state.shouldMount) {
      return (<div></div>);
    }

    switch(this.state.menuItemSelected) {
      case Constants.MenuItems.ESTADISTICAS_VISITANTE:
        styleActiveEstadisticaVisitante = MENU_ITEM_ACTIVO;
      break;

      case Constants.MenuItems.ESTADISTICAS_SATISFACCION:
        styleActiveEstadisticaSatisfaccion = MENU_ITEM_ACTIVO;
      break;

      case Constants.MenuItems.CONTROL_CIERRE:
        styleActiveControlCierre = MENU_ITEM_ACTIVO;
      break;

      case Constants.MenuItems.CONFIG_PARAMETROS:
        styleActiveConfigParametros = MENU_ITEM_ACTIVO;
      break;

      case Constants.MenuItems.ADMIN_GENERAL_ACCION:
        styleActiveAdminGeneralAccion = MENU_ITEM_ACTIVO;
      break;

      case Constants.MenuItems.ADMIN_ACCIONES:
        styleActiveAdminAcciones = MENU_ITEM_ACTIVO;
      break;

      case Constants.MenuItems.ADMIN_VISITANTES:
        styleActiveAdminVisitantes = MENU_ITEM_ACTIVO;
      break;

      case Constants.MenuItems.ADMIN_LLAMADO_ACCION:
        styleActiveAdminLlamadoAccion = MENU_ITEM_ACTIVO;
      break;
    }

    return (
      <div id='home' style={{width:'100%', height:'100%'}}>
        <div id='backgroundEspecial' className={popUpLoadingHide}>
          <span id='loading'></span>
        </div>

        <header id='header'>
          <hgroup>
            <h1 className='site_title'><a href='#/home' onClick={this.onClickHome}>{Label.SITE_TITLE}</a></h1>
            <h2 className='section_title'>{this.state.sectionTitle}</h2><div className='btn_view_site'><a href='#/'>Salir</a></div>
          </hgroup>
        </header>
    
        <section id='secondary_bar'>
          <div className='user'>
            <p>{nombreUsuario}</p>
          </div>
          <div className='breadcrumbs_container'>
            <article className='breadcrumbs'>
              <a href='#/home' onClick={this.onClickHome}>Inicio</a> 
              <div className='breadcrumb_divider'></div> 
              <a className='current'>{this.state.sectionTitle}</a>
            </article>
          </div>
        </section>

        <aside id='sidebar' className='column'>
          <h3>Menú</h3>
          <ul className='toggle'>
            <li className='icn_graphic_visitor'>
              <a href='#/home' onClick={this.onClickHome} className={styleActiveEstadisticaVisitante}>
                {Label.TITLE_ESTADISTICAS_VISITANTE}
              </a>
            </li>
            <li className='icn_graphic_satisfaction'>
              <a href='#' onClick={this.onClickEstadisticasSatisfaccion} className={styleActiveEstadisticaSatisfaccion}>
                {Label.TITLE_ESTADISTICAS_SATISFACCION}
              </a>
            </li>
            <li className='icn_change_action'>
              <a href='#' onClick={this.onClickControlCierreAccion} className={styleActiveControlCierre}>
                {Label.TITLE_CONTROL_CIERRE}
              </a>
            </li>
            <li className='icn_tool_action'>
              <a href='#' onClick={this.onClickConfigParametros} className={styleActiveConfigParametros}>
                {Label.TITLE_CONFIG_PARAMETROS}
              </a>
            </li>
            <li className='icn_tool_action'>
              <a href='#' onClick={this.onClickAdminGeneralAccion} className={styleActiveAdminGeneralAccion}>
                {Label.TITLE_ADMIN_GENERAL_ACCION}
              </a>
            </li>
            <li className='icn_tool_action'>
              <a href='#' onClick={this.onClickAdminAcciones} className={styleActiveAdminAcciones}>
                {Label.TITLE_ADMIN_ACCIONES}
              </a>
            </li>
            <li className='icn_tool_action'>
              <a href='#' onClick={this.onClickAdminVisitantes} className={styleActiveAdminVisitantes}>
                {Label.TITLE_ADMIN_VISITANTES}
              </a>
            </li>
            <li className='icn_tool_action'>
              <a href='#' onClick={this.onClickAdminLlamadoAccion} className={styleActiveAdminLlamadoAccion}>
                {Label.TITLE_ADMIN_LLAMADO_ACCION}
              </a>
            </li>
          </ul>
          
          <footer>
            <hr/>
            <p><strong>Copyright &copy; 2015 Administración MyT </strong></p>
          </footer>
        </aside>
    
        <section id='main' className='column'>
          <RouteHandler />
        </section>
      </div>
    );
  }
});

module.exports = Home;