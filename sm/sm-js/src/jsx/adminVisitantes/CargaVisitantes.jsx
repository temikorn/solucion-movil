'use strict';

var Context = require('../../utils/Context.js');
var Constants = require('../../utils/Constants.js');
var ErrorCodes = require('../../utils/ErrorCodes.js');
//### seccion para componentes comunes ###
var PopUpReact = require('../../components/commons/PopUpReact.js');
//### seccion para servicios  ###
var validaService = require('../../utils/ValidaService.js');
var cargaVisitantesService = require('../../modulos/CargaVisitantesService.js');

var CargaVisitantes = React.createClass({
  getInitialState: function() {
    return {
      isShow: true,
      datePicked: new Date(),
      dateView: new Date(),
      dateFormat: this.props.dateFormat,
      numVisitantes: 0,
      cargaVisitantes: []
    };
  },
  getDefaultProps: function() {
    var oneMinute = 60 * 1000;
    var oneHour = oneMinute * 60;
    var oneDay = oneHour * 24;
    var oneWeek = oneDay * 7;

    var ctx = new Context();
    ctx.put('oneMinute', oneMinute);
    ctx.put('oneHour', oneHour);
    ctx.put('oneDay', oneDay);
    ctx.put('oneWeek', oneWeek);
    ctx.put('lastDayInMonth', Constants.lastDayInMonth);

    return {
      dateFormat: 'dd/mm/yyyy',
      ctx: ctx
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
    var self = this;

    if(validaSession()) {

      var onSuccess = function(res) {
        self.setState({
          cargaVisitantes: self.convertFechaCargas(res.cargaVisitantes)
        });
      };

      var params = {
        fechaCarga: this.state.dateView
      };
      cargaVisitantesService.getNumVisitantesByMonth(params, onSuccess, this.onError, this.onFail);

    } else {
      this.setState({
        shouldMount: false
      });
    }
  },
  componentWillReceiveProps: function(nextProps) {
    this.setState({

    });
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  onFail: function(err) {
    console.log(err);
    this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
  },
  onError: function(err) {
    console.log(err);
    
    switch(err.errorCode) {
      case ErrorCodes.ERR_01:
        this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
      break;

      case ErrorCodes.ERR_03:
        this.refs.popUpAlert.show(ErrorCodes.ERR_03_MSG);
      break;

      default:
        console.log('Error no categorizado...');
      break;
    } 
  },
  reset: function() {
    this.setState({
      isShow: true,
      datePicked: new Date(),
      dateView: new Date(),
      numVisitantes: 0,
      cargaVisitantes: []
    });
  },
  show: function(evt) {
    this.setState({
      isShow: true
    });
  },
  hide: function() {
    this.setState({
      isShow: false
    });
  },
  isBisiesto: function(dateView) {
    var isBisiesto = false;
    var anioCalcular = dateView.getFullYear();
    //calcular si es biciesto y comunicarlo
    if ((anioCalcular % 4 == 0) && ((anioCalcular % 100 != 0) || (anioCalcular % 400 == 0))) {
      isBisiesto = true;
    }

    return isBisiesto;
  },  
  lastDayInMonth: function(dateView) {
    var lastDay = 0;
    var month = dateView.getMonth();
    var isBiciesto = false;
    var BICIESTO_LAST_DAY = 28;

    isBiciesto = this.isBisiesto(dateView);    

    if(isBiciesto && month == Constants.Months.FEBREO) {
      lastDay = BICIESTO_LAST_DAY;
      
    } else {
      lastDay = this.props.ctx.get('lastDayInMonth')[month];
    }

    return lastDay;
  },
  firstDayInWeek: function(dateView) {
    var firstDayInWeek = 0;
    var dateTmp = new Date(dateView.getTime());
    var FIRST_DAY = 1;

    dateTmp.setDate(FIRST_DAY);

    return dateTmp.getDay();
  },
  onPickDateOn: function(dayCount, dateView, cantidadXDia, evt) {
    evt.preventDefault();
    var datePicked = new Date(dateView.getTime());

    datePicked.setDate(dayCount);
    this.refs.popUpAskCargaVisitantes.show();

    this.setState({
      datePicked: datePicked,
      numVisitantes: cantidadXDia      
    });
  },
  onPickDateOff: function(dayCount, dateView, evt) {
    evt.preventDefault();
  },
  onNextMonth: function(evt) {
    evt.preventDefault();
    var self = this;
    var dateView = this.state.dateView;

    dateView.setMonth(dateView.getMonth() + 1);

    var onSuccess = function(res) {
      self.setState({
        numVisitantes: 0,
        dateView: dateView,
        cargaVisitantes: self.convertFechaCargas(res.cargaVisitantes)
      });
    };  

    var params = {
      fechaCarga: dateView
    };
    cargaVisitantesService.getNumVisitantesByMonth(params, onSuccess, this.onError, this.onFail);
  },
  onNextYear: function(evt) {
    evt.preventDefault();
    var self = this;
    var dateView = this.state.dateView;

    dateView.setFullYear(dateView.getFullYear() + 1);

    var onSuccess = function(res) {
      self.setState({
        numVisitantes: 0,
        dateView: dateView,
        cargaVisitantes: self.convertFechaCargas(res.cargaVisitantes)
      });
    };  

    var params = {
      fechaCarga: dateView
    };
    cargaVisitantesService.getNumVisitantesByMonth(params, onSuccess, this.onError, this.onFail);
  },
  onPrevMonth: function(evt) {
    evt.preventDefault();
    var self = this;
    var dateView = this.state.dateView;

    dateView.setMonth(dateView.getMonth() - 1);   

    var onSuccess = function(res) {
      self.setState({
        numVisitantes: 0,
        dateView: dateView,
        cargaVisitantes: self.convertFechaCargas(res.cargaVisitantes)
      });
    };  

    var params = {
      fechaCarga: dateView
    };
    cargaVisitantesService.getNumVisitantesByMonth(params, onSuccess, this.onError, this.onFail);
  },
  onPrevYear: function(evt) {
    evt.preventDefault();
    var self = this;
    var dateView = this.state.dateView;

    dateView.setFullYear(dateView.getFullYear() - 1);

    var onSuccess = function(res) {
      self.setState({
        numVisitantes: 0,
        dateView: dateView,
        cargaVisitantes: self.convertFechaCargas(res.cargaVisitantes)
      });
    };  

    var params = {
      fechaCarga: dateView
    };
    cargaVisitantesService.getNumVisitantesByMonth(params, onSuccess, this.onError, this.onFail);
  },
  findPos: function (obj) {
    var curleft = 0;
    var curtop = 0;

    if (obj.offsetParent) {
      curleft = obj.offsetLeft
      curtop = obj.offsetTop

      while (obj = obj.offsetParent) {
        curleft += obj.offsetLeft
        curtop += obj.offsetTop
      }
    }

    return {
      top: curtop,
      left: curleft
    };
  },
  convertFechaCargas: function(cargaVisitantes) {
    if(cargaVisitantes != undefined && cargaVisitantes.length > 0) {
      cargaVisitantes.forEach(function(cargaVisitante) {
        var fechaSplit = cargaVisitante.fechaCarga.split("-");
        cargaVisitante.fechaCarga = new Date(parseInt(fechaSplit[0]), (parseInt(fechaSplit[1])-1), parseInt(fechaSplit[2]));
      });
    }

    return cargaVisitantes;
  },
  onChangeCargaVisitantes: function(evt) {
    if(evt.target.value == "" || validaService.isOnlyNumbers(evt.target.value)) {
      this.setState({
        numVisitantes: evt.target.value
      });
    }
  },
  onClickCancelarCargaVisitantes: function(evt) {
    this.refs.popUpAskCargaVisitantes.hide();
    this.setState({
      numVisitantes: 0
    });
  },
  onClickAceptarCargaVisitantes: function(evt) {
    var self = this;
    var datePicked = this.state.datePicked;
    var numVisitantes = this.state.numVisitantes;

    if(numVisitantes == "") {
      numVisitantes = 0;
    }

    var onSuccess = function(res) {
      self.refs.popUpAskCargaVisitantes.hide();
      self.recargarCargaVisitantes();
    };  

    var params = {
      fechaCarga: datePicked,
      numVisitantes: numVisitantes
    };
    cargaVisitantesService.cargarNumVisitantes(params, onSuccess, this.onError, this.onFail);
  },
  recargarCargaVisitantes: function() {
    var self = this;
    var dateView = this.state.dateView;

    var onSuccess = function(res) {
      self.setState({
        numVisitantes: 0,
        dateView: dateView,
        cargaVisitantes: self.convertFechaCargas(res.cargaVisitantes)
      });
    };  

    var params = {
      fechaCarga: dateView
    };
    cargaVisitantesService.getNumVisitantesByMonth(params, onSuccess, this.onError, this.onFail);
  },
  render: function() {  
    var self = this;
    var componentShow = 'componentShow';
    var componentHide = 'componentHide';
    var datePickerStyle = 'react-cal-container';
    var rowsHTML = [];
    var colsHTML = [];
    var calTitle = '';
    var datePicked = '';
    var cargaVisitantesHeaderHTML = [];
    var cargaVisitantesBodyHTML = [];

    if(this.state.isShow) {
      datePickerStyle += ' ' + componentShow;

    } else {
      datePickerStyle += ' ' + componentHide;
    }

    //crear el calendario
    var initCol = 0;
    var MAX_DAYS_PER_WEEK = 7;
    var continueDayOfMonth = false;

    var dayCount = 0;
    var lastDay = this.lastDayInMonth(this.state.dateView);
    var dayInWeek = this.firstDayInWeek(this.state.dateView);

    while(dayCount <= lastDay) {
      var colHTML = [];

      for(initCol = 0; initCol < MAX_DAYS_PER_WEEK; initCol++) {
        if(initCol == dayInWeek && !continueDayOfMonth) {
          dayCount++;
          continueDayOfMonth = true;
        }

        if(dayCount > 0 && dayCount <= lastDay) {
          var cantidadXDia = 0;
          var onPickDateOn = undefined;

          var stylePicked = {};

          if(this.state.datePicked.getDate() == dayCount && 
              this.state.datePicked.getMonth() == this.state.dateView.getMonth() && 
              this.state.datePicked.getFullYear() == this.state.dateView.getFullYear()) {
            stylePicked = {
              background: '#6FC1D6'
            }; 
          }
          //obtener el numero de visitantes del dia calculado
          self.state.cargaVisitantes.every(function(cargaVisitante) {
            if(cargaVisitante.fechaCarga.getDate() == dayCount) {
              cantidadXDia = cargaVisitante.cantidadVisitantes;
              
              return false;
            }

            return true;
          });

          onPickDateOn = function(dayCount, dateView, cantidadXDia, evt){
            self.onPickDateOn(dayCount, dateView, cantidadXDia, evt);
          }.bind(self, dayCount, this.state.dateView, cantidadXDia);

          colHTML.push(
            <td style={stylePicked} onDoubleClick={onPickDateOn}>
              <div className='dayCountStyle' style={{width: '100%'}}>
                {dayCount}
              </div>
              <div className='dayCountXDayStyle' style={{float: 'left', width: '100%', textAlign: 'center'}}>
                {cantidadXDia}
              </div>
            </td>
          );
          dayCount++;

        } else {
          var onPickDateOff = function(dayCount, dateView, evt){
            self.onPickDateOff(dayCount, dateView, evt);
          }.bind(self, 0, this.state.dateView);

          colHTML.push(<td className='react-cal-off'><a href='#' onClick={onPickDateOff}></a></td>);
        }
      }

      rowsHTML.push(<tr>
        {colHTML}
      </tr>);
    }
    //crear etiqueta del titulo de mes y anio que se esta calculando
    calTitle = Constants.MonthsFull[this.state.dateView.getMonth()] + ' ' + this.state.dateView.getFullYear();
    //crear etiqueta para el campo en base al formato deseado (TODO)
    datePicked = (this.state.datePicked.getDate() + '/' + (this.state.datePicked.getMonth() + 1) + '/' + this.state.datePicked.getFullYear());
    //crear contendo html para popup de cargar visitante
    cargaVisitantesHeaderHTML.push(
      <p>Cargar número de visitantes en el museo</p>
    );
    cargaVisitantesBodyHTML.push(
      <div>
        <label htmlFor='numVisitantes'>Número visitantes: </label>
        <input name='numVisitantes' type='text' style={{width:'71%', marginLeft: '10px'}} placeholder='Número de visitantes' 
          maxLength='100' value={this.state.numVisitantes} onChange={this.onChangeCargaVisitantes}/>
      </div>
    );

    return (
      <div className={datePickerStyle}>
        <PopUpReact ref='popUpAlert' type='alert' popUpStyle='PopUpReactAlert'/>
        <PopUpReact ref='popUpAskCargaVisitantes' type='ask' htmlHeaderContent={cargaVisitantesHeaderHTML}
          htmlBodyContent={cargaVisitantesBodyHTML}
          buttons={[
            {name: 'Aceptar', label: 'Aceptar', onClick: this.onClickAceptarCargaVisitantes},
            {name: 'Cancelar', label: 'Cancelar', onClick: this.onClickCancelarCargaVisitantes}
        ]}/>

        <div className='react-cal'>
          <table className='react-cal-table' style={{height: '500px'}}>
            <caption className='react-cal-caption'>
              <div style={{float: 'left', width: '5%', fontSize: '22px'}}> 
                <a href='#' className='react-prev' onClick={this.onPrevYear}>&laquo;&laquo;</a>
              </div>
              <div style={{float: 'left', width: '5%', fontSize: '22px'}}> 
                <a href='#' className='react-prev' onClick={this.onPrevMonth}>&laquo;</a>
              </div>
              <div style={{float: 'left', width: '80%', fontSize: '22px'}}> 
                {calTitle}
              </div>
              <div style={{float: 'left', width: '5%', fontSize: '22px'}}> 
                <a href='#' className='react-next' onClick={this.onNextMonth}>&raquo;</a>
              </div>
              <div style={{float: 'left', width: '5%', fontSize: '22px'}}> 
                <a href='#' className='react-next' onClick={this.onNextYear}>&raquo;&raquo;</a>
              </div>
            </caption>
            <thead>
              <tr>
                <td className='react-cal-thead-td'>D</td>
                <td className='react-cal-thead-td'>L</td>
                <td className='react-cal-thead-td'>M</td>
                <td className='react-cal-thead-td'>M</td>
                <td className='react-cal-thead-td'>J</td>
                <td className='react-cal-thead-td'>V</td>
                <td className='react-cal-thead-td'>S</td>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <td colSpan='100%'>
                </td>
              </tr>
            </tfoot>
            <tbody className='react-cal-body'>
              {rowsHTML}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
});

module.exports = CargaVisitantes;