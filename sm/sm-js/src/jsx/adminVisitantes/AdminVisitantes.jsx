'use strict';

/* # seccion para declarar utilerias # */
var Clone = require('../../utils/Clone.js');
var Constants = require('../../utils/Constants.js');
var ErrorCodes = require('../../utils/ErrorCodes.js');
/* # seccion para declarar compronentes comunes # */
var PopUpReact = require('../../components/commons/PopUpReact.js');
/* # seccion para declarar servicios # */
/* # Seccion de componentes a los que depende # */
var CargaVisitantes = require('../../components/adminVisitantes/CargaVisitantes.js');
var BusquedaVisitantes = require('../../components/adminVisitantes/BusquedaVisitantes.js');
var AgendaVisitantes = require('../../components/adminVisitantes/AgendaVisitantes.js');

/*################################################*/
/*###       Administración de visitantes       ###*/
/*################################################*/
var AdminVisitantes = React.createClass({
  getInitialState: function() {
    return {
      shouldMount: true,
      showComponent: false,
      tabSelected: 1,
    };
  },
  getDefaultProps: function() {
    return {
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
    var self = this;

    if(validaSession()) {

    } else {
      this.setState({
        shouldMount: false
      });
    }
  },
  componentWillReceiveProps: function(nextProps) {
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  onFail: function(err) {
    console.log(err);
    this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
  },
  onError: function(err) {
    console.log(err);
    
    switch(err.errorCode) {
      case ErrorCodes.ERR_01:
        this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
      break;

      case ErrorCodes.ERR_03:
        this.refs.popUpAlert.show(ErrorCodes.ERR_03_MSG);
      break;

      default:
        console.log('Error no categorizado...');
      break;
    } 
  },
  reset: function() {
    this.setState({
      tabSelected: 1,
    });
  },
  show: function() {
    this.setState({
      showComponent: true,
      tabSelected: 1
    });
  },
  hide: function() {
    this.setState({
      showComponent: false,
      tabSelected: 1
    });
  },
  onClickTab: function(tabSelected, evt) {
    switch(tabSelected) {
      case Constants.ADMIN_VISITANTES.TAB_CARGA_VISITANTES:
      break;

      case Constants.ADMIN_VISITANTES.TAB_BUSQUEDA_VISITANTES:
      break;

      case Constants.ADMIN_VISITANTES.TAB_AGENDA_VISITANTES:
      break;
    }

    this.setState({
      tabSelected: tabSelected
    });
  },
  render: function() {
    var self = this;
    var componentShow = 'componentShow';
    var componentHide = 'componentHide';
    var styleTabContent = 'tab_content';

    var activeTabCargaVisitantes = '';
    var styleTabCargaVisitantes = componentHide;
    var onTabCargaVisitantes = undefined;

    var activeTabBusquedaVisitantes = '';
    var styleTabBusquedaVisitantes = componentHide;
    var onTabBusquedaVisitantes = undefined;

    var activeTabAgendaVisitantes = '';
    var styleTabAgendaVisitantes = componentHide;
    var onTabAgendaVisitantes = undefined;

    if(!this.state.shouldMount) {
      return (<div></div>);
    }
    //que hacer dependiendo del tab seleccionado
    switch(this.state.tabSelected) {
      case Constants.ADMIN_VISITANTES.TAB_CARGA_VISITANTES:
        styleTabCargaVisitantes = (styleTabContent + ' ' + componentShow);
        activeTabCargaVisitantes = 'active';
      break;

      case Constants.ADMIN_VISITANTES.TAB_BUSQUEDA_VISITANTES:
        styleTabBusquedaVisitantes = (styleTabContent + ' ' + componentShow);
        activeTabBusquedaVisitantes = 'active';
      break;

      case Constants.ADMIN_VISITANTES.TAB_AGENDA_VISITANTES:
        styleTabAgendaVisitantes = (styleTabContent + ' ' + componentShow);
        activeTabAgendaVisitantes = 'active';
      break;
    }
    //crear acciones para los tab existentes
    onTabCargaVisitantes = function(tabSelected, evt) {
      self.onClickTab(tabSelected, evt);
    }.bind(self, Constants.ADMIN_VISITANTES.TAB_CARGA_VISITANTES);

    onTabBusquedaVisitantes = function(tabSelected, evt) {
      self.onClickTab(tabSelected, evt);
    }.bind(self, Constants.ADMIN_VISITANTES.TAB_BUSQUEDA_VISITANTES);

    onTabAgendaVisitantes = function(tabSelected, evt) {
      self.onClickTab(tabSelected, evt);
    }.bind(self, Constants.ADMIN_VISITANTES.TAB_AGENDA_VISITANTES);
    //#####################  
    return (
      <div id='container-gral'>
        <PopUpReact ref='popUpAlert' type='alert' popUpStyle='PopUpReactAlert'/>

        <nav>
          <ul id='menuNav'>
            <li className={activeTabCargaVisitantes} style={{width: '33%'}} onClick={onTabCargaVisitantes}>Carga de Visitantes</li>
            <li className={activeTabBusquedaVisitantes} style={{width: '33%'}} onClick={onTabBusquedaVisitantes}>Búsqueda de visitantes</li>
            <li className={activeTabAgendaVisitantes} style={{width: '33%'}} onClick={onTabAgendaVisitantes}>Agenda de visitantes</li>
          </ul>
        </nav>
        
        <div className='tab_container'>

          <div className={styleTabCargaVisitantes}>
            <article className='module width_full'>
              <CargaVisitantes ref='cargaVisitantes' />
            </article>
          </div>

          <div className={styleTabBusquedaVisitantes}>
            <BusquedaVisitantes ref='busquedaVisitantes'/>
          </div>
          
          <div className={styleTabAgendaVisitantes}>
            <AgendaVisitantes ref='agendaVisitantes'/>
          </div>

        </div>
        <div className='spacer'></div>
      </div>
    );
  }
});

module.exports = AdminVisitantes;
