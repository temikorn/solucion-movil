'use strict';

/* # seccion para declarar utilerias # */
var FunUtils = require('../../utils/FunUtils.js');
var Clone = require('../../utils/Clone.js');
var Constants = require('../../utils/Constants.js');
var ErrorCodes = require('../../utils/ErrorCodes.js');
var Label = require('../../utils/Label.js');
var Context = require('../../utils/Context.js');
var highCharts = require('../../utils/HighCharts.js');
/* # seccion para declarar compronentes comunes # */
var DatePickerReact = require('../../components/commons/DatePickerReact.js');
var PopUpReact = require('../../components/commons/PopUpReact.js');
/* # seccion para declarar servicios # */
var cargaVisitantesService = require('../../modulos/CargaVisitantesService.js');
/* # Seccion de componentes a los que depende # */

/*################################################*/
/*###       Busqueda de visitantes       ###*/
/*################################################*/
var BusquedaVisitantes = React.createClass({
  getInitialState: function() {
    return {
      shouldMount: true,
      showComponent: false,
      tabSelected: 1,
      pagina: 0,
      resultPerPagina: 10,
      fechaInicio: new Date(),
      fechaFinal: new Date(),
      resultBusqueda: undefined,
      resultBusquedaOrg: undefined,
      fechaInicioBusqueda: undefined,
      fechaFinalBusqueda: undefined,
      emailCorrecto: false,
      emailIncorrecto: false,
      emailVacio: false,
      totalCorrectos: 0,
      totalIncorrectos: 0,
      totalVacios: 0,
      isResultBusqueda: false
    };
  },
  getDefaultProps: function() {
    return {
    };
  },  
  componentWillMount: function() {
    this.storage = new Context();
    this.storage.put('graficas', []);
  },
  componentDidMount: function() {
    var self = this;

    if(validaSession()) {

    } else {
      this.setState({
        shouldMount: false
      });
    }
  },
  componentWillReceiveProps: function(nextProps) {
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
    this.storage.get('graficas').forEach(function(grafica) {
      grafica.redraw();
      grafica.reflow();
    });
  },
  componentWillUnmount: function() {
  },
  onFail: function(err) {
    console.log(err);
    this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
  },
  onError: function(err) {
    console.log(err);
    
    switch(err.errorCode) {
      case ErrorCodes.ERR_01:
        this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
      break;

      case ErrorCodes.ERR_03:
        this.refs.popUpAlert.show(ErrorCodes.ERR_03_MSG);
      break;

      default:
        console.log('Error no categorizado...');
      break;
    } 
  },
  reset: function() {
    this.setState({
      resultBusqueda: undefined
    });
  },
  show: function() {
    this.setState({
      showComponent: true
    });
  },
  hide: function() {
    this.setState({
      showComponent: false
    });
  },
  onClickTab: function(tabSelected, evt) {
    switch(tabSelected) {
      case Constants.BUSQUEDA_VISITANTES.TAB_BUSQUEDA_VISITANTES:
      break;

      case Constants.BUSQUEDA_VISITANTES.TAB_GRAFICAS_VISITANTES:

      break;
    }

    this.setState({
      tabSelected: tabSelected
    });
  },
  onClickNextPage: function(evt) {
    evt.preventDefault();
    var nextPage = 0;

    if(this.state.resultBusqueda != undefined) {
      nextPage = (this.state.pagina + 1);

      if(nextPage <= (this.state.resultBusqueda.length - 1) ) {
        this.setState({
          pagina: nextPage
        });  
      }
    }
  },
  onClickBackPage: function(evt) {
    evt.preventDefault();
    var backPage = 0;
    var firstPage = 0;

    if(this.state.resultBusqueda != undefined) {
      backPage = (this.state.pagina - 1);

      if(backPage >= firstPage) {
        this.setState({
          pagina: backPage
        });  
      }
    }
  },
  onClickFirstPage: function(evt) {
    evt.preventDefault();
    var firstPage = 0;

    if(this.state.resultBusqueda != undefined) {
      if(firstPage != this.state.pagina) {
        this.setState({
          pagina: firstPage
        });  
      }
    }
  },
  onClickLastPage: function(evt) {
    evt.preventDefault();
    var lastPage = 0;

    if(this.state.resultBusqueda != undefined) {
      lastPage = (this.state.resultBusqueda.length - 1);

      if(lastPage != this.state.pagina) {
        this.setState({
          pagina: lastPage
        });
      }
    }
  },
  onChangeResultPerPagina: function(evt) {
    if(this.state.resultBusquedaOrg != undefined) {
      this.setState({
        pagina: 0,
        resultPerPagina: evt.target.value,
        resultBusqueda: this.getPaginado(this.state.resultBusquedaOrg, evt.target.value)
      });
    }
  },
  getPaginado: function(resultBusqueda, resultPerPagina) {
    var paginado = undefined;
    var pagina = undefined;
    var i = 0;

    if(resultBusqueda != undefined && resultBusqueda.length > 0) {
      paginado = [];
      pagina = [];

      resultBusqueda.forEach(function(result, index) {
        pagina.push(result);
        
        if(i < resultPerPagina) {
          i++
        } 

        if(i == resultPerPagina || index == (resultBusqueda.length - 1)) {
          paginado.push(pagina);
          pagina = [];
          i = 0;
        }
      });
    }

    return paginado;
  },
  onDatePickedFechaInicio: function(datePicked, evt) {
    this.setState({
      fechaInicio: datePicked
    });
  },
  onDatePickedFechaFinal: function(datePicked, evt) {
    this.setState({
      fechaFinal: datePicked
    });
  },
  onClickLimpiar: function(evt) {
    this.refs.fechaInicio.reset();
    this.refs.fechaFinal.reset();
    this.setState({
      tabSelected: 1,
      fechaInicio: new Date(),
      fechaFinal: new Date(),
      resultBusquedaOrg: undefined,
      resultBusqueda: undefined,
      emailCorrecto: false,
      emailIncorrecto: false,
      emailVacio: false,
      totalCorrectos: 0,
      totalIncorrectos: 0,
      totalVacios: 0,
      isResultBusqueda: false
    });
  },
  onClickBuscar: function(evt) {
    var self = this;

    var onSuccess = function(res) {
      var isResultBusqueda = false;
      var theChart = undefined;
      var resultBusquedaVisitante = undefined;
      var totalCorrectos = 0;
      var totalIncorrectos = 0;
      var totalVacios = 0;

      self.destroyGrafics();

      if(res.resultBusquedaVisitante == undefined || res.resultBusquedaVisitante.length == 0) {
        self.refs.popUpAlert.show(Label.NO_RESULTADOS);
        isResultBusqueda = false;

      } else {
        isResultBusqueda = true;
        resultBusquedaVisitante = self.convertFechas(res.resultBusquedaVisitante);
        self.createGrafica(resultBusquedaVisitante, 'barVisitantesComprometidos', {subTitle: 'Gráficas de visitantes', showInLegend: true, showInLegendSerie: true});
        //calcular el numero de emails correctos, incorrectos y vacios
        res.resultBusquedaVisitante.forEach(function(result) {
          totalCorrectos += result.numCorrectos;
          totalIncorrectos += result.numIncorrectos;
          totalVacios += result.numVacios;
        });
      }

      self.setState({
        pagina: 0,
        resultBusquedaOrg: resultBusquedaVisitante,
        resultBusqueda: self.getPaginado(resultBusquedaVisitante, self.state.resultPerPagina),
        fechaInicioBusqueda: self.state.fechaInicio,
        fechaFinalBusqueda: self.state.fechaFinal,
        totalCorrectos: totalCorrectos,
        totalIncorrectos: totalIncorrectos,
        totalVacios: totalVacios,
        isResultBusqueda: isResultBusqueda
      });
    };

    var params = {
      fechaInicio: this.state.fechaInicio,
      fechaFinal: this.state.fechaFinal,
      emailCorrecto: this.state.emailCorrecto,
      emailIncorrecto: this.state.emailIncorrecto,
      emailVacio: this.state.emailVacio
    };

    cargaVisitantesService.buscarCargaVisitante(params, onSuccess, this.onError, this.onFail);
  },
  convertFechas: function(resultBusquedaVisitante) {
    if(resultBusquedaVisitante != undefined && resultBusquedaVisitante.length > 0) {
      resultBusquedaVisitante.forEach(function(busquedaVisitante) {
        var fechaSplit = busquedaVisitante.fechaCarga.split("-");
        busquedaVisitante.fechaCarga = new Date(parseInt(fechaSplit[0]), (parseInt(fechaSplit[1])-1), parseInt(fechaSplit[2]));
      });
    }

    return resultBusquedaVisitante;
  },
  createGrafica: function(resultBusqueda, idContainer, options) {
    var self = this;
    var series = [];
    var serieVisitantes = [];
    var serieComprometidos = [];
    var categories = [];
    var theChart = undefined;

    resultBusqueda.forEach(function(result, index) {
      var fechaCarga = (result.fechaCarga.getDate() + '/' + (result.fechaCarga.getMonth() + 1) + '/' + result.fechaCarga.getFullYear());
      
      categories.push(fechaCarga);
      serieVisitantes.push(result.cantidadVisitantes);
      serieComprometidos.push(result.numComprometidos);
    });

    series.push({
      name: 'Visitantes', 
      data: serieVisitantes,
      extraOpts: undefined
    });

    series.push({
      name: 'Comprometidos', 
      data: serieComprometidos,
      extraOpts: undefined
    });

    theChart = highCharts.basicBar(idContainer, series, categories, options);
    this.storage.get('graficas').push(theChart);
  },
  destroyGrafics: function() {
    this.storage.get('graficas').forEach(function(grafica) {
      grafica.destroy();
    });
    this.storage.put('graficas', []);
  },
  convertFechaCargas: function(dateToformat) {
    var fechaSplit = dateToformat.split("-");
    var dateFormated = new Date(parseInt(fechaSplit[0]), (parseInt(fechaSplit[1])-1), parseInt(fechaSplit[2]));

    return dateFormated;
  },
  onClickExportExcel: function(evt) {
    evt.preventDefault();
    var self = this;

    var onSuccess = function(res) {
      var blob = FunUtils.b64toBlob(res.exportToExcel, Constants.EXCEL_CONTENCT_TYPE);
      var URL = window.URL || window.webkitURL || window.mozURL || window.msURL;
      var blobUrl = URL.createObjectURL(blob);
      var ventana = window.open(undefined, '_blank', 'titlebar=no, width=320, height=240');

      if(ventana != undefined) {
        //otra forma de descargar archivos
        //ventana.document.write('Su descarga iniciar&aacute; en un instante... <a id=descarga href=' + blobUrl + 
        //                        ' download='EstadisticasVisitante.xlsx'>estadisticasVisitante.xlsx</a>');
        //ventana.document.getElementById('descarga').click();

        //otra forma de descargar archivos
        var pom = document.createElement('a');
        pom.setAttribute('id', 'descarga');
        pom.setAttribute('href', blobUrl);
        pom.setAttribute('download', 'CargaVisitante.xlsx');
        pom.click();
        ventana.close();
        
      } else {
        console.log('Error - Ventanas emergentes no permitidas.');
        self.refs.popUpAlert.show(Label.NO_VENTANAS_EMERGENTES);
      }
    };

    var params = {
      cargaVisitantes: this.state.resultBusquedaOrg,
      fechaInicio: this.state.fechaInicioBusqueda,
      fechaFinal: this.state.fechaFinalBusqueda,
      emailCorrectos: this.state.totalCorrectos,
      emailIncorrectos: this.state.totalIncorrectos,
      emailVacios: this.state.totalVacios
    };
    cargaVisitantesService.exportCargaVisitante(params, onSuccess, this.onError, this.onFail);
  },
  onChangeCheckEmailCorrecto: function(evt) {
    this.setState({
      emailCorrecto: evt.target.checked      
    });
  },
  onChangeCheckEmailIncorrecto: function(evt) {
    this.setState({
      emailIncorrecto: evt.target.checked      
    });
  },
  onChangeCheckEmailVacio: function(evt) {
    this.setState({
      emailVacio: evt.target.checked      
    });
  },
  render: function() {
    var self = this;
    var componentShow = 'componentShow';
    var componentHide = 'componentHide';
    var exportExcel = '';
    var fechaInicioFormat = '';
    var fechaFinalFormat = '';
    var totalPaginado = '0 de 0 Páginas';
    var resultadosHTML = [];
    //#####################
    // control de tabs
    var styleTabContent = 'tab_content';
    var activeTabResultBusqueda = '';
    var styleTabResultBusqueda = componentHide;
    var onTabResultBusqueda = undefined;

    var activeTabGraficasVisitantes = '';
    var styleTabGraficasVisitantes = componentHide;
    var onTabGraficasVisitantes = undefined;
    //########################
    if(!this.state.shouldMount) {
      return (<div></div>);
    }
    //que hacer dependiendo del tab seleccionado
    switch(this.state.tabSelected) {
      case Constants.BUSQUEDA_VISITANTES.TAB_BUSQUEDA_VISITANTES:
        activeTabResultBusqueda = 'active';
        styleTabResultBusqueda = (styleTabContent + ' ' + componentShow);
      break;

      case Constants.BUSQUEDA_VISITANTES.TAB_GRAFICAS_VISITANTES:
        activeTabGraficasVisitantes = 'active';
        styleTabGraficasVisitantes = (styleTabContent + ' ' + componentShow);
      break;
    }
    //crear acciones para los tab existentes
    onTabResultBusqueda = function(tabSelected, evt) {
      self.onClickTab(tabSelected, evt);
    }.bind(self, Constants.BUSQUEDA_VISITANTES.TAB_BUSQUEDA_VISITANTES);

    onTabGraficasVisitantes = function(tabSelected, evt) {
      self.onClickTab(tabSelected, evt);
    }.bind(self, Constants.BUSQUEDA_VISITANTES.TAB_GRAFICAS_VISITANTES);
    //#####################  
    //si existen resultados en la busqueda
    if(this.state.isResultBusqueda) {
      exportExcel = (<a className='IconExcelStyle' href='#' onClick={this.onClickExportExcel}></a>);
    }

    if(this.state.fechaInicioBusqueda != undefined) {
      fechaInicioFormat = (this.state.fechaInicioBusqueda.getDate() + '/' + (this.state.fechaInicioBusqueda.getMonth() + 1) + '/' + this.state.fechaInicioBusqueda.getFullYear());
    }

    if(this.state.fechaFinalBusqueda != undefined) {
      fechaFinalFormat = (this.state.fechaFinalBusqueda.getDate() + '/' + (this.state.fechaFinalBusqueda.getMonth() + 1) + '/' + this.state.fechaFinalBusqueda.getFullYear());
    }
    //crear tabla de resultados de la busqueda
    if(this.state.resultBusqueda != undefined) {
      totalPaginado = ((this.state.pagina + 1) + ' de ' +  this.state.resultBusqueda.length + ' Páginas')
      this.state.resultBusqueda[this.state.pagina].forEach(function(result) {
        var fechaCarga = (result.fechaCarga.getDate() + '/' + (result.fechaCarga.getMonth() + 1) + '/' + result.fechaCarga.getFullYear());
        resultadosHTML.push(
          <tr> 
            <td>{fechaCarga}</td> 
            <td>{result.cantidadVisitantes}</td> 
            <td>{result.numComprometidos}</td>
            <td>{result.numCorrectos}</td>
            <td>{result.numIncorrectos}</td>
            <td>{result.numVacios}</td>
          </tr>
        );
      });
    }
    return (
      <div style={{width: '100%', height: '100%'}}>
        <PopUpReact ref='popUpAlert' type='alert' popUpStyle='PopUpReactAlert'/>
        <article className='module width_full'>
          <header><h3>Filtros de Búsqueda</h3></header>
          <div className='module_content' style={{width: '99.8%'}}>

            <fieldset style={{width:'48.8%', float:'left'}}>
              <DatePickerReact ref='fechaInicio' idCal='fechaInicio' inputLabel='Fecha Inicio:' onDatePicked={this.onDatePickedFechaInicio}/>
            </fieldset>

            <fieldset style={{width:'48.8%', float:'left', marginLeft: '10px'}}>
              <DatePickerReact ref='fechaFinal' idCal='fechaFinal' inputLabel='Fecha Final:' onDatePicked={this.onDatePickedFechaFinal}/>
            </fieldset>

            <fieldset style={{width:'99%', float:'left', marginLeft: '0px'}}>
              <div style={{width: '30%', float: 'left', marginLeft: '10px', marginRight: '10px'}}>
                <div style={{float: 'left', marginLeft: '10px', marginRight: '10px'}}>
                  <input type="checkbox" value={this.state.emailCorrecto} checked={this.state.emailCorrecto} onChange={this.onChangeCheckEmailCorrecto}/>
                </div>
                <div style={{width: '70%', marginLeft: '10px', float: 'left'}}>
                  Email correcto
                </div>
              </div>

              <div style={{width: '30%', float: 'left', marginLeft: '10px', marginRight: '10px'}}>
                <div style={{float: 'left', marginLeft: '10px', marginRight: '10px'}}>
                  <input type="checkbox" value={this.state.emailIncorrecto} checked={this.state.emailIncorrecto} onChange={this.onChangeCheckEmailIncorrecto}/>
                </div>
                <div style={{width: '70%', marginLeft: '10px', float: 'left'}}>
                  Email Incorrecto
                </div>
              </div>

              <div style={{width: '30%', float: 'left', marginLeft: '10px', marginRight: '10px'}}>
                <div style={{float: 'left', marginLeft: '10px', marginRight: '10px'}}>
                  <input type="checkbox" value={this.state.emailVacio} checked={this.state.emailVacio} onChange={this.onChangeCheckEmailVacio}/>
                </div>
                <div style={{width: '70%', marginLeft: '10px', float: 'left'}}>
                  Email Vacio
                </div>
              </div>
            </fieldset>

          </div>
          <footer>
            <div className='submit_link'>
              <input type='button' value='BUSCAR' className='alt_btn' onClick={this.onClickBuscar}/>
              <input type='button' value='LIMPIAR' onClick={this.onClickLimpiar}/>
            </div>
          </footer>
        </article>

        <nav>
          <ul id='menuNav'>
            <li className={activeTabResultBusqueda} onClick={onTabResultBusqueda}>Resultados de la búsqueda</li>
            <li className={activeTabGraficasVisitantes} onClick={onTabGraficasVisitantes}>Gáficas</li>
          </ul>
        </nav>

        <article className='module width_full'>
          <header>
            <div className='AlignContainer'>
              <div className='AlignLeft' style={{width: '95%'}}>
                <h3 className='tabs_involved'>Resultado de la Búsqueda</h3>
              </div>
              <div className='AlignRight' style={{width: '5%', marginTop: '5px', textAlign: 'center'}}>
                {exportExcel}
              </div>
            </div>
          </header>

          <article className='result'>
            <div className='overview' style={{width: '50%', height: '52px'}}>
              <p className='overview_title'>Fecha Inicio</p>
              <p className='overview_count'>{fechaInicioFormat}</p>
            </div>
            <div className='overview' style={{width: '50%', height: '52px'}}>
              <p className='overview_title'>Fecha Final</p>
              <p className='overview_count'>{fechaFinalFormat}</p>
            </div>
          </article>

          <article className='result'>
            <div className='overview' style={{width: '33.3%', height: '52px'}}>
              <p className='overview_title'>Email Corectos</p>
              <p className='overview_count'>{this.state.totalCorrectos}</p>
            </div>
            <div className='overview' style={{width: '33.3%', height: '52px'}}>
              <p className='overview_title'>Email Incorrectos</p>
              <p className='overview_count'>{this.state.totalIncorrectos}</p>
            </div>
            <div className='overview' style={{width: '33.3%', height: '52px'}}>
              <p className='overview_title'>Email Vacios</p>
              <p className='overview_count'>{this.state.totalVacios}</p>
            </div>
          </article>

          <div className='tab_container'>
            <div className={styleTabResultBusqueda}>
              <table className='tablesorter' cellSpacing='0'> 
                <thead> 
                  <tr> 
                    <th>Fecha</th> 
                    <th>Visitantes por día</th> 
                    <th>Comprometidos</th> 
                    <th>Correctos</th> 
                    <th>Incorrectos</th> 
                    <th>Vacios</th> 
                  </tr> 
                </thead> 
                <tbody> 
                  {resultadosHTML}
                </tbody>
                <tfoot>
                  <tr>
                    <td colSpan='100%'>
                      <div id='pag'>
                        <select id='soflow-color' value={this.state.resultPerPagina} onChange={this.onChangeResultPerPagina}>
                          <option value='5'>5</option>
                          <option value='10'>10</option>
                          <option value='15'>15</option>
                          <option value='20'>20</option>
                          <option value='25'>25</option>
                          <option value='50'>50</option>
                        </select>
                        <ul id='pagination'>
                          <li className='previous'><a href='#' onClick={this.onClickFirstPage}>&lt;&lt; Primero</a></li>
                          <li className='previous'><a href='#' onClick={this.onClickBackPage}>&lt; Anterior</a></li>
                          <li className='active'>{totalPaginado}</li>
                          <li className='next'><a href='#' onClick={this.onClickNextPage}>Siguiente &gt;</a></li>
                          <li className='next'><a href='#' onClick={this.onClickLastPage}>Último &gt;&gt;</a></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                </tfoot>
              </table>
            </div>

            <div className={styleTabGraficasVisitantes}>
              <table className='tablesorter' cellSpacing='0'>
                <tr>
                  <td>
                    <article className='module width_2_full' style={{height: '600px'}}>
                      <div id='barVisitantesComprometidos' style={{minWidth: '100%', maxWidth: '100%', width: '100%', height: '100%', margin: '0 auto'}}></div>
                    </article>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </article>
      </div>
    );
  }
});

module.exports = BusquedaVisitantes;
