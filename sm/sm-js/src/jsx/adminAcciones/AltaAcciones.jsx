'use strict';

/* # seccion para declarar utilerias # */
var Clone = require('../../utils/Clone.js');
var Context = require('../../utils/Context.js');
var ErrorCodes = require('../../utils/ErrorCodes.js');
var ErrorMsg = require('../../utils/ErrorMsg.js');
var Constants = require('../../utils/Constants.js');
/* # seccion para declarar compronentes comunes # */
var PopUpReact = require('../../components/commons/PopUpReact.js');
var CheckTableReact = require('../../components/commons/CheckTableReact.js');
var InputFileReact = require('../../components/commons/InputFileReact.js');
/* # seccion para declarar servicios # */
var accionesService = require('../../modulos/AccionesService.js');
/* # Seccion de componentes a los que depende # */

/*################################################*/
/*###             Alta de accion               ###*/
/*################################################*/
var AltaAcciones = React.createClass({
  getInitialState: function() {
    return {
      shouldMount: true,
      showComponent: false,
      tabSelected: 1,
      newAccion: {
        descripcion: '',
        idGeneralAccion: 0,
        idLlamadoAccion: 0
      },
      catGeneralAcciones: undefined,
      catLamadoAcciones: undefined,
      temas: undefined,
      temasClone: undefined,
      subTemasClone: undefined,
      sectores: undefined,
      sectoresClone: undefined,
      subSectoresClone: undefined,
      tiempos: undefined,
      tiemposClone: undefined,
      medios: undefined,
      mediosClone: undefined,
      niveles: undefined,
      nivelesClone: undefined,
      inversiones: undefined,
      inversionesClone: undefined,
      archivosAccionCount: 0,
      archivosAccion: []
    };
  },
  getDefaultProps: function() {
    return {
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
    var self = this;

    if(validaSession()) {
      this.storage = new Context();

      var onSuccess = function(res) {
        self.setState({
          catGeneralAcciones: res.generalAcciones,
          catLlamadoAcciones: res.llamadoAcciones,
          temas: res.temas,
          temasClone: Clone.clone(res.temas),
          sectores: res.sectores,
          sectoresClone: Clone.clone(res.sectores),
          tiempos: res.tiempos,
          tiemposClone: Clone.clone(res.tiempos),
          medios: res.medios,
          mediosClone: Clone.clone(res.medios),
          niveles: res.niveles,
          nivelesClone: Clone.clone(res.niveles),
          inversiones: res.inversiones,
          inversionesClone: Clone.clone(res.inversiones)
        });
      };

      var params = {};
      accionesService.getCatalogos(params, onSuccess, this.onError, this.onFail);

    } else {
      this.setState({
        shouldMount: false
      });
    }
  },
  componentWillReceiveProps: function(nextProps) {
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  onFail: function(err) {
    console.log(err);
    this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
  },
  onError: function(err) {
    console.log(err);
    
    switch(err.errorCode) {
      case ErrorCodes.ERR_01:
        this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
      break;

      case ErrorCodes.ERR_03:
        this.refs.popUpAlert.show(ErrorCodes.ERR_03_MSG);
      break;

      default:
        console.log('Error no categorizado...');
      break;
    } 
  },
  reset: function() {
    this.refs.sectoresChecked.reset();
    this.refs.subSectoresChecked.reset();
    this.refs.temasChecked.reset();
    this.refs.subTemasChecked.reset();
    this.refs.tiemposChecked.reset();
    this.refs.mediosChecked.reset();
    this.refs.nivelesChecked.reset();
    this.refs.inversionesChecked.reset();
    this.setState({
      tabSelected: 1,
      newAccion: {
        descripcion: '',
        idGeneralAccion: 0,
        idLlamadoAccion: 0
      },
      temasClone: Clone.clone(this.state.temas),
      subTemasClone: undefined,
      sectoresClone: Clone.clone(this.state.sectores),
      subSectoresClone: undefined,
      tiemposClone: Clone.clone(this.state.tiempos),
      mediosClone: Clone.clone(this.state.medios),
      nivelesClone: Clone.clone(this.state.niveles),
      inversionesClone: Clone.clone(this.state.inversiones),
      archivosAccionCount: 0,
      archivosAccion: []
    });
  },
  show: function() {
    this.setState({
      showComponent: true,
      tabSelected: 1
    });
  },
  hide: function() {
    this.setState({
      showComponent: false,
      tabSelected: 1
    });
  },
  onClickTab: function(tabSelected, evt) {
    switch(tabSelected) {
      case Constants.ADMIN_ALTA_ACCION.TAB_CONFIG_ACCION:
      break;

      case Constants.ADMIN_ALTA_ACCION.TAB_CONFIG_ARCHIVOS:
      break;
    }

    this.setState({
      tabSelected: tabSelected
    });
  },
  onChangeNombreAccion: function(evt) {
    var newAccion = this.state.newAccion;
    newAccion.descripcion = evt.target.value;

    this.setState({
      newAccion: newAccion
    });
  },
  onChangeGeneralAccion: function(evt) {
    var newAccion = this.state.newAccion;
    newAccion.idGeneralAccion = evt.target.value;

    this.setState({
      newAccion: newAccion
    });
  },
  onChangeLlamadoAccion: function(evt) {
    var newAccion = this.state.newAccion;
    newAccion.idLlamadoAccion = evt.target.value;

    this.setState({
      newAccion: newAccion
    });
  },
  onClickCancelarAltaAccion: function(evt) {
    this.reset();
    this.hide();
    this.props.onClickCancelarAltaAccion();
  },
  onClickGuardarAccion: function(evt) {
    var self = this;
    var result = {
      isError: false,
      erromsg: ''
    };

    var onSuccess = function(res) {
      self.refs.popUpGuardarSucces.show("La acción fue creada exitosamente.");
    };
    //construir objetos necesarios para el servicio eliminando las propiedades no necesarios
    var accion = {
      idAccion: 0,
      descripcion: this.state.newAccion.descripcion,
      idGeneralAccion: this.state.newAccion.idGeneralAccion,
      idLlamadoAccion: this.state.newAccion.idLlamadoAccion,
    };

    var temas = [];
    this.state.temasClone.forEach(function(tema) {
      if(tema.checked) {
        var subTemas = [];

        tema.subTemas.forEach(function(subTema) {
          if(subTema.checked) {
            subTemas.push({
              idSubTema: subTema.idSubTema,
              checked: subTema.checked
            });
          }
        });

        temas.push({
          idTema: tema.idTema,
          checked: tema.checked,
          subTemas: subTemas
        });
      }
    });

    var sectores = [];
    this.state.sectoresClone.forEach(function(sector) {
      var subSectores = [];
      var hasSubSectores = false;

      if(sector.checked) {
        if(sector.subSectores != 0) {
          hasSubSectores = true;
          sector.subSectores.forEach(function(subSector) {
            if(subSector.checked) {
              subSectores.push({
                idSubSector: subSector.idSubSector,
                checked: subSector.checked
              });
            }
          });
        }

        sectores.push({
          idSector: sector.idSector,
          checked: sector.checked,
          subSectores: subSectores,
          hasSubSectores: hasSubSectores
        });
      }
    });

    var tiempos = [];
    this.state.tiemposClone.forEach(function(tiempo) {
      if(tiempo.checked) {
        tiempos.push({
          idTiempo: tiempo.idTiempo,
          checked: tiempo.checked
        });
      }
    });

    var medios = [];
    this.state.mediosClone.forEach(function(medio) {
      if(medio.checked) {
        medios.push({
          idMedio: medio.idMedio,
          checked: medio.checked
        });
      }
    });

    var niveles = [];
    this.state.nivelesClone.forEach(function(nivel) {
      if(nivel.checked) {
        niveles.push({
          idNivel: nivel.idNivel,
          checked: nivel.checked
        });
      }
    });

    var inversiones = [];
    this.state.inversionesClone.forEach(function(inversion) {
      if(inversion.checked) {
        inversiones.push({
          idInversion: inversion.idInversion,
          checked: inversion.checked
        });
      }
    });
    //aqui poner codigo para validar los datos
    //minimo se debe seleciconar una de cada cosa
    result = this.validaGuardarAccion(temas, sectores, tiempos, medios, niveles, inversiones, accion, this.state.archivosAccion);

    if(!result.isError) {
      var params = {
        accion: accion,
        temas: temas,
        sectores: sectores,
        tiempos: tiempos,
        medios: medios,
        niveles: niveles,
        inversiones: inversiones,
        archivosAccion: this.state.archivosAccion
      };
      accionesService.guardarAccion(params, onSuccess, this.onError, this.onFail);

    } else {
      this.refs.popUpAlert.show(result.errorMsg);
    }
  },
  validaGuardarAccion: function(temas, sectores, tiempos, medios, niveles, inversiones, accion, archivosAccion) {
    var result = {
      isError: false,
      errorMsg: ''
    };

    if(accion.descripcion.trim() == '') {
      result = {
        isError: true,
        errorMsg: 'Es necesario escribir un nombre para la acción.'
      };

      return result;
    }

    if(accion.idGeneralAccion == 0) {
      result = {
        isError: true,
        errorMsg: 'Es necesario seleccionar un general acción.'
      };

      return result;
    }   

    if(accion.idLlamadoAccion == 0) {
      result = {
        isError: true,
        errorMsg: 'Es necesario seleccionar un llamdo a la acción.'
      };

      return result;
    }

    if(archivosAccion.length != 0) {
      archivosAccion.every(function (archivoAccion) {
        if(!archivoAccion.fileSelected) {
          result = {
            isError: true,
            errorMsg: 'Existen archivos sin cargar para la acción.'
          };
          return false;
        }

        return true;
      });

      if(result.isError) {
        return result;  
      }
      
    } else {
      result = {
        isError: true,
        errorMsg: 'Es necesario cargar minimo un archivo para la acción.'
      };

      return result;
    }

    if(temas.length != 0) {
      temas.every(function(tema) {
        if(tema.subTemas.length == 0) {
          result = {
            isError: true,
            errorMsg: 'Es necesario seleccionar mínimo un subtema por cada tema seleccionado.'
          };

          return false;
        }

        return true;
      });

      if(result.isError) {
        return result;  
      }

    } else {
      result = {
        isError: true,
        errorMsg: 'Es necesario seleccionar mínimo un tema.'
      };

      return result;
    }

    if(sectores.length != 0) {
      //validar que si tiene subsectores entonces debe tener seleccionado minimo un subsector
      sectores.every(function(sector) {
        if(sector.hasSubSectores && sector.subSectores.length == 0) {
          result = {
            isError: true,
            errorMsg: 'Es necesario seleccionar mínimo un subsector por cada sector que contiene subsectores.'
          };

          return true;
        }

        return true;
      });

      if(result.isError) {
        return result;  
      }

    } else {
      result = {
        isError: true,
        errorMsg: 'Es necesario seleccionar mínimo un sector.'
      };

      return result;
    }

    if(tiempos.length == 0) {
      result = {
        isError: true,
        errorMsg: 'Es necesario seleccionar mínimo un tiempo a invertir.'
      };

      return result;
    }

    if(medios.length == 0) {
      result = {
        isError: true,
        errorMsg: 'Es necesario seleccionar mínimo un medio.'
      };

      return result;
    }

    if(niveles.length == 0) {
      result = {
        isError: true,
        errorMsg: 'Es necesario seleccionar mínimo un nivel.'
      };

      return result;
    }

    if(inversiones.length == 0) {
      result = {
        isError: true,
        errorMsg: 'Es necesario seleccionar mínimo un tipo de inversión.'
      };

      return result;
    }

    return result;
  },
  onSelectedSector: function(itemSelected, index) {
    this.refs.subSectoresChecked.setArrElements(itemSelected.subSectores);
  },
  onSelectedTema: function(itemSelected, index) {
    this.refs.subTemasChecked.setArrElements(itemSelected.subTemas);
  },
  onClickEliminaArchivo: function(archivoAccion, index, evt) {
    evt.preventDefault();
    var archivosAccion = this.state.archivosAccion;
    archivosAccion.splice(archivosAccion.indexOf(archivoAccion), 1);

    this.setState({
      archivosAccion: archivosAccion
    });
  },
  onErrorFileSelected: function(fileId, message) {
    console.log('fileId-> ' + fileId + ', message-> ' + message);
    this.refs.popUpAlert.show(message);
  },
  onFileSelected: function(fileId, fileName, fileSelected, inputFile) {
    var archivosAccion = this.state.archivosAccion;
    var archivoAccion = undefined;
    var archivoIndex = 0;
    //buscar si el archivo ya existe en el arreglo (que debe :D)
    this.state.archivosAccion.every(function(archivo, index) {
      if(archivo.fileId == fileId) {
        archivoAccion = archivo;
        archivoIndex = index;

        return false;
      }

      return true;
    });

    archivoAccion.fileName = fileName;
    archivoAccion.inputFile = inputFile;
    archivoAccion.fileSelected = fileSelected;
    archivosAccion[archivoIndex] = archivoAccion;

    this.setState({
      archivosAccion: archivosAccion
    });
  },
  onClickNewArchivoAccion: function(evt) {
    evt.preventDefault();
    var archivosAccion = this.state.archivosAccion;
    var archivosAccionCount = (this.state.archivosAccionCount + 1);
    var archivoAccion = {
      fileId: archivosAccionCount,
      fileName: '',
      inputFile: undefined,
      fileSelected: false
    };

    archivosAccion.push(archivoAccion);

    this.setState({
      archivosAccionCount: archivosAccionCount,
      archivosAccion: archivosAccion
    });
  },
  onSuccessGuardarAccion: function(evt) {
    this.props.onClickGuardarAccion();
  },
  render: function() {
    var self = this;
    var componentShow = 'componentShow';
    var componentHide = 'componentHide';
    var styleTabContent = 'tab_content';
    var activeTabConfigAccion = '';
    var styleTabConfigAccion = componentHide;
    var activeTabConfigArchivos = '';
    var styleTabConfigArchivos = componentHide;
    var styleAltaComp = componentHide;
    var onTabConfigAccion = undefined;
    var onTabConfigArchivos = undefined;
    var generalAcciones = [];
    var llamadoAcciones = [];
    var archivosAccion = [];

    if(!this.state.shouldMount) {
      return (<div></div>);
    }
    //decidir si mostrar el componente
    if(this.state.showComponent) {
      styleAltaComp = componentShow;

    } else {
      styleAltaComp = componentHide;
    }
    //que hacer dependiendo del tab seleccionado
    switch(this.state.tabSelected) {
      case Constants.ADMIN_ALTA_ACCION.TAB_CONFIG_ACCION:
        styleTabConfigAccion = (styleTabContent + ' ' + componentShow);
        activeTabConfigAccion = 'active';
      break;

      case Constants.ADMIN_ALTA_ACCION.TAB_CONFIG_ARCHIVOS:
        styleTabConfigArchivos = (styleTabContent + ' ' + componentShow);
        activeTabConfigArchivos = 'active';
      break;
    }
    //crear acciones para los tab existentes
    onTabConfigAccion = function(tabSelected, evt){
      self.onClickTab(tabSelected, evt);
    }.bind(self, Constants.ADMIN_ALTA_ACCION.TAB_CONFIG_ACCION);

    onTabConfigArchivos = function(tabSelected, evt){
      self.onClickTab(tabSelected, evt);
    }.bind(self, Constants.ADMIN_ALTA_ACCION.TAB_CONFIG_ARCHIVOS);
    //#####################
    //Construir catalogos
    if(this.state.catGeneralAcciones != undefined) {
      this.state.catGeneralAcciones.forEach(function(generalAccion) {
        generalAcciones.push(<option value={generalAccion.idGeneralAccion}>{generalAccion.nombre}</option>);
      });
    }
    
    if(this.state.catLlamadoAcciones != undefined) {
      this.state.catLlamadoAcciones.forEach(function(llamadoAccion) {
        llamadoAcciones.push(<option value={llamadoAccion.idLlamadoAccion}>{llamadoAccion.descripcion}</option>);
      });
    }
    //####################
    //construccion de compornentes para carga de archivos
    this.state.archivosAccion.forEach(function(archivoAccion, index) {
      var onClickEliminaArchivo = function(archivoAccion, index, evt){
        self.onClickEliminaArchivo(archivoAccion, index, evt);
      }.bind(self, archivoAccion, index);

      archivosAccion.push(
        <article className='width_2_full' style={{height: '30px'}}>
          <a className='IconCancelStyle' style={{width: '5%', float: 'left'}} href='#' onClick={onClickEliminaArchivo}></a>
          <InputFileReact fileId={archivoAccion.fileId} fileName={archivoAccion.fileName} fileInput={archivoAccion.fileInput} name={'Archivo_' + index} 
            placeholder='Nombre del archivo' maxSize='3000' extensions={['.pdf', 'docx']} onFileSelected={self.onFileSelected} onErrorFileSelected={self.onErrorFileSelected}/>
        </article>
      );
    });
    
    return (
      <div id='container-gral' className={styleAltaComp}>
        <PopUpReact ref='popUpAlert' type='alert' popUpStyle='PopUpReactAlert'/>
        <PopUpReact ref='popUpGuardarSucces' type='alert' popUpStyle='PopUpReactAlert' onClickAceptar={this.onSuccessGuardarAccion}/>

        <nav>
          <ul id='menuNav'>
            <li className={activeTabConfigAccion} onClick={onTabConfigAccion}>Configuración de Accion</li>
            <li className={activeTabConfigArchivos} onClick={onTabConfigArchivos}>Configuración de Archivos</li>
          </ul>
        </nav>
        
        <article className='module width_full'>
          <div className={styleTabConfigAccion}>
            <article className='module width_full'>
              <header><h3>Configuración acción</h3></header>
              <div className='module_content' style={{width: '98%'}}>

                <article className='width_2_full' style={{height: '30px'}}>
                  <label style={{width:'13%'}}>Acción:</label>
                  <input type='text' style={{width:'86%'}} name='nombre' maxLength='340' size='300' value={this.state.newAccion.descripcion} 
                    onChange={this.onChangeNombreAccion}/>
                </article>

                <article className='width_2_full' style={{height: '30px'}}>
                  <label style={{width:'13%'}}>General Acción:</label>
                  <select style={{width:'86%'}} value={this.state.newAccion.idGeneralAccion} onChange={this.onChangeGeneralAccion}>
                    <option value='0'>Selecciona una opción</option>
                    {generalAcciones}
                  </select>
                </article>

                <article className='width_2_full' style={{height: '30px'}}>
                  <label style={{width:'13%'}}>Llamado a la Acción:</label>
                  <select style={{width:'86%'}} value={this.state.newAccion.idLlamadoAccion} onChange={this.onChangeLlamadoAccion}>
                    <option value='0'>Selecciona una opción</option>
                    {llamadoAcciones}
                  </select>
                </article>

                <article className='width_half' style={{marginTop: '15px'}}>
                  <label style={{width:'100%'}}>Temas:</label>
                  <CheckTableReact ref='temasChecked' arrElements={this.state.temasClone} elementId='idTema' elementName='descripcion'
                    showTools={true} onSelectedItem={this.onSelectedTema}/>
                </article>

                <article className='width_half' style={{marginLeft: '5px', marginTop: '15px', marginBottom: '15px'}}>
                  <label style={{width:'100%'}}>SubTemas:</label>
                  <CheckTableReact ref='subTemasChecked' arrElements={this.state.subTemasClone} elementId='idSubTema' elementName='descripcion'/>
                </article>

                <article className='width_half' style={{marginTop: '15px'}}>
                  <label style={{width:'100%'}}>Sectores:</label>
                  <CheckTableReact ref='sectoresChecked' arrElements={this.state.sectoresClone} elementId='idSector' elementName='descripcion' 
                    showTools={true} onSelectedItem={this.onSelectedSector}/>
                </article>

                <article className='width_half' style={{marginLeft: '5px', marginTop: '15px', marginBottom: '15px'}}>
                  <label style={{width:'100%'}}>SubSectores:</label>
                  <CheckTableReact ref='subSectoresChecked' elementId='idSubSector' elementName='descripcion'/>
                </article>

                <article className='width_1_4_quarter' style={{marginTop: '15px'}}>
                  <label style={{width:'100%'}}>Tiempo a invertir:</label>
                  <CheckTableReact ref='tiemposChecked' arrElements={this.state.tiemposClone} elementId='idTiempo' elementName='descripcion'/>
                </article>                

                <article className='width_1_4_quarter' style={{marginLeft: '5px', marginTop: '15px'}}>
                  <label style={{width:'100%'}}>Medios:</label>
                  <CheckTableReact ref='mediosChecked' arrElements={this.state.mediosClone} elementId='idMedio' elementName='descripcion'/>
                </article>

                <article className='width_1_4_quarter' style={{marginLeft: '5px', marginTop: '15px'}}>
                  <label style={{width:'100%'}}>Niveles:</label>
                  <CheckTableReact ref='nivelesChecked' arrElements={this.state.nivelesClone} elementId='idNivel' elementName='descripcion'/>
                </article>

                <article className='width_1_4_quarter' style={{marginLeft: '5px', marginTop: '15px'}}>
                  <label style={{width:'100%'}}>Inversión:</label>
                  <CheckTableReact ref='inversionesChecked' fileId='0' arrElements={this.state.inversionesClone} elementId='idInversion' elementName='descripcion'
                    onFileSelected={this.onFileSelected}/>
                </article>

              </div>
            </article>
          </div>

          <div className={styleTabConfigArchivos}>
            <article className='module width_full'>
              <header>
                <div className='AlignContainer'>
                  <div className='AlignLeft' style={{width: '95%'}}>
                    <h3 className='tabs_involved'>Configuración de acción</h3>
                  </div>
                  <div className='AlignRight' style={{width: '5%', marginTop: '5px', textAlign: 'center'}}>
                    <a className='IconNewStyle' href='#' onClick={this.onClickNewArchivoAccion}></a>
                  </div>
                </div>
              </header>
              <div className='module_content' style={{width: '98%'}}>
                {archivosAccion}
              </div>
            </article>
          </div>

          <footer>
            <div className='submit_link'>
              <input type='button' value='CANCELAR' className='alt_btn' onClick={this.onClickCancelarAltaAccion}/>
              <input type='button' value='GUARDAR' onClick={this.onClickGuardarAccion}/>
            </div>
          </footer>
        </article>
        <div className='spacer'></div>
      </div>
    );
  }
});

module.exports = AltaAcciones;
