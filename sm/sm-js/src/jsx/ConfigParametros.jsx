'use strict';

var Router = require('../libs/react-router/index.js');
/* # seccion para declarar utilerias # */
var Clone = require('../utils/Clone.js');
var Context = require('../utils/Context.js');
var Label = require('../utils/Label.js');
var ErrorCodes = require('../utils/ErrorCodes.js');
var ErrorMsg = require('../utils/ErrorMsg.js');
var ComponentState = require('../utils/ComponentState.js');
var Constants = require('../utils/Constants.js');
/* # seccion para declarar compronentes comunes # */
var PopUpReact = require('../components/commons/PopUpReact.js');
/* # seccion para declarar servicios # */
var adminService = require('../modulos/MyTAdminService.js');
var parametrosService = require('../modulos/ParametrosService.js');
var validaService = require('../utils/ValidaService.js');

var ConfigParamateros = React.createClass({
  mixins: [ Router.Navigation ],
  getInitialState: function() {
    return {
      usuario: ctx.get('usuario'),
      shouldMount: true,
      componentState: ComponentState.NORMAL_STATE,
      parametros: undefined
    };
  },
  getDefaultProps: function() {
    return {
      isNormal: 1,
      isEditar: 2,
      storage: new Context()
    };
  },
  componentWillMount: function() {
  },
  componentDidMount: function() {
    var self = this;

    var onSuccess = function(res) {
      //estableser todos los parametros a un estado normal
      res.parametros.forEach(function(param) {
        param.isState = self.props.isNormal; 
      });

      self.setState({
        parametros: res.parametros
      });
    };

    if(validaSession()) {
      var params = {};
      parametrosService.getParametrosConfig(params, onSuccess, this.onError, this.onFail);

    } else {
      this.setState({
        shouldMount: false
      });
      this.transitionTo('Login');
    }
  },
  componentWillReceiveProps: function(nextProps) {
  },
  shouldComponentUpdate: function() {
    var shouldUpdate = false;

    if(validaSession()) {
      shouldUpdate = true;
    }

    return shouldUpdate;
  },
  componentWillUpdate: function() {
  },
  componentDidUpdate: function() {
  },
  componentWillUnmount: function() {
  },
  onFail: function(err) {
    console.log(err);
    this.setState({
      componentState: ComponentState.ERROR_STATE,
    });
    this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
  },
  onError: function(err) {
    console.log(err);

    switch(err.errorCode) {
      case ErrorCodes.ERR_01:
        this.setState({
          componentState: ComponentState.ERROR_STATE,
        });
        this.refs.popUpAlert.show(ErrorCodes.ERR_01_MSG);
      break;

      case ErrorCodes.ERR_03:
        this.setState({
          componentState: ComponentState.ERROR_STATE,
        });
        this.refs.popUpAlert.show(ErrorCodes.ERR_03_MSG);
      break;

      default:
        console.log('Error no categorizado...');
      break;
    }

    scrollMainTo();
  },
  onClickIrEditar: function(param, index, evt) {
    evt.preventDefault();
    var parametros = this.state.parametros;

    param.valorOrg = param.valor;
    param.isState = this.props.isEditar;
    parametros[index] = param;

    this.setState({
      parametros: parametros
    });
  },
  onClickSiEditar: function(param, index, evt) {
    evt.preventDefault();
    var result = this.validaParametro(param);

    if(!result.isError) {
      this.props.storage.put('PARAM_TO_UPDATE', Clone.clone(param));
      this.refs.popUpAsk.show(Label.ASK_MODIF_PARAMETRO 
        + '  [NOMBRE = ' + param.descripcion + ', VALOR = ' + param.valor + ']' + ' ?');

    } else {
      this.refs.popUpAlert.show(
        ErrorMsg.ERROR_PARAMTERO_FORMATO + 
          '  [NOMBRE = ' + param.descripcion + 
          ', VALOR = ' + param.valor + 
          ']  no tiene un formato correcto, Ejemplo: ' +
          result.ejemploFormato      
        );
    }
  },
  validaParametro: function(param) {
    var result = {
      isError: false,
      errMsg: '',
      ejemploFormato: ''
    };

    switch(param.tipoValor) {
      case Constants.ParametroTipoValor.ENTERO:
        if(!validaService.isOnlyNumbers(param.valor)) {
          result = {
            isError: true,
            errMsg: ErrorMsg.ERROR_PARAMTERO_FORMATO_INCORRECTO,
            ejemploFormato: Constants.ParametroFormato.ENTERO
          };
        }
      break;
      case Constants.ParametroTipoValor.DECIMAL:
        if(!validaService.isDecimal(param.valor)) {
          result = {
            isError: true,
            errMsg: ErrorMsg.ERROR_PARAMTERO_FORMATO_INCORRECTO,
            ejemploFormato: Constants.ParametroFormato.DECIMAL
          };
        }
      break;
      case Constants.ParametroTipoValor.MINUTO_SEGUNDO:
        if(!validaService.isHoraMinuto(param.valor)) {
          result = {
            isError: true,
            errMsg: ErrorMsg.ERROR_PARAMTERO_FORMATO_INCORRECTO,
            ejemploFormato: Constants.ParametroFormato.MINUTO_SEGUNDO
          };
        }
      break;
      case Constants.ParametroTipoValor.ALFANUMERIC:
        if(!validaService.isAlfaNumeric(param.valor)) {
          result = {
            isError: true,
            errMsg: ErrorMsg.ERROR_PARAMTERO_FORMATO_INCORRECTO,
            ejemploFormato: Constants.ParametroFormato.ALFANUMERIC
          };
        }
      break;
    }

    return result;
  },
  onClickNoEditar: function(param, index, evt) {
    evt.preventDefault();
    var parametros = this.state.parametros;

    this.props.storage.clean();
    param.valor = param.valorOrg;
    param.valorOrg = undefined;
    param.isState = this.props.isNormal;
    parametros[index] = param;

    this.setState({
      parametros: parametros
    });
  },
  onChangeParamValue: function(param, index, evt) {
    var parametros = this.state.parametros;
    param.valor = evt.target.value;
    parametros[index] = param;

    this.setState({
      parametros: parametros
    });
  },
  onClickAceptarEditar: function(evt) {
    var self = this;
    this.refs.popUpAsk.hide();
    var paramToUpdate = this.props.storage.get('PARAM_TO_UPDATE');

    var onSuccess = function(res) {
      var parametros = self.state.parametros;
      var indexTmp = self.getIndexParametro(paramToUpdate.idParametro);
      var parametroUpdated = self.state.parametros[indexTmp];

      self.props.storage.clean();
      parametroUpdated.isState = self.props.isNormal;
      parametroUpdated.valorOrg = undefined;
      parametros[indexTmp] = parametroUpdated;

      self.setState({
        parametros: parametros
      });

      self.refs.popUpAlert.show(Label.REGISTRO_MODIF_SUCCESS);
    };

    var params = {
      idParametro: paramToUpdate.idParametro,
      valor: paramToUpdate.valor
    };
    parametrosService.updateParametro(params, onSuccess, this.onError, this.onFail);
  },
  onClickCancelarEditar: function(evt) {
    this.refs.popUpAsk.hide();
    this.props.storage.clean();
  },
  getIndexParametro: function(idParametro) {
    var indexTmp = undefined;

    if(this.state.parametros != undefined) {
      this.state.parametros.every(function(param, index) {
        if(param.idParametro == idParametro) {
          indexTmp = index;
          return false;
        }

        return true;
      });
    }

    return indexTmp;
  },
  render: function() {
    var self = this;
    var parametrosRows = [];

    if(!this.state.shouldMount) {
      return (<div></div>);
    }

    switch(this.state.componentState) {
      case ComponentState.NORMAL_STATE:
      break;

      case ComponentState.EXITO_STATE:
      break;

      case ComponentState.ERROR_STATE:
      break;

      case ComponentState.INFO_STATE:
      break;

      case ComponentState.ALERT_STATE:
      break;
    }
    //crear lista de etiquetas tr para la tabla de parametros
    if(this.state.parametros != undefined) {
      this.state.parametros.forEach(function(param, index) {
        var accionesHTML = [];
        var valorHTML = [];
        var onClickIrEditar = function(param, index, evt) {
          self.onClickIrEditar(param, index, evt);
        }.bind(self, param, index);

        var onChangeParamValue = function(param, index, evt) {
          self.onChangeParamValue(param, index, evt);
        }.bind(self, param, index);

        var onClickSiEditar = function(param, index, evt) {
          self.onClickSiEditar(param, index, evt);
        }.bind(self, param, index);

        var onClickNoEditar = function(param, index, evt) {
          self.onClickNoEditar(param, index, evt);
        }.bind(self, param, index);

        if(param.isState == self.props.isNormal) {
          valorHTML.push(param.valor);
          accionesHTML.push(<a className='IconEditStyle' href='#' onClick={onClickIrEditar}></a>);

        } else if(param.isState == self.props.isEditar) {
          valorHTML.push(
            <input type="text" style={{width:'50%'}} placeholder="Valor del parámetro" 
              maxLength="100" value={param.valor} onChange={onChangeParamValue}/>
          );
          accionesHTML.push(<a className='IconOKStyle' href='#' onClick={onClickSiEditar}></a>);
          accionesHTML.push(<a className='IconCancelStyle' href='#' onClick={onClickNoEditar}></a>);
        }

        parametrosRows.push(
          <tr> 
            <td style={{paddingTop: '5px', paddingBottom: '5px'}}>{param.descripcion}</td> 
            <td style={{paddingTop: '5px', paddingBottom: '5px'}}>
              {valorHTML}
            </td> 
            <td style={{paddingTop: '5px', paddingBottom: '5px'}}>
              {accionesHTML}
            </td> 
          </tr> 
        );
      });
    }
   
    return (
      <div id='container-gral'>
        <PopUpReact ref='popUpAlert' type='alert'/>
        <PopUpReact ref='popUpAsk' type='ask' 
          buttons={[
            {name: 'Aceptar', label: 'Aceptar', onClick: this.onClickAceptarEditar},
            {name: 'Cancelar', label: 'Cancelar', onClick: this.onClickCancelarEditar}
        ]}/>
        
        <article className='module width_full'>
          <header>
            <h3 className='tabs_involved'>Lista de parámetros</h3>
          </header>
          <div className='tab_container'>
            <div id='tab1' className='tab_content'>
              <table className='tablesorter' cellSpacing='0'> 
                <thead> 
                  <tr>
                    <th style={{width: '40%'}}>NOMBRE</th> 
                    <th>VALOR</th> 
                    <th style={{width: '10%'}}></th> 
                  </tr> 
                </thead> 
                <tbody> 
                  {parametrosRows}
                </tbody>
              </table>
            </div>
          </div>
        </article>
        <div className='spacer'></div>
      </div>
    );
  }
});

module.exports = ConfigParamateros;