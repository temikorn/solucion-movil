module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch: {
      main: {
        files: ['src/**/*.js','src/**/*.html', 'src/jsx/**/*.jsx', 'src/less/*.less'],
        tasks: ['clean:build','copy:html', 'copy:libs', 'react', 'less', 'browserify:main'],
        options: {
          spawn: false,
        },
      }
    },
    clean: { 
      options: {force:true}, 
      build: {src:["build/*"]},
      cordova: {src:["movil/myt/www/*"]}
    },        
    copy: {
      html: {
        files: [
          {expand: true, src: ['src/index.html'], dest: 'build/', flatten: true, filter: 'isFile'},
          {expand: true, src: ['src/fonts/*'], dest: 'build/fonts/', flatten: true, filter: 'isFile'},
          {expand: true, src: ['src/images/*'], dest: 'build/images/', flatten: true, filter: 'isFile'}
        ]
      },
      libsDev: {
        files: [
          {expand: true, cwd:'src/libs/', src: ['jquery.js'], dest: 'build/js/lib/'},
          {expand: true, cwd:'src/libs/react/dev/', src: ['react.js'], dest: 'build/js/lib/'},
          {expand: true, cwd:'src/libs/react/dev/', src: ['react-with-addons.js'], dest: 'build/js/lib/'},
          {expand: true, cwd:'src/libs/', src: ['highcharts.js'], dest: 'build/js/lib/'},
          {expand: true, cwd:'src/libs/', src: ['hammer.min.js'], dest: 'build/js/lib/'},
          {expand: true, cwd:'src/libs/', src: ['init.js'], dest: 'build/js'}
        ]                
      },
      libsProd: {
        files: [
          {expand: true, cwd:'src/libs/', src: ['jquery.js'], dest: 'build/js/lib/'},
          {expand: true, cwd:'src/libs/react/prod/', src: ['react.js'], dest: 'build/js/lib/'},
          {expand: true, cwd:'src/libs/react/prod/', src: ['react-with-addons.js'], dest: 'build/js/lib/'},
          {expand: true, cwd:'src/libs/', src: ['highcharts.js'], dest: 'build/js/lib/'},
          {expand: true, cwd:'src/libs/', src: ['hammer.js'], dest: 'build/js/lib/'},
          {expand: true, cwd:'src/libs/', src: ['init.js'], dest: 'build/js'}
        ]                
      },
      cordova: {
        files: [
          {expand:true, cwd: 'build/', src: '**', dest: 'movil/myt/www/'}
        ]
      },
      webapp: {
        files: [
          {expand:true, cwd: 'build/', src: '**', dest: '../sm-webapps/sm-front/src/main/webapp/'}
        ]
      }
    },
    less: {
      main: {
        options: {
          paths: ["src/less/*"],
          compress: true,
          ieCompat: false,
          strictImports: true,
          syncImport: true,
          cleancss: true
        },
        files: {
          'build/css/app.css': 'src/less/style.less'
        }
      }
    },
    react: {
      files: {
        expand: true,
        cwd: 'src/jsx/',
        src: ['**/*.jsx','*.jsx'],
        dest: 'src/components',
        ext: '.js'
      }
    },
    browserify: {
      main: {
        files: {
          'build/js/app.js' : [
            'src/components/index.js'
          ]
        }
      }
    },
    uglify: {
      main: {
        files: {
          'build/js/app.js': ['build/js/app.js']
        }
      }
    }, 
    jasmine: {
      pivotal: {
        src: 'test/testApp.js',
        options: {
          specs: 'src/test/jasmine/*.js'
        }
      }
    }
  });

  //Load the plugin for copy files
  grunt.loadNpmTasks('grunt-contrib-copy');

  //load the plugin for watch files
  grunt.loadNpmTasks('grunt-contrib-watch');

  //load the plugin for clean files
  grunt.loadNpmTasks('grunt-contrib-clean');

  grunt.loadNpmTasks('grunt-browserify');
  
  grunt.loadNpmTasks('grunt-react');

  grunt.loadNpmTasks('grunt-contrib-less');

  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.loadNpmTasks('grunt-contrib-jasmine');

  grunt.registerTask('default', ['watch:main']);

  grunt.registerTask('build', ['clean:build','copy:html', 'copy:libsDev', 'react', 'less', 'browserify:main']);

  grunt.registerTask('prod', ['clean:build', 'copy:html', 'copy:libsProd', 'react', 'less', 'browserify:main', 'uglify', 'copy:webapp']);

  grunt.registerTask('cordova', ['clean:build','copy:html', 'copy:libs', 'react', 'less', 'browserify:main','clean:cordova','copy:cordova']);
};
