package mx.org.sm.dao;

import java.util.List;

import mx.org.sm.dao.model.Localizacion;

public interface LocalizacionDAO {

  List<Localizacion> getLocations();
  int addLocations(final Localizacion localizacion);  
}
