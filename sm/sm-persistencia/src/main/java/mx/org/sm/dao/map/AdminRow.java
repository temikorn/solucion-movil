package mx.org.sm.dao.map;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.org.sm.dao.model.Usuario;

@SuppressWarnings("rawtypes")
public class AdminRow implements RowMapper {
  public enum Opcion {
    USUARIO
  };

  private Opcion opcion;
  
  public AdminRow(Opcion opcion) {
    this.opcion = opcion;
  }
  
  @Override
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    Object returnObject = null;

    switch(this.opcion) {
      case USUARIO:
        returnObject = fillUsuario(rs);
        break;
            
    }
    
    return returnObject;
  }
  
  private Usuario fillUsuario(ResultSet rs) throws SQLException {
    Usuario usuario = null;
    
    usuario = new Usuario();
    usuario.setIdUsuario(rs.getInt("ID_USUARIO"));
    usuario.setUsuario(rs.getString("USUARIO"));
    usuario.setNombre(rs.getString("NOMBRE"));
    
    return usuario;
  }  
}
