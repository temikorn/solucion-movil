package mx.org.sm.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import mx.org.sm.dao.LocalizacionDAO;
import mx.org.sm.dao.map.LocalizacionRow;
import mx.org.sm.dao.model.Localizacion;
import mx.org.sm.dao.util.PersistenceDAO;

@Repository("LocalizacionDAO")
public class LocalizacionDAOImpl extends PersistenceDAO implements LocalizacionDAO {
  private static final Logger LOG = Logger.getLogger(LocalizacionDAOImpl.class);

  @SuppressWarnings("unchecked")
  @Override
  public List<Localizacion> getLocations() {
    LOG.info("# getLocations DAO #");
    List<Object> locations = null;
    StringBuilder query = null;
    
    query = new StringBuilder();
    locations = new ArrayList<Object>();
    
    query.append(getQueries().getProperty("GET_LOCATIONS"));
    
    LocalizacionRow daoRow = new LocalizacionRow(LocalizacionRow.Opcion.LOCALIZACION);
    return getJdbcTemplate().query(query.toString(), daoRow, locations.toArray());
  }
  
  
 @Override
 public int addLocations(final Localizacion localizacion) {
   LOG.info("# addLocations #");
   int lastInsert = 0;
   KeyHolder keyHolder = null;
   StringBuilder query = null;
   
   query = new StringBuilder();
   query.append(getQueries().getProperty("INSERT_LOCALIZACION"));
   final String sql = query.toString(); 
   
   keyHolder = new GeneratedKeyHolder();
   getJdbcTemplate().update(new PreparedStatementCreator()  {
     public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
       int param = 0;
       PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
       ps.setString(++param, localizacion.getVehiculo());
       ps.setInt(++param, localizacion.getLatitud());
       ps.setInt(++param, localizacion.getLongitud());
       return ps;
     }
   }, keyHolder);
   
   lastInsert = keyHolder.getKey().intValue();
   return lastInsert;
 }   
  
  
}
