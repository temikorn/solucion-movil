package mx.org.sm.dao.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Usuario implements Serializable {

  private int idUsuario;
  private String nombre;
  private String usuario;
  private String token;

  public int getIdUsuario() {
    return idUsuario;
  }
  public void setIdUsuario(int idUsuario) {
    this.idUsuario = idUsuario;
  }
  public String getNombre() {
    return nombre;
  }
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
  public String getToken() {
    return token;
  }
  public void setToken(String token) {
    this.token = token;
  }
  public String getUsuario() {
    return usuario;
  }
  public void setUsuario(String usuario) {
    this.usuario = usuario;
  }
  
}
