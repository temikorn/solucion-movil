package mx.org.sm.dao.map;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import mx.org.sm.dao.model.Localizacion;

@SuppressWarnings("rawtypes")
public class LocalizacionRow implements RowMapper {

  public enum Opcion {
    LOCALIZACION
  };

  private Opcion opcion;
  
  public LocalizacionRow(Opcion opcion) {
    this.opcion = opcion;
  }
  
  @Override
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    Object returnObject = null;

    switch(this.opcion) {
      case LOCALIZACION:
        returnObject = fillLocalizacion(rs);
        break;
    }
    
    return returnObject;
  }
  
  private Localizacion fillLocalizacion(ResultSet rs) throws SQLException {
    Localizacion localizacion = null;
    
    localizacion = new Localizacion();
    localizacion.setIdLocalizacion(rs.getInt("ID_LOCALIZACION"));
    localizacion.setVehiculo(rs.getString("VEHICULO"));
    localizacion.setLatitud(rs.getInt("LATITUD"));
    localizacion.setLongitud(rs.getInt("LONGITUD"));
    localizacion.setFhSatelite(rs.getDate("FH_SATELITE"));
    localizacion.setFhRegistro(rs.getDate("FH_REGISTRO"));
    
    return localizacion;
  }
}
