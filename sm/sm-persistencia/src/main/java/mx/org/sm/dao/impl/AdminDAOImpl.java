package mx.org.sm.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import mx.org.sm.dao.AdminDAO;
import mx.org.sm.dao.map.AdminRow;
import mx.org.sm.dao.model.Usuario;
import mx.org.sm.dao.util.PersistenceDAO;

@Repository("AdminDAO")
public class AdminDAOImpl extends PersistenceDAO implements AdminDAO {
  private static final Logger LOG = Logger.getLogger(AdminDAOImpl.class);
  
  @SuppressWarnings("unchecked")
  @Override
  public Usuario getUsuario(String nombreUsuario, String password) {
    LOG.info("# getUsuario #");
    List<Object> parameters = null;
    List<Usuario> usuarios = null;
    Usuario usuario = null;
    StringBuilder query = null;
    
    query = new StringBuilder();
    parameters = new ArrayList<Object>();
    parameters.add(nombreUsuario);
    parameters.add(password);
    
    query.append(getQueries().getProperty("GET_USUARIO"));
    
    AdminRow daoRow = new AdminRow(AdminRow.Opcion.USUARIO);
    usuarios = getJdbcTemplate().query(query.toString(), daoRow, parameters.toArray());
    
    if(usuarios != null && !usuarios.isEmpty()) {
      usuario = usuarios.get(0);
    }
    
    return usuario;
    }
 
}
