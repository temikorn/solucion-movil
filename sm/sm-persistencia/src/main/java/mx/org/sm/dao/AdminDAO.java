package mx.org.sm.dao;

import mx.org.sm.dao.model.Usuario;

public interface AdminDAO {
	
   Usuario getUsuario(String nombreUsuario, String password);  
  
}
