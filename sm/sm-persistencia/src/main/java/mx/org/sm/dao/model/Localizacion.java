package mx.org.sm.dao.model;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Localizacion implements Serializable {
  private Integer idLocalizacion;
  private String vehiculo;
  private Integer latitud;
  private Integer longitud;
  private Date fhSatelite;
  private Date fhRegistro;

public String getVehiculo() {
	return vehiculo;
}
public void setVehiculo(String vehiculo) {
	this.vehiculo = vehiculo;
}
public Integer getLatitud() {
	return latitud;
}
public void setLatitud(Integer latitud) {
	this.latitud = latitud;
}
public Integer getLongitud() {
	return longitud;
}
public void setLongitud(Integer longitud) {
	this.longitud = longitud;
}
public Date getFhSatelite() {
	return fhSatelite;
}
public void setFhSatelite(Date fhSatelite) {
	this.fhSatelite = fhSatelite;
}
public Date getFhRegistro() {
	return fhRegistro;
}
public void setFhRegistro(Date fhRegistro) {
	this.fhRegistro = fhRegistro;
}
public Integer getIdLocalizacion() {
	return idLocalizacion;
}
public void setIdLocalizacion(Integer idLocalizacion) {
	this.idLocalizacion = idLocalizacion;
}

}
