package mx.org.sm.service.impl;

import java.util.Date;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import mx.org.sm.dao.model.Localizacion;
import mx.org.sm.service.LocalizacionService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
                      "classpath:mx/org/sm/dao/xml/*.xml",
                      "classpath*:/CommonsAppCtx.xml",
                      "classpath*:/PersistenciaAppCtx.xml",
                      "classpath*:/ServiceAppCtx.xml"})
public class DemoServiceImplTest {
  private static final Logger LOG = Logger.getLogger(DemoServiceImplTest.class);
  
  @Autowired
  private LocalizacionService localizacionService;
  
  @Ignore
  @Test
  public void test_getLocations(){
    LOG.info("# test_getLocations #");

    try {         
        localizacionService.getLocations();      
    } catch(Exception e) {
      LOG.error(e.getMessage(), e);
    }
  }
    
  @Ignore
  @Test
  public void test_addLocations(){
    LOG.info("# test_addLocations #");
    Localizacion localizacion = new Localizacion();
   
    try {
           
    	localizacion.setVehiculo("VW");
    	localizacion.setLatitud(1);
      	localizacion.setLongitud(3);
      	localizacion.setFhSatelite(new Date());
     	localizacion.setFhRegistro(new Date());
        localizacionService.addLocations(localizacion);
         
    } catch(Exception e) {
      LOG.error(e.getMessage(), e);
    }
  }
 
 }