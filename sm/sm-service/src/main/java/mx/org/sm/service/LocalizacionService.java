package mx.org.sm.service;

import java.util.List;

import mx.org.sm.common.ApplicationException;
import mx.org.sm.dao.model.Localizacion;

public interface LocalizacionService {
  List<Localizacion> getLocations() throws ApplicationException; 
  void addLocations(Localizacion localizacion) throws ApplicationException;
}
