package mx.org.sm.service;

import mx.org.sm.common.ApplicationException;
import mx.org.sm.dao.model.Usuario;

public interface AdminService {
  Usuario getUsuario(String nombreUsuario, String password) throws ApplicationException, Exception; 
}
