package mx.org.sm.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.org.sm.common.ApplicationException;
import mx.org.sm.dao.LocalizacionDAO;
import mx.org.sm.dao.model.Localizacion;
import mx.org.sm.service.LocalizacionService;

@Service("LocalizacionService")
public class LocalizacionServiceImpl implements LocalizacionService {
  private static final Logger LOG = Logger.getLogger(LocalizacionServiceImpl.class);
  
  @Autowired
  private LocalizacionDAO localizacionDAO;

  @Override
  public List<Localizacion> getLocations() throws ApplicationException {
    LOG.info("# getLocations Service tamaño#" + localizacionDAO.getLocations().size());
    return localizacionDAO.getLocations();
  }
  
  @Override
  public void addLocations(Localizacion localizacion) throws ApplicationException {
    LOG.info("# guardarLocalizacion #");
    
    Localizacion guardarLocalizacion = null;
    int idLocalizacion = 0;
    //construir objeto para insertar en Localizacion
    guardarLocalizacion = new Localizacion();
    guardarLocalizacion.setIdLocalizacion(localizacion.getIdLocalizacion());
    guardarLocalizacion.setVehiculo(localizacion.getVehiculo());
    guardarLocalizacion.setLongitud(localizacion.getLongitud());
    guardarLocalizacion.setLatitud(localizacion.getLatitud());
    guardarLocalizacion.setFhSatelite(localizacion.getFhSatelite());
    guardarLocalizacion.setFhRegistro(localizacion.getFhRegistro());
    idLocalizacion = localizacionDAO.addLocations(guardarLocalizacion);
    LOG.info("idLocalizacion-> " + idLocalizacion);
      
    }

}
