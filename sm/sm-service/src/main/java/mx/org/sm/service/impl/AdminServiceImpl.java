package mx.org.sm.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.org.sm.common.AppError;
import mx.org.sm.common.ApplicationException;
import mx.org.sm.common.SecurityUtil;
import mx.org.sm.dao.AdminDAO;
import mx.org.sm.dao.model.Usuario;
import mx.org.sm.service.AdminService;

@Service("AdminService")
public class AdminServiceImpl implements AdminService {
  private static final Logger LOG = Logger.getLogger(AdminServiceImpl.class);
  
  @Autowired
  private AdminDAO adminDAO;
  
  @Autowired
  private SecurityUtil securityUtil;
  
 
  @Override
  public Usuario getUsuario(String nombreUsuario, String password) throws ApplicationException, Exception {
    LOG.info("# getUsuario #");
    Usuario usuario = null;
    
    usuario = adminDAO.getUsuario(nombreUsuario, securityUtil.encrypt(password));
    
    if(usuario == null) {
      throw new ApplicationException(AppError.ERROR_02);
    }
    
    return usuario;
  }
}
